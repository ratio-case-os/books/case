install:
  uv sync

update:
  uv lock
  uv sync

scripts:
  uv run python ./src/how-to-guides/scripts/read-dsm.py

build:
  uv run mkdocs build

serve:
  uv run mkdocs serve
