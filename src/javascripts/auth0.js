let auth0Client

window.onload = async () => {
  console.log("Setting up Auth0 client...")
  if (auth0Client == undefined) {
    auth0Client = await auth0.createAuth0Client({
      domain: "ratio.eu.auth0.com",
      clientId: "HCw1Nz7SOZkIbtH1Z2MtomM76DFiPAa1",
    })
  }

  console.log("Checking auth status...")

  const isAuthenticated = await isAuth()

  if (isAuthenticated) {
    console.log("Authenticated!")
  } else {
    const query = window.location.search
    if (query.includes("code=") && query.includes("state=")) {
      console.log("Handling authentication query...")
      await auth0Client.handleRedirectCallback()
      window.history.replaceState(
        {},
        document.title,
        window.location.origin + window.location.pathname
      )
    }
  }

  document$.subscribe(() => {
    // Only work on /launcher/
    if (window.location.pathname.startsWith("/launcher/")) {
      updateUI()
    }
  })
}

const updateUI = () => {
  console.log("Updating UI...")
  setButtonProps()
  setUserInfo()
  setEnvironments()
}

const setButtonProps = async () => {
  const isAuthenticated = await auth0Client.isAuthenticated()
  const button = document.getElementById("btn-login-logout")
  if (!button) return
  if (isAuthenticated) {
    button.onclick = logout
    button.innerText = "Log out"
  } else {
    button.onclick = login
    button.innerText = "Log in"
  }
}

const setUserInfo = async () => {
  const user = await getUser()
  const email = user ? user["email"] : ""
  const roles = user ? getRoles(user) : []
  const emailElem = document.getElementById("user-email")
  if (emailElem) {
    emailElem.innerText = email
  }
  const rolesElem = document.getElementById("user-roles")
  if (rolesElem) {
    const newRoles = roles.map((role) => {
      const li = document.createElement("li")
      li.innerText = role
      return li
    })
    rolesElem.replaceChildren(...newRoles)
  }
}

const getUser = async () => {
  return await auth0Client.getUser()
}

const getRoles = (user) => {
  return user ? user["https://ratio-case.nl/roles"] ?? [] : []
}

const login = async () => {
  console.log("Logging in")
  return await auth0Client.loginWithRedirect({
    authorizationParams: {
      redirect_uri: `${window.location.origin}/launcher/`,
    },
  })
}

const isAuth = async () => {
  return await auth0Client.isAuthenticated()
}

const logout = async () => {
  return await auth0Client.logout({ logoutParams: { federated: true } })
}

const setEnvironments = async () => {
  const envs = document.getElementById("user-environments")
  if (envs) {
    const roles = getRoles(await getUser())
    const children = roles
      .filter((role) => role.startsWith("access:"))
      .map((role) => environment(role))
    envs.replaceChildren(...children)
  }
}

const environment = (role) => {
  const domain = role.replace(/^access:/, "")
  const url = `https://${domain}`

  const container = document.createElement("a")
  container.setAttribute("style", "display:flex;flex-direction:column;height:8rem")
  container.setAttribute("href", url)
  container.setAttribute("target", "_blank")

  const logo = document.createElement("img")
  logo.setAttribute("src", "/assets/jupyter.svg")
  container.appendChild(logo)

  const name = document.createElement("div")
  name.innerText = domain
  container.appendChild(name)

  return container
}
