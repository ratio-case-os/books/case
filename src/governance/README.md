# Language Enhancement Proposals

Our languages' concepts and changes each are set in stone in a Language Enhancement Proposal (LEP),
inspired by the [PEP system for Python](https://www.python.org/dev/peps). They describe the
reasoning and functionality and form a reference for all the language's implementations.

LEPs serve as the basis for our languages and therefore require technical specifications. The
[Parsing Expression Grammars by Pest](https://pest.rs/book/grammars/peg.html) are used to describe
the wording side of a proposal. LEPs are proposed and managed within a versioned repository, which
also serves as the way of tracking their revision history. Each proposal contains an abstract,
rationale, technical functionality and concise examples section.

See the [LEP Template](./eslxxxx-lep-template.md) for its layout.

## LEP Types

There are two types of LEP to submit:

- **Standard [S]** : LEPs that describe a new language feature or implementation change.
- **Informational [I]**: LEPs that don't describe any new feature, but rather state **how** the
  language and tools should be used. Think code style and tool guides.

Each submitted LEP is pre-fixed by the abbreviation or extension of the language name (e.g. ESL for
Elephant Specification Language), and suffixed with a number as soon as it is published in some
state (e.g. ESL0001 for the first). These numbers are assigned indefinitely.

## Life of a LEP

Proposing a LEP slightly differs from Python PEP's structure, as our organizational structure
differs as well.

1. Go to the [LEP Template](./eslxxxx-lep-template.md) and click on the edit button at the top of the page. This
   should open it in GitLab's web IDE. Alternatively, clone the repository and edit the template
   there.
1. Fill out the preamble. Stick with the `Draft` status at first.
1. Describe your ideas in the remaining sections.
1. Create a merge request on the [GitLab project](https://gitlab.com/ratio-case-os/docs/case/) to
   the `main` branch as soon as your draft is complete.
1. Basic checks on content, formatting, and style are performed by a reviewer.
   - Early mistakes or objections up to this point may result in the deletion of the LEP without
     further administration.
1. This is the point where discussion on the contents of the LEP start and need to be resolved.
   - Resolving all discussions can lead to the status `Accepted` or `Final` if no code changes to
     the [ESL tools](https://gitlab.com/ratio-case-os/rust/esl) are required.
   - If a LEP requires more triaging or research first, it may be set to `Deferred`.
   - Discussions can also result in a LEP being `Rejected` or `Withdrawn`.
1. Now the LEP is assigned a number and merged to `main` for future reference if it's either
   `Accepted`, `Rejected`, or `Final` and thus is publicly visible on this site.
   - `Withdrawn` LEPs are not merged.
   - For `Accepted` LEPs, create an issue at the
     [ESL tools](https://gitlab.com/ratio-case-os/rust/esl) project for the work that needs to be
     done before its status can be set to `Final`.
1. Implementation in all essential code repositories can lead to status `Final` if not already.
1. Implementation of another LEP may result in status `Obsolete` if the LEP is
   superseded or no longer relevant.

So the life cycle becomes:

```mermaid
stateDiagram-v2
    [*] --> Draft
    Draft --> Deferred
    Deferred --> Draft
    Draft --> Merged
    Draft --> Withdrawn
    Withdrawn --> [*]

    state "Merged to main" as Merged {
        [*] --> Accepted: work to do?
        [*] --> Final: no work to do
        Accepted --> Final: work done
        Final --> Obsolete

        [*] --> Rejected: document why\nfor the future
        Rejected --> [*]

        Obsolete --> [*]
    }

    Merged --> [*]

```

Let's hope we get many great ones in `Final`!
