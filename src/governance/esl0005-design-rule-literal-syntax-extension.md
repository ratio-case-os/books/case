# ESL0005 - Design rule literal syntax extension

| Field           | Value                            |
| :-------------- | :------------------------------- |
| **Author(s)**   | T. Wilschut                      |
| **Reviewer(s)** | A.T. Hofkamp, T.J.L. Schuijbroek |
| **Status**      | Final                            |
| **Type**        | Standard (S)                     |
| **Created**     | 14-12-2019                       |
| **Finalized**   | t.b.d.                           |

!!! warning

    This LEP's syntax enhancement is written using an EBNF syntax as opposed to the current PEG
    syntax since it was finalized before the switch.

## Abstract

In this LEP the design rule syntax is extended to support the specification of place holder values,
targets, and objectives. Placeholder values are added to better support to process of building a
specification over the course of a design project. Value targets and objectives are added to bridge
the gap towards multi-disciplinary optimization methods.

## Motivation

In ESL design rule literals express bounds on the values of variables. One can define (in)equality
requirements and constraints using the set of comparisons `smaller than`, `greater than`, `not equal
to`, `equal to`, `at least`, and `at most` followed by a value or variable that represents the
boundary. This syntax imposes hard boundaries on the values of variables.

However, the actual values for boundaries within design requirements and constraints are may be
unknown at first. In such cases, one usually specifies a placeholder value such as _to be
determined_. During the course of the design project the placeholder values are replaced by the
actual value.

Moreover, hard equality requirements and constraints may increase the difficulty of finding an
initial feasible design. Instead, target values are specified at first. Subsequently, one aims at
minimizing the difference between the actual value of a variable and the value of its target.
Additionally, one specifies objectives for the values of variables. For example, that the value of a
variable must be minimized or maximized. In general, the usage of targets and objectives is common
practice in the field of multi-disciplinary optimization.

Currently, the design rule syntax does not allow for the specification of placeholder values,
targets, and objectives. Hence, in this LEP, the design rule syntax is extended to support these
features as recommended by [@beernaert_from_requirements_2018].

## Rationale

### Targets and objectives

To accommodate for the specification of targets the word `approximately` is added to the set of
comparisons as shown in the following example:

```elephant
design-requirement
  # Target requirement
  dr-0: foo must be approximately 5.0 [m/s^2]
```

which implies that variable `foo` has a target of value of `5.0 [m/s^2]`. This implicitly denotes
the objective of minimizing the difference between the value of `foo` and `5.0`.

To define explicit objectives the pair of objectives `maximized` and `minimized` is added to ESL.
These objectives may follow the predicate within a design-rule literal as shown in the example
below.

```elephant
design-requirement
  # Objectives
  dr-1: faa must be maximized
  dr-2: fuu must be minimized
```

The comparison `approximately` and objectives `maximized` and `minimized` may only be used in the
requirement form of the design-rule literal. As constraints denote limitations on that what is
desired which is denoted by the requirements. Therefore, specifying a target or objective within a
constraint does not make sense.

### Placeholder values

To better support the process of building a specification over the course of a design project the
special value `t.b.d.` (to be determined) is added to the ESL syntax as shown in the following
example:

```elephant
design-requirement
  dr-3: fii must be at most t.b.d.
```

So far, this single special value seems to be sufficient.

## Specification

The introduction of targets, objectives, and placeholder values effects the design rule literal
syntax. The new syntax is defined by the following EBNF listing:

```ebnf
design-rule-literal ::=
    argument-name
  ( constraint-rule-literal | requirement-rule-literal )

constraint-rule-literal ::=
  "is" comparison bound

requirement-rule-literal ::=
  auxiliary-verb "be" ( comparison bound | objective )

comparison ::=
    "smaller" "than"
  | "greater" "than"
  | "not" "equal" "to"
  | "equal" "to"
  | "at" "least"
  | "at" "most"
  | "approximately" # Only in a requirement-rule-literal

bound ::=
    argument-name
  | ( BOUND-VALUE | "t.b.d." ) [ UNIT ]

objective ::=
    "maximized"
  | "minimized"
```

where we now distinguish a `constraint-rule-literal` and a `requirement-rule-literal`. The
`constraint-rule-literal` is not altered. In a `requirement-rule-literal` one can define a
`comparison` `bound` combination or an `objective`. The word `approximately` is added as a
`comparison` option which is only allowed in a requirement rule literal.

## Backwards Compatibility

The proposed changes are additions so no compatibility issues are expected.

## Proof of concept

This section will be added once the proposed changes are implemented.

## Rejected ideas

The following ideas where rejected.

### Has target

The following `has target` syntax has been considered for defining targets within requirement rule
literals.

```elephant
design-requirement
  dr-0: foo has target 5 [-]
```

However, this options is a deviation of the regular predicate-comparison structure of design-rule
literals. Hence we preferred the addition of `approximately` to the set of comparisons.

## Open issues

No open issues were identified.
