# ESLXXXX - LEP Template

| Field           | Value                               |
| :-------------- | :---------------------------------- |
| **Author(s)**   | {(co)author name(s)}                |
| **Reviewer(s)** | {reviewer(s) name(s)}               |
| **Status**      | {Draft}                             |
| **Type**        | {Standard (S) or Informational (I)} |
| **Created**     | {date}                              |
| **Finalized**   | {date}                              |

## Abstract

> A short (~200 word) description of the technical issue being addressed.

## Motivation

> Clearly explain why the existing language specification is inadequate to address the problem that the LEP solves.

## Rationale

> Describe why particular design decisions were made.

## Specification

> Describe the syntax and semantics of any new language feature. Preferably use [Parsing Expression Grammar (PEG)](https://pest.rs/book/grammars/peg.html) snippets. See the current live [ESL grammar](https://gitlab.com/ratio-case/rust/esl/-/blob/main/esl-compiler/src/grammar.pest) as an example.

## Backwards compatibility

> Describe potential impact and severity on pre-existing code of the language's tools implementation.

## Security implications (optional)

> How could a malicious user take advantage of this new feature?

## How to teach this (optional)

> How to teach users, new and experienced, how to apply the LEP to their work. Don't forget potential deprecation warnings.

## Proof of concept

> Clear and concise example that shows that the implementation actually works.

## Rejected ideas

> Why certain ideas that were brought while discussing this LEP were not ultimately pursued.

## Open issues

> Any points that are still being decided/discussed.

## References (optional)

> A collection of URLs used as references through the LEP.
