# ESL0008 - Documentation comment tags

| Field           | Value              |
| :-------------- | :----------------- |
| **Author(s)**   | T. Wilschut        |
| **Reviewer(s)** | T.J.L. Schuijbroek |
| **Status**      | Final              |
| **Type**        | Standard (S)       |
| **Created**     | 23-08-2021         |
| **Finalized**   | t.b.d.             |

!!! warning

    This LEP's syntax enhancement is written using an EBNF syntax as opposed to the current PEG
    syntax since it was finalized before the switch.

## Abstract

In this LEP it is proposed to introduce a lightweight syntax to explicitly tag documentation
comments. This omits the need for a user to post-process documentation comments to extract
information with respect to a certain topic. For example, information regarding design-decisions,
stakeholders, and verification and validation methods.

## Motivation

In general, the systems engineering literature, such as the NASA Systems engineering Handbook
[@kapurch_nasa_2016], advocates the importance of annotating specification elements with information
such as an owner, stakeholders, verification method, validation method, and rational in support of
efficient requirement and stakeholder management.

Currently, an ESL-user has to resort to placing such information within the ESL doc-comment section
and post-processing the text with a custom script to extract information with respect to certain
topics. This route is being used by several active users of ESL.

However, it is desirable to support this natively to increase the user-friendliness and
functionality of ESL in support of the overall Systems Engineering process.

## Rationale

It is proposed to introduce a lightweight syntax to explicitly tag documentation comments for
several reasons.

First of all, the systems engineering literature presents a create variety of (meta) information
that is being attached to elements of system specifications, such as requirements, constraints, and
components. Moreover, the same information is often referred to using different terminology. For
example, some may use the term 'project actor' while others use 'project stakeholder' to refer to
organizations, departments or people that are involved within a design project. As a consequence,
the literature presents a create variety in ontologies that are used to characterize the information
that is contained within system specifications.

Secondly, the primary goal of ESL is to create a structured system specification from which system
architecture models can be derived automatically. It is not meant as a means to create a full
formally defined dependency network between all bits of information that are used throughout the
systems engineering process.

Third of all, it is preferred to keep the number of language elements as small as possible to ensure
ESL will remain an easy to learn and easy to use language.

Therefore, it is proposed to not consider the tags as being elements part of the specification that
need to be (formally) defined, but rather consider them as keys for making sense of the
documentation comments that are attached to a specification element. As a consequence, it is the
responsibility of the user to ensure the consistent usage of tags throughout the specification. At
this time, we are willing to accept that cost to the benefit of flexibility and ease of use.

### Example

The snippet below shows an example specification in which the documentation tags `rationale`,
`stakeholders`, and `expertise` are used. The `@` symbol is used to indicate that the following word
should be considered as being a tag. These tags allow for easy filtering on the reasoning behind the
goal requirement, the people that are involved in stating the requirement, and the expertise needed
to implement the requirement.

```elephant
---8<--- "esl0008.esl"
```

!!! note

    The tags are optional and that one can use the same tag multiple times. All comments
    attached to a specification element tagged with the same tag will be stored within a dictionary
    stored within the `esl_info` property of the
    [`ragraph.generic.Annotations`][ragraph.generic.Annotations] object of the respective
    [`ragraph.node.Node`][ragraph.node.Node] object within
    the output [`ragraph.graph.Graph`][ragraph.graph.Graph].

## Syntax

The snippet below shows the updated EBNF specification of the comment section syntax in which the
optional tag is added. A tag can be added to inline documentation comments as well.

=== "EBNF"

    ```ebnf title="Comment tags syntax proposal"
    ---8<--- "esl0008.ebnf"
    ```

## Backwards Compatibility

As the documentation tags are optional no backwards compatibility issues are expected.

## Proof of concept

The tagged comments are stored within the `esl_info` property of the `annotations` property of the
[`ragraph.node.Node`][ragraph.node.Node] objects within the output
[`ragraph.graph.Graph`][ragraph.graph.Graph].

The snippet below shows the `esl_info` dictionary of node `world.provide-power` generated based on
the [Example](#example) above. From this dictionary one can easily retrieve the information
tagged with `rationale`, `stakeholders` and `expertise` using the `"tagged_comments"` key. Regular
(untagged) comments are stored in `esl_info["comments"]`

```json title="Example output"
---8<--- "esl0008.json"
```

If one converts the ESL specification into a [PDF document](./assets/esl0008.pdf) one can find the
tags as bold headings within the requirement table of goal `world.provide-power`. As such, one can
enrich the output document with as much information as one would like.

## Rejected Ideas

The following ideas were rejected:

- Enforcing the the user to define all allowed tags within a separate `define comment-tag` section.
  For now, we preferred ease of usage and ease of implementation above rigorousness. At some point
  in the future we might reconsider this decision. However, we first want to gain some experience in
  how this functionality will be used in practice.

## Open Issues

No open issues were identified.
