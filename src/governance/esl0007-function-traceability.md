# ESL0007 - Function traceability

| Field           | Value                            |
| :-------------- | :------------------------------- |
| **Author(s)**   | T. Wilschut                      |
| **Reviewer(s)** | A.T. Hofkamp, T.J.L. Schuijbroek |
| **Status**      | Final                            |
| **Type**        | Standard (S)                     |
| **Created**     | 21-03-2020                       |
| **Finalized**   | t.b.d.                           |

!!! warning

    This LEP's syntax enhancement is written using an EBNF syntax as opposed to the current PEG
    syntax since it was finalized before the switch.

## Abstract

This LEP introduces new mathematics to derive traceability dependencies from ESL specifications to
support functional traceability and completeness checks. The goal- and transformation-specifications
domains are merged into a single function domain. Transformation specifications are assigned a set
of sub-functions composed of chains of goal- and transformation-specifications that describe
functional dependency paths between the input and output variables of the parent transformation.

## Motivation

In the literature, one can find many works focused on functional modelling in
engineering[@crilly_function_2013;@summers2017function]. A frequently encountered goal in functional
modelling is achieving functional traceability and completeness. That is, enable engineers to trace
how top level system functions are achieved by functions of system sub-components. This traceability
is required to be able to verify whether the system will actually fulfil the all desired top-level
functions.

In this LEP, we aim to extend and improve the functional traceability features of ESL such that ESL
better aligns with current systems engineering practice. Moreover, by doing so we aim to increase
the usability and consistency of the derived dependency network between goal- and transformation
specifications and allow for the introduction completeness rules.

## Rationale

Currently, ESL supports goal-specification (function) migration as described by _Crilly et
al._[@crilly_function_2013]. That is, goal-specifications that are formulated at decomposition level
$k$ automatically migrate to decomposition level $k' > k$ if the subject or indirect object
(component) of a goal-specification is decomposed. This feature supports function traceability.

Traceability of transformation-specifications (functions) is, however, not fully supported since
transformation-specifications do not automatically migrate through the system specification. While
adding more detail while decomposing a component, a transformation function is realized by a chain
of goal and transformation functions at a more detailed level, but the latter are not formally
linked to the former higher level transformation.

In essence, a transformation-specification $t$ specified at level $k$ is an abstraction of the chain
of goal- and transformation-specifications specified at level $k' > k$. In graph terms,
transformation-specification $t$ is the parent of all the goal- and transformation-specifications
with the chains that describe the transformation of input variables $V_t$ to output variables
$V_t'$. Thus, ESL can support the derivations of traceability dependencies to support functional
traceability. In general, a transformation-specification may have multiple goal- and transformation
specifications as children.

The introduction of function traceability has two consequences. First of all, the goal-and
transformation domains should no longer be treated as separate domains. Goals and transformations
should be considered different types of function within the function domain. This is required to
ensure that a consistent traceability network is obtained. Since all elements within a such a
network to be part of the same domain (dogs cannot have cats as children).

Secondly, goal-specifications should only have indirect dependencies via one or more
transformation-specifications. Otherwise, 'bypasses' occur in the network of function dependencies.
Such bypasses are problematic when performing sequence analysis.

### Example function chain

```mermaid
---8<--- "esl0007-g-t-chain.mmd"
```

For example, this figure shows an example function chain of goal-specification $g$ that states
that component $c$ must provide flow $x$ to component $c'$, transformation-specification $t$ that
states that component $c'$ must transform flow $x$ into flow $y$, transformation-specification $t'$
that states that component $c'$ must transform flow $y$ into flow $z$ and goal-specification $g'$
that states that component $c'$ must provide flow $z$ to component $c''$. This specification defines
the function chain $g \rightarrow t \rightarrow t' \rightarrow g'$. A direct dependency between $g$
and $g'$ would bypass transformation $t$ and $t'$. Such bypasses clutter sequence analysis results
and are therefore undesired.

In summary, ESL can support the derivation of traceability dependencies without the need for any
syntactical changes by merging the goal- and transformation specification domains into a single
function domain and changing the dependency derivation rules.

## Specification

In this section, first the derivations rules to compute the set of children of transformation
specifications are presented. Secondly, modifications to the dependency derivation rules for
goal-specification dependencies, transformations-specification dependencies, goal-transformation
dependencies, and functional dependencies between variables are introduced. As a reference to the
nomenclature please see the [Dependency derivation](../reference/2_dependency-derivations.md)
section of the ESL reference manual. Here only a subset of the nomenclature is explained.

In the remainder of this section, $\mathcal{G}$ is the set of all goal-specifications and
$\mathcal{T}$ is the set of all transformation-specifications and $\mathcal{C}$ is the set of all
components over an instantiated ESL specification. An arbitrary, transformation specification is
given by:

$$t = (c_t, V_t, V_t', S_t) \in \mathcal{T}$$

where $c_t \in \mathcal{T}$ is the subject component, $V_t$ is the set of input variables, $V_t'$ is
the set of output variables, and $S_t$ is the set of subclauses of $t$.

A component is given by:

$$c=(V_c, N_c, C_c, G_c, T_c, D_c, R_c) \in \mathcal{C}$$

where $V_c$ is the set of variables, $N_c$ is the set of needs, $C_c$ is the set of components,
$G_c$ is the set of goal-specifications, $T_c$ is the set of transformation specifications, $D_c$ is
the set of design specifications, and $R_c$ is the set of relations instantiated within the body of
$c$.

### Traceability dependencies

Let $\mathcal{F}=\mathcal{T} \cup \mathcal{G}$ be the set of all transformation-and
goal-specifications over an instantiated ESL specification. The children of
transformation-specification $t \in \mathcal{T}$, denoted $F(t) \subset \mathcal{F}$, have to be
part of a function chain starting with an input variable $v_i \in V_t$ and ending at an output
variable $v_j \in V_t'$. The set of all variables that are part of a path starting at an input
variable $v_i \in V_t$ and ending at an an output variable $v_j \in V_t'$ over the scope of subject
component $c_t$ is given by:

$$V(v_i, v_j, c_t) = \bigcup_{(v', v'') \in \textit{path}(v_i, v_j, c_t)} \{v', v''\}$$

where $path(v_i, v_j, c_t)$ is defined in the
[Dependency derivation](../reference/2_dependency-derivations.md) section.

$V(v_i, v_j, c_t)$ is used to compute the set of children of transformation functions $F(t) \subset
\mathcal{F}$. The set of children of an arbitrary transformation specification $t$ is given by:

$$
\begin{aligned} \begin{array}{rcrcl} F(t)  &   =   & \{ t' & \mid & t = (c_t, V_t, V_t', S_t) \in
\mathcal{T}, \\
      &       &       &      & t' = (c_{t'}, V_{t'}, V_{t'}', S_{t'}) \in \mathcal{T}, \\
      &       &       &      & t \neq t', c_{t'} \in C_{c_t},  v_i \in V_t, v_j \in V_t', \\
      &       &       &      & V_{t'} \cap V(v_i, v_j, c_t) \neq \emptyset, \\
      &       &       &      & V_{t'}' \cap V(v_i, v_j, c_t) \neq \emptyset \}  \\
      & \cup  & \{ g  & \mid & t = (c_t, V_t, V_t', S_t) \in \mathcal{T}, \\
      &       &       &      & g = (c_g, c_g', V_g, S_g) \in \mathcal{G}, \\
      &       &       &      & c_{g} \in C_{c_t}, c_{g}' \in C_{c_t}, v_i \in V_t, v_j \in V_t', \\
      &       &       &      & V_g \cap V(v_i, v_j, c_t) \neq \emptyset\}
\end{array} \end{aligned}
$$

That is, a transformation specification $t'$ is a child of transformation specification $t$ if the
subject component $c_t'$ is a child of subject component $c_t$, there exists a path from input
variable $v_i$ to output variable $v_j$ over the scope of $c_t$ such the the intersection of the set
of input variables $V_{t'}$ and the set of path variables $V(v_i, v_j, c_t)$ and the intersection of
output variables $V_{t'}'$ and the set of path variables $V(v_i, v_j, c_t)$ are non empty sets. In
other words, $t'$ is a child of transformation specification $t$ if it is part of the function chain
describing the transformation of an input variable $v_i$ to an output variable $v_j$.

A goal-specification $g$ is a child of transformation-specification $t$ if the subject component
$c_g$ and indirect object component $c_g'$ are children of subject component $c_t$ and the
intersection of the set of goal variables $V_g$ with $V(v_i, v_j, c_t)$ is a non empty set. In other
words, if one or more variables in $V_g$ are path of an input-output path.

#### Example function chain for traceability dependencies

Goal- and transformation-specifications can be part of multiple function chains describing multiple
parent transformations. For example, the figure below shows two transformations $t_i$ and $t_j$ with
subject component $c$ describing transformations from $x$ to $y$ and $x$ to $z$, respectively.

```mermaid
---8<--- "esl0007-traceability-dependencies.mmd"
```

The three transformations $t'$, $t''$, and $t'''$, where $c_{t'}, c_{t''}, c_{t'''} \in C_c$,
describe these transformations in more detail. $t'$ describes a transformation from $x$ to $q$,
$t''$ describes a transformation from $q$ to $y$, and $t'''$ describes a transformation from $q$ to
$z$. So, $t'$, $t''$, and $t'''$ describe the two transformation paths $x \rightarrow q \rightarrow
y$ and $x \rightarrow q \rightarrow z$. As such, $t' \in F(t_i)$ and $t' \in F(t_j)$ both hold.

Therefore, $t'$ has two parent functions ($t_i$ and $t_j$) as it contributes to the fulfillment of
both of these functions. In other words, the function hierarchies form directed acyclic graphs
rather than tree structures as often used in the literature.

### Dependencies between functions

The introduction of traceability dependencies requires the modification of the derivation rules for
dependencies between goal- and transformation specifications, as discussed in the the
[Rationale](#rationale) section.

Currently, separate sets of goal-function dependencies ($E_\mathrm{g}$),
transformation-specification dependencies ($E_\mathrm{t}$), and goal-transformation mapping
relations ($M_\mathrm{gt}$) are derived. This LEP proposes to replace these sets by a single set of
function dependencies ($E_\mathrm{f}$), which is given by:

$$
\begin{aligned} \begin{array}{rclcl} E_\mathrm{f} & =    & \{ (t_i, t_j) & \mid & t_i = (c, V_i,
V'_i, S_i) \in \mathcal{T}, \\
             &      &               &      & t_j = (c, V_j, V'_j, S_j) \in \mathcal{T}, \\
             &      &               &      & t_i \neq t_j, \\
             &      &               &      & V'_i \cap V_j \neq \emptyset \} \\
             & \cup & \{ (t, g)     & \mid & t = (c_t, V_t, V_t', S_t) \in \mathcal{T}, \\
             &      &               &      & g = (c_g, c_g', V_g, S_g) \in \mathcal{G}, \\
             &      &               &      & V_t' \cap V_g \neq \emptyset \} \\
             & \cup & \{ (g, t)     & \mid & g = (c_g, c_g', V_g, S_g) \in \mathcal{G}, \\
             &      &               &      & t = (c_t, V_t, V_t', S_t) \in \mathcal{T}, \\
             &      &               &      & V_g \cap V_t \neq \emptyset \}
\end{array} \end{aligned}
$$

That is, transformation-specification $t_j$ depends on transformation-specification $t_i$ if the
intersection of output variable set $V_{t_i}'$ and input variable set $V_{t_j}$ $t_j$ is a non-empty
set. Note that, no direct goal-specification dependencies $(g_i, g_j)$ are derived to avoid
bypasses.

A goal-specification $g$ depends on transformation-specification $t$ if the intersection of output
variable set $V_t'$ with variable set $V_g$ is not empty.

Similarly, transformation specification $t$ depends on goal-specification $g$ if the intersection of
variable set $V_g$ with input variable set $V_t$ is not empty.

These new dependency derivations rules imply that only transformation-specifications that share the
same subject component can have direct dependencies. Transformation-specifications that do not have
the same subject component can only have indirect dependencies via goal-specifications.
Goal-specifications can only have indirect dependencies via transformation-specifications.

As a consequence, a user must specify transformation functions at all decomposition levels to obtain
complete function chains and traceability network. This differs from the current convention of only
specifying transformation-specifications within leaf components ($\{ c \mid C_c = \emptyset \}$).
This is a price we are willing to pay as it provides functional traceability and allows for
completeness checks. Completeness checks are further discussed in the next section.

### Functional dependencies between variables

The rules for deriving the traceability dependencies make use of the $path(v_i, v_j, c)$ function
that searches for dependency paths starting at variable $v_i$ and ending at $v_j$ over the scope of
component $c$. To find these paths the set of functional dependencies between variables
$E_\mathrm{v_f}$ is required.

In the [Rationale](#rationale) section, we argued that dependencies 'bypasses' are undesired in the
function domain. These are equally undesired in the variable domain. In fact, dependency bypasses in
the variable domain will by definition yield dependency bypasses in the function domain. Therefore,
the dependency derivation rules for $E_\mathrm{v_f}$ are modified as well, $E_\mathrm{v_f}$ is now
given by:

$$
\begin{aligned} \begin{array}{rlll} E_\mathrm{v_f} = & \{(v_i, v_j) &\mid& (c_t, V_t, V'_t, S_t)
\in \mathcal{T}, \\
                 &              &    & c_t = (V_{c_t}, N_{c_t}, C_{c_t}, G_{c_t}, T_{c_t}, D_{c_t}, R_{c_t}) \in \mathcal{C} \\
                 &              &    & v_i \in V_t, v_j \in V'_t, v_i \neq v_j, C_{c_t} = \emptyset \}  \\
\end{array}\label{eqn:vf} \end{aligned}
$$

That is, variable $v_j$ depends on variable $v_i$ if $v_i$ is a member of input variable set $V_t$,
$v_j$ is a member of output variable set $V_t'$, and the set of child component $C_{c_t}$ is an
empty set. In other words, only transformations of leaf components (those with no sub-components)
are used to derive the functional dependencies between variables.

An additional advantage of the modifications of these derivation rules is that the set
$E_\mathrm{v_f}$ can be used to check the functional completeness of a specification. For example,
assume we have a transformation specification $t = (c_t, V_t, V_t', S_t)$ for which $C_{c_t} \neq
\emptyset$ then $t$ is traceable if $path(v_i, v_j, c_t) \neq \emptyset \ \forall v_i \in V_t, v_j
\in V_t'$. That is, the transformations specified within leaf components of the tree with root $c_t$
should describe a transformation path for all input variables $v_i \in V_t$ to all output variables
$v_j \in V_t'$. The non-existence of such a path implies that the transformation $t$ that should be
fulfilled by $c_t$ is not traceable to the subcomponents of $c_t$. Thus, the specification is
incomplete.

## Backwards Compatibility

This LEP does not require any syntactical changes to ESL. Therefore, no compatibility issues are
expected at the input side of the compiler. However, as the dependency derivation rules are
modified, different dependency networks may be derived. This especially holds for
goal-specifications, as they no longer have direct dependencies. These direct dependencies, however,
could be reconstructed by analyzing the dependency paths via transformation specifications.

## Proof of concept

In this section the proof of concept is shown using a small example specification of a pumping
system. The specification is shown in the listing below. Using this example we first of all aim to
show the derivation of function chains at the first and second decomposition level. Secondly, we aim
to show how transformation requirement `dm-convert-power-potential` (line 48) of component
`drive-mechanism` can be traced down to transformation `ba-convert-potential` (line 78) of component
`power-supply`, goal `ps-provide-power` (line 60) and transformation `bm-convert-power` (line 69) of
component `electric-motor`.

```elephant
## Specification of a drive-mechanism driving a pump.
define type
  mechanical-energy-flow is a real
  electrical-energy-flow is a real
  liquid-material-flow is a real
  chemical-energy-flow is a real

define verb
  provide to
  convert into


world
  variables
    torque is a mechanical-energy-flow
    water-flow is a liquid-material-flow

  components
    pump is a CentrifugalPump with arguments
      * torque
      * water-flow

    drive-mechanism is an ElectricalDriveMechanism with arguments
      * torque

  goal-requirement
    dm-provide-torque: drive-mechanism must provide torque to pump


define component CentrifugalPump
  parameters
    torque is a mechanical-energy-flow
    water-flow is a liquid-material-flow

  transformation-requirements
    cp-convert-torque: must convert torque into water-flow


define component ElectricalDriveMechanism
  parameters
    torque is a mechanical-energy-flow

  variables
    power is an electrical-energy-flow
    power-potential is a chemical-energy-flow

  transformation-requirements
    dm-convert-power-potential: must convert power-potential into torque

  components
    power-source is a Battery with arguments
      * power
      * power-potential

    motor is a BrushlessMotor with arguments
      * power
      * torque

  goal-requirement
    ps-provide-power: power-source must provide power to motor


define component BrushlessMotor
  parameters
    power is an electrical-energy-flow
    torque is a mechanical-energy-flow

  transformation-requirements
    bm-convert-power: must convert power into torque


define component Battery
  parameters
    power-out is an electrical-energy-flow
    power-potential is a chemical-energy-flow

  transformation-constraint
    ba-convert-potential: does convert power-potential into power-out
```

The two figures below show two component - function specification multi-domain matrices. The
function specification parts contains both transformation and goal function specifications.

The first figure shows the component - function specification MDM at decomposition level 1. At this
level the specifications contains two component: `drive-mechanism` and `pump`. These components must
fulfil three functions as shown in the component - function mapping matrix. From the function DSM
one can identify the function-chain `dm-convert-power-potential` $\rightarrow$ `dm-provide-torque`
$\rightarrow$ `cp-convert-torque`, which denotes to conversion of chemical energy into
mechanical-energy (torque), the transfer of mechanical-energy, and finally the conversion of
mechanical energy into a water flow.

<figure markdown>
  ![Component - function multi-domain matrix of a small pump example at level decomposition level 1.](./assets/esl0007-pump-level-1.svg)
  <figcaption>
    Component - function multi-domain matrix of a small pump example at level decomposition level 1.
  </figcaption>
</figure>

The second figure shows the component - function specification MDM at decomposition level 2. Here
the component `drive-mechanism` is decomposed into the sub-components `power-supply` and
`electric-motor`. Note that within the function specification DSM the transformation
`dm-convert-power-potential` has been replaced bij the transformation `ba-convert-potential` of
component `power-supply`, goal `ps-provide-power` and transformation `bm-convert-power` of
`electric-motor`.

<figure markdown>
  ![Component - function multi-domain matrix of a small pump example at level decomposition level 2.](./assets/esl0007-pump-level-2.svg)
  <figcaption>
    Component - function multi-domain matrix of a small pump example at level decomposition level 2.
  </figcaption>
</figure>

One can now identify the function chain `ba-convert-potential` $\rightarrow$ `ps-provide-power`
$\rightarrow$ `bm-convert-power` $\rightarrow$ `dm-provide-torque` $\rightarrow$
`cp-convert-torque`. This chain denotes a longer function chain that starts at `power-supply` and
ends at `pump`.

To make the replacement of transformation functions specified at level $i$ by goal and
transformation functions at level $i+1$ traceable, _traceability dependencies_ are added to the
graph.

The figure below shows the traceability dependencies between
the function specs part of the example specification. Within the matrix one can indeed see that
transformation specification `dm-convert-power-potential` to functions `ba-convert-potential`,
`ps-provide-power`, and `bm-convert-power`.

<figure markdown>
  ![Traceability dependencies between functions specifications of the pump example.](./assets/esl0007-pump-traceability.svg)
  <figcaption>
    Traceability dependencies between functions specifications of the pump example.
  </figcaption>
</figure>

## Rejected ideas

No ideas were rejected.

## Open issues

The following open issues are identified:

### Visualization of dependencies between variables

Dependency set $E_\mathrm{v_f}$ only contains functional dependencies between variables derived from
leaf transformations. When visualizing a multi-domain-matrix at an arbitrary decomposition level $k
< k_\mathrm{max}$, where $k_\mathrm{max}$ is the maximum depth of the component decomposition tree,
not all variables are displayed. As such, abstraction of edges is required in order to obtain a
consistent dependency network between variables at all decomposition levels.
