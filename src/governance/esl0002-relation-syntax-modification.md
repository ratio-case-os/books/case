# ESL0002 - Relation syntax modification

| Field           | Value                            |
| :-------------- | :------------------------------- |
| **Author(s)**   | T. Wilschut                      |
| **Reviewer(s)** | A.T. Hofkamp, T.J.L. Schuijbroek |
| **Status**      | Final                            |
| **Type**        | Standard (S)                     |
| **Created**     | 24-10-2019                       |
| **Finalized**   | t.b.d.                           |

!!! warning

    This LEP's syntax enhancement is written using an EBNF syntax as opposed to the current PEG
    syntax since it was finalized before the switch.

## Abstract

In this LEP the relation parameter syntax is replaced with an extended form stating the type of each
parameter and direction of the data with respect to the relation environment. This introduces
specifying input-output relations. In addition, the interface of a relation is explicitly defined,
which is useful for instantiating a relation as well as improving type checking.

## Motivation

In engineering design detailed (mathematical) analysis is usually required to ensure that a system
will function according to the set requirements. An analysis often consists of defining a
mathematical function that requires a set of input arguments and returns a set of output arguments.
Based on the values of the input arguments, the values of the output arguments are computed. The
values of the output arguments are subsequently (manually) verified against the set requirements.

The current version of ESL contains `relations`, which can be used to denote the existence of a
(mathematical) relation between variables. For example, in the listing below the relation definition
`newtons-second-law` (line 2) is defined. Relation `r1` (line 9) is an instance of
`newtons-second-law` with arguments `f`, `m`, and `a`.

```elephant
define relation
  newtons-second-law

world
  variables
    f, m, a is a real

  relation
    r1: newtons-second-law with arguments ...
      f, m, a
```

Relation `r1` denotes the existence a (mathematical) dependency between variables `f`, `m`, and `a`,
which is described by relation definition `newtons-second-law`. The relation definition name
`newtons-second-law` is considered to be an implicit reference to an external file in which the
mathematical relation is implemented.

The current relation syntax, however, has a few shortcomings. First of all, the expected type of
arguments and the number of arguments of `newtons-second-law` is not specified. As such, the
compiler cannot verify correct use of a relation.

Secondly, a relation definition could be instantiated multiple times with different numbers of
arguments of different types, which could lead to erroneous specifications.

Third of all, relations are considered to be undirected. That is, neither input nor output is
specified. In practice, however, many analyses have distinct input and output variables. In the
literature, input-output relations are often visualized in a parameter dependency structure matrix
(DSM). By sequencing a parameter DSM one typically aims to obtain an optimal sequence in which the
values of the parameters should be determined to minimize design iterations. Supporting such
analysis fits well within the scope of ESL. However, due to the current lack of directionality it
cannot be supported.

Hence, in this LEP, we replace the relation syntax with an extended version to enable type checking,
to ensure one correctly instantiates relations, and enable one to define directed and undirected
relations between variables. The number of parameters of a relations can be defined as a being fixed
or arbitrary.

## Rationale

The amount of information one has one a relation between variables may change during the course of a
design project. At the start of a design project, one may only be aware of the existence of a
relation. Later, one may obtain additional information that denotes the input-output relations.
Therefore, the proposed relation syntax allows for the specification of directed and undirected
relations. This enables users to convert undirected relations into directed relations during the
course of the design project.

For example, the listing below shows an example specification in which two relations are defined and
instantiated following the proposed syntax. At line 2, relation `ohms-law` is defined which requires
parameters `current` and `resistance` as input and returns parameter `voltage` as output. At line 9,
relation `newtons-second-law` is defined which relates parameters `force`, `mass`, and
`acceleration`.

At line 21, `ohms-law` is instantiated as relation `r1` which requires arguments `I` and `R` and
returns argument `U`. At line 28, `newtons-second-law` is instantiated as relation `r2` which
relates arguments `F`, `m`, and `a`.

```elephant
define relation
  ohms-law
    requiring parameters
      current is a real
      resistance is a real
    returning parameter
      voltage is a real

 newtons-second-law
   relating parameters
     force is a real
     mass is a real
     acceleration is a real

 world
   variables
     I, R, U is a real
     F, m, a is a real

 relation
   r1: ohms-law
     requiring arguments
       I
       R
     returning argument
       U

   r2: newtons-second-law
     relating arguments
       F
       m
       a
```

The relations between the parameters of `ohms-law` are directed and follow a clear input-output
pattern. The relation between the parameters of `newtons-second-law` are undirected. That is, the
`requiring` keyword indicates that all parameters that follow are inputs to the relation. The
`returning` keyword indicates that all parameters that follow are outputs of the relation. The
`relating` keyword indicates that one has not decided whether the parameters that follow are input
or output of the relation.

All parameters within relation definitions are assigned a type. The type of each parameter must
match the type of the respective positional argument upon instantiation. For example, the type of
argument `a` must be equal to the type of parameter `acceleration`. This mechanism allows for
automated type checking. That is, the compiler can automatically check if all relations are
instantiated with the correct number and correct type of arguments.

So far, the proposed syntax requires the user to define all relation parameters a priori. However,
many mathematical formulas can take an arbitrary number of arguments. For example, a summation or
balance equation. Therefore, the `one or more` keywords are introduced which indicate that a
relation requires one or more parameters of a certain type. For example, in the following listing
the relation `cost-summation` requires one or more `cost` arguments as input and returns the
variable `total-cost` as argument.

```elephant
define relation
  cost-summation
    requiring parameters
      one or more cost is a real
    returning parameter
      total-cost is a real
```

## Specification

In this section the formal syntax and dependency derivation rules are given and discussed.

The EBNF code block below shows the new syntax for relation definitions and a relation section. A
`relation-definition` starts with the keywords `define` `relation` followed by a new line. The new
line starts with a `RELATION-DEF-NAME`, which is the name of the relation definition. On the
following lines the input, output and undirected parameters lists are specified. A parameter list
starts with one of the keywords `requiring`, `returning` or `relating` followed by the keyword
`parameter` followed by the respective input, output, or undirected parameter list.

```EBNF
relation-definition ::=
    "define" "relation" \n
        { RELATION-DEF-NAME \n
            { ( "requiring" | "returning" | "relating" ) "parameter"  \n
                { ["one" "or" "more"] PARAMETER-NAME "is" ("a" | "an") TYPE-NAME \n }+
            }+ (*At most one requiring, returning and relating.*)
        }+

relation-section ::=
    "relation" \n
        { RELATION-NAME ":" RELATION-DEF-NAME \n
            { ( "requiring" | "returning" | "relating" ) "argument"  \n
                { ARGUMENT-NAME \n }+
            }+ (*At most one requiring, returning and relating.*)
        }+
```

A parameter list consists of one or more lines that start with the optional keywords `one` `or`
`more`, a `PARAMETER-NAME` followed by the keywords `is` `a` or `is` `an` and a `TYPE-NAME`. A
relation definition can have at most one input, one output, and one undirected parameter list. The
order of the parameter lists is unconstrained.

A `relation-section` starts with the keyword `relation` and a new line. Next one can instantiate one
or more relations. A relation instantiation starts with a `RELATION-NAME` followed by a `:`, a
`RELATION-DEF-NAME` and a new line. Subsequently, one must specify the input, output, and undirected
arguments lists.

### Dependency derivations

The new syntax imposes a partitioning of the variables used by a relation. Therefore, a relation $r
\in \mathcal{R}$ is given:

$$r = (V, V', V'') \in \mathcal{R}$$

where $V$ is the set of input variables, $V'$ is the set of output variables and $V''$ is the set of
undirected variables. The partioning requires a reformulation of the set of relation dependencies as
well. The set of dependencies between relations $E_\mathrm{r}$ is given by:

$$
\begin{aligned}
\begin{array}{rll}
E_\mathrm{r} =\{(r_i, r_j) &\mid& r_i = (V_i, V_i', V_i'') \in \mathcal{R},  r_j=(V_j, V_j', V_j'') \in \mathcal{R}, \\
                           &    &  ( V_i' \cup V_i'' ) \cap ( V_j \cup V_j'' ) \neq \emptyset, \\
                           &    &  r_i \neq r_j \}
\end{array}
\end{aligned}
$$

That is, two relations $r_i$ and $r_i$ have a dependency if the intersection of the union of the
output variables $V_i'$ and undirected variables $V_i''$ of relation $r_i$ with the union of the
input variables $V_j$ and undirected variables $V_j''$ of relation $r_j$ is not empty. In other
words, if an output or undirected variable of $r_i$ is an input or undirected variable of $r_j$,
then $r_j$ depends on $r_i$. Note that if $V''$ is empty for all $r \in \mathcal{R}$ that all
relation dependencies become directed.

The set of design dependencies between variables $E_\mathrm{v_d}$ is affected by the partioning as
well. Let $\mathcal{Q}$ be the set of all design rules. Reformulation of $E_\mathrm{v_d}$ then
yields:

$$
\begin{aligned}
\begin{array}{rlll}
E_\mathrm{v_d} = & \{(v_i, v_j) &\mid& (v_i, v_j) \in \mathcal{Q} \} \\
\cup             & \{(v_i, v_j) &\mid& (V, V', V'') \in \mathcal{R}, \\
                 &              &    & v_i \in (V' \cup V''), v_j \in (V \cup V'') \} \\
\end{array}
\end{aligned}
$$

That is, two variables $v_i$ and $v_j$ have a dependency if there exists a design rule $(v_i, v_j)
\in \mathcal{Q}$ or there exists a relation $r = (V, V', V'') \in \mathcal{R}$ such that $v_i$ is a
member of the union of the output variables $V'$ and undirected variables $V''$ and $v_j$ is a
member of the union of the input variables $V$ and undirected variables $V''$ of $r$. Note that if
$V''$ is empty for all $r \in \mathcal{R}$ that all dependencies between variables become directed.

To determine the mapping relations between relations and all other ESL elements one can simply use
the existing formulas and use $V_r = V \cup V' \cup V''$ as the set of variables used by a relation
$r$.

## Backwards Compatibility

This modification will break all existing ESL specifications that contain relation definitions and
instantiations. As such, backwards compatibility issues are expected.

The simplest way to resolve these issues is to add `relating parameters` and parameter type
information to all relation definitions and to add `relating arguments` to all relation
instantiations.

## Proof of concept

The section will be added once the syntax modifications have been implemented.

## Rejected ideas

The following ideas where rejected.

### Explicit link to external files

The name of a relation definition is considered to be an implicit reference to an external file in
which the mathematical relation is implemented. We explored the possibility to add explicit
references to external files. However, such references are not stable over time and differ per file
system. As such, explicit references may quickly _break_ a specification. Therefore, an external
mapping should be created that takes the name of the relation definition, and links it to an
external definition chosen by the user.

## Open issues

The following open issues are identified.

### Format of mapping file

In the [Rejected Ideas](#rejected-ideas) Section, it has been proposed to to create an external
mapping file in which relation definition names are mapped to names of external definitions. The
format of such files has yet to be designed.
