# ESL0001 - Behavior requirements and constraints

| Field           | Value                           |
| :-------------- | :------------------------------ |
| **Author(s)**   | T.J.L. Schuijbroek, T. Wilschut |
| **Reviewer(s)** | A.T. Hofkamp                    |
| **Status**      | Final                           |
| **Type**        | Standard (S)                    |
| **Created**     | 29-04-2019                      |
| **Finalized**   | 12-08-2019                      |

!!! warning

    This LEP's syntax enhancement is written using an EBNF syntax as opposed to the current PEG
    syntax since it was finalized before the switch.

## Abstract

The Elephant Specification Language (ESL) is a language for creating highly structured multi-level
system specifications. ESL version 1.0 is particularly suited to describe the static structure of a
system. Modern systems, however, are usually dynamic in nature. Therefore, behavior requirements and
constraints are introduced in this LEP. Behavior requirements and constraints can be used to
describe the dynamic behavior of systems. That is, they describe **when** (under which conditions)
**what** requirements must be satisfied and **what** constraints hold. The aim of behavior
requirements and constraints is to provide additional detail in the system specifications.
Additionally, we aim to enable engineers to automatically derive, visualize and analyze the dynamic
dependency structure of systems. The dynamic dependency structure and underling behavior
specifications are meant to serve as a structured starting point for modelling and implementing the
dynamic behavior of systems.

## Motivation

In ESL version 1.0 the static structure of a system can be neatly defined in terms of goal- and
transformation-requirements that state the transfer and transformation of flows between and inside
components. Those flows can be subject to design-requirements that state the static bounds on the
values of flows. Similarly, properties of components, for example length and life-span, can be
subject to design-requirements. Together, the goal-, transformation- and design-requirements
describe the static structure of a system.

Modern engineering systems, however, are usually dynamic in nature. That is, they change behavior
and/or state in response to stimuli such as operator input or changes in their environment. As such,
there is a strong need to be able to specify **when** a system should exhibit **what** behavior. In
other words, there is a strong need to be able to specify dynamic bounds on flows.

By adding behavior requirements and constraints to ESL we aim to provide additional detail and
reduce ambiguity in design specifications describing the dynamic behavior of systems. Additionally,
we aim to enable engineers to automatically derive, visualize and analyze the dynamic dependency
structure of systems. The dependency structure and underlying behavior specifications are meant to
serve as a structured starting point for modelling and implementing the dynamic behavior of systems.

!!! note

    The concepts of behavior and state are not limited to the control
    domain. For example, thermal and mechanical responses of a system as a
    result of stimuli are considered to be behavior as well.

## Rationale

In the literature, system behavior is often modeled in conjunction with system functions and system
states. See, for example, the Function-Behavior-State model[@umeda_supporting_1996], the
Structure-Behavior-Function model[@goel_structure_2009;@komoto_framework_2012] and the
requirements engineering book of Hull[@hull_requirements_2017]. These models all have their own
definitions of behavior, function and state.

In light of the literature and the flow based foundation of ESL, function, behavior and state are
defined as:

- **Function:** everything what a 'thing' must be able to do, i.e., all transfers and
  transformations of flow which a 'thing' must be able to perform.
- **Behavior:** everything what a 'thing' must do in response to stimuli, i.e., when which flows
  need to be transferred and transformed in what quantity in response to stimuli. Stimuli are
  changes in (un)controllable flows (state changes).
- **State:** description of the behavior of a 'thing' given certain bounded stimuli, i.e., what
  flows are being transferred and transformed in what quantity given certain bounded stimuli. A
  state change happens when a stimulus crosses the defined boundaries. For example, a sensor signal
  (information flow) switches from high to low.

!!! note

    When following the state definition above, a state is a representation
    of the values of a set of flows within certain bounds. In ESL all flows
    are represented by variables. Bounds are variables as well. A state in
    ESL is specified as a variable bounded by other variables.

For example, the specification below contains three variables: `door-open` and `door-closed` of type
`state` and `door-sensor-voltage` of type `electrical-energy-flow`.

```elephant
define type
  state is a boolean
  electrical-energy-flow is a real

world
  variables
    door-open, door-closed is a state
    door-sensor-voltage is an electrical-energy-flow
```

Here, the variable types enable a reader to easily distinguish the flow variable
`door-sensor-voltage` and the state variables `door-open` and `door-closed`. Such a distinction at
type level is currently sufficient for the goal of ESL to provide a structured starting point for
modelling and implementing the dynamic behavior of systems.

The dynamic behavior itself, however, cannot be described within ESL version 1.0. At best, one can
specify a `sensor-door-behavior-model` relation with variables `door-open`, `door-closed` and
`door-sensor-voltage` as arguments, as shown in the example below.

```elephant
relation
  door-sensor-behavior: sensor-door-behavior-model with arguments ...
    door-open, door-closed, door-sensor-voltage
```

Here, relation `door-sensor-behavior` indicates that there exists a dependency between arguments
`door-open`, `door-closed` and `door-sensor-voltage`. The relation contains no information on what
behavior should be modeled or implemented. With the addition of behavior requirements and
constraints we aim to provide additional structured information on the input-output relation between
these arguments.

### Syntax rationale

In the literature one can find many boilerplates for behavior requirements, see for example these
references[@goel_structure_2009;@mavin2009easy;@arora2014requirement]. In general, boilerplates have
a _"when < A > then < B >"_ like structure. The `when` keyword indicates a set of conditional
statements _A_ that indicate under which conditions requirement set _B_ must be satisfied.

Furthermore, behavior requirements usually describe a set of alternative cases. Such requirements
follow a _"when < A<sub>1</sub> > then < B<sub>1</sub> > or \... or when < A<sub>n</sub> > then <
B<sub>n</sub> >"_ structure. The order of the cases in such a requirement is usually meaningless.
That is, the order of case specification does not impose any precedence constraints.

Following the literature, the behavior syntax as shown in the example below is proposed. In this
example, a behavior requirement named `door-state` is specified. The requirement comprises the two
cases `door-is-closed` and `door-is-open`. The case order has no semantics. Each case is composed of
when-clauses and then-clauses. The when-clauses state the conditions under which the then-clauses
apply. That is, when condition `c1` holds then requirements `r1` an `r2` must be satisfied. When
condition `c2` holds requirements `r3` and `r4` must be satisfied.

```elephant
behavior-requirement
  door-state:
    case door-is-closed:
      when
        * c1: door-sensor-voltage is greater than 0.9 V
      then
        * r1: door-open must be equal to False
        * r2: door-closed must be equal to True

    case door-is-open:
      when
        * c2: door-sensor-voltage is at most 0.9 V
      then
        * r3: door-open must be equal to True
        * r4: door-closed must be equal to False
```

In writing behavior requirements one should strive for **complete** and **deterministic**
requirements. Complete implies that the conditions cover the complete range of stimuli.
Deterministic implies that only one case may apply simultaneously. Note that the example above is
complete and deterministic as conditions `c1` and `c2` cover the full range of possible values of
`door-sensor-voltage` and can never hold simultaneously.

!!! note

    In the [Completeness and determinism](#completeness-and-determinism)
    section we briefly explore to possibilities for the compiler to
    automatically check for completeness and determinism.

#### Fall-back cases

In striving for completeness, exhaustively specifying all possible cases may become a cumbersome and
time consuming task. As such, it is desirable to be able to specify a _fall-back case_. A fall-back
case is similar to the `else` construct used in many programming languages.

A special `when no other case applies` clause is used to enable users to specify fall-back cases.
The example below specifies behavior requirement `output-x` which contains cases `A`, `B` and `C`.
Case `C` is the fall-back case that applies when neither `A` nor `B` applies. In other words, `C`
applies when `y` is at least 5 and at most 10.

```elephant
behavior-requirement
  output-x:
    case A:
      when
        * c1: y is smaller than 5
      then
        * r1: x must be at most 5
    case B:
      when
        * c2: y is larger than 10
      then
        * r2: x must be at most 10
    case C:
      when no other case applies
      then
        * r3: x must be equal to 0
```

Note that the `when no other case applies` when-clause is a short-hand notation for negation of the
when-clauses of case `A` and `B`.

### Behavior, goal and transformation specifications

Behavior specifications are complimentary to goal and transformation specifications. In fact, the
addition of behavior requirements allows for more natural transformation specifications. For
example, assume we have the following three goal-requirements:

```elephant
goal-requirements
  send-control-signal: motor-controller must send motor-control-signal to motor
  provide-power: power-supply must provide power to motor
  provide-torque: motor most provide torque to pump
```

which are dependent on one another. In ESL version 1.0 one has to define a
transformation-requirement `convert-power`, as shown below, to ensure that goal `provide-torque` has
a functional dependency with goals `send-control-signal` and `provide-power`.

```elephant
define component ElectricMotor
  parameters
    motor-control-signal is an information-flow
    power is an electrical-energy-flow
    torque is a mechanical-energy-flow

  transformation-requirement
    convert-power: must convert motor-control-signal and power into torque
```

Physically, however, `motor-control-signal` is not really transformed into `torque` by `motor`. As
such, transformation requirement `convert-power` may seem flawed by readers.

By adding a behavior-requirement, as shown in the listing below, one can separate the physical
function of transforming power into torque from the logical behavior that `motor` must only provide
torque when needed.

```elephant
define component ElectricMotor
  parameters
    motor-control-signal is an information-flow
    power is an electrical-energy-flow
    torque is a mechanical-energy-flow

  transformation-requirement
    convert-power: must convert power into torque

  behavior-requirement
    motor-behavior:
      case motor-on:
        when
          * c1: motor-control-signal is equal to 'on'
        then
          * r1: torque must be equal to 60 NM
      case motor-off:
        when
          * c2: motor-control-signal is equal to 'off'
        then
          * r2: torque must be equal to 0 NM
```

That is, variable `motor-control-signal` is no longer an input argument of `convert-power`. Instead,
it is the subject of conditions `c1` and `c2`. This implies that the desired value of `torque`,
which is an output argument of `convert-power`, is conditionally dependent on the value of
`motor-control-signal`. Consequently, goal `provide-torque` torque has a functional dependency on
goal `provide-power` and a behavior dependency on `send-control-signal`. Since `power` is needed to
provide torque and `motor-control-signal` denotes **when** `torque` must be provided.

## Specification

In this section the formal syntax and dependency derivation rules are given and discussed.

### Syntax

The [Syntax definition](#syntax-definition) below shows the syntax of an ESL behavior section in
Extended Backus-Naur Form (EBNF). A behavior section starts with a behavior type keyword, either
`behavior-requirement` or `behavior-constraint`, followed by a new line. The next line starts with a
`behavior-NAME`, i.e., the identifier of the behavior statement, followed by a colon, a
`behavior-clause` and a new line. Such a pattern is repeated one or more times. A `behavior-clause`
starts with the keyword `case` followed by a `CASE-NAME`, a colon and a new line. On the new line
one may find a `regular-case-clause` or a `fall-back-case-clause`. This pattern may repeat itself
one or more times. A `behavior-clause` may contain at most one `fall-back-case-clause`.

A `regular-case-clause` is composed of a `when-clause` followed by a `then-clause`. The
`when-clause` states the case conditions and is composed of the keyword `when` followed by a new
line. The new line starts with a `CASE-CONDITION-NAME` followed by a colon, a `design-rule-line` and
a new line. A `when-clause` must contain at least one condition.

A `then-clause` states the case design-rules and starts with the keyword `then` followed by a new
line, a `CASE-RULE-NAME`, a colon, a `design-rule-literal` and a new line. Each `then-clause` must
contain at least one design rule. The then-clauses of the cases within the scope of a
behavior-requirement must have the same set of subject variables.

A `fall-back-case-clause` starts with the keyword sequence `when` `no` `other` `case` `applies` and
a new line where one finds a `then-clause`.

#### Syntax definition

=== "EBNF"

    ```ebnf title="Behavior syntax proposal"
    --8<-- "esl0001.ebnf"
    ```

### Dependency derivations

This section describes the formal derivation rules that are used to derive 'behavior dependencies'
between components, goals, transformations, behaviors, variables and combinations thereof. These
dependencies are an addition to the function, design and coordination dependencies described in the
[Dependency derivations](../reference/2_dependency-derivations.md) section of the ESL reference
documentation.

#### Definitions

Let $\mathcal{H}$ be the set of all behavior specifications (requirements and constraints) within an
instantiated ESL specification. Let a single behavior specification $h \in \mathcal{H}$ be given by:

$$h = \{ A_1, A_2, \ldots, A_n \}$$

where, $n$ is the number of cases in $h$ and $A_{i}$ is case $i$. Now note that the when- and
then-clauses are syntactically equal to subclauses which are defined in the [Dependency
derivation](../reference/2_dependency-derivations.md) section of the ESL reference documentation.
As such, a case $A$ is given by:

$$A = (S, S')$$

where $S$ and $S'$ denote the when-clauses and then-clauses, respectively, and are sets of design
rule lines $Q$. A design rule literal $q \in Q$ is given by:

$$q = (v, b)$$

where $v$ is the subject variable and bound $b$ which may be a variable or real, integer, boolean or
string value.

#### Behavior dependencies between variables

In ESL, behavior is expressed in terms of flows which are represented by variables. Therefore,
behavior dependencies between variables are the most fundamental behavior dependencies. Behavior
dependencies between other ESL elements, such as components and goals, can be derived from the
behavior dependencies between variables.

Following the definitions provided in the previous section, the set of behavior dependencies between
variables $E_{\mathrm{v_b}}$ is given by:

$$
\begin{aligned} \begin{array}{rclcl} E_{\mathrm{v_b}} & = & \{ (v, v')  & \mid & (S, S') \in h, h
\in \mathcal{H}, \\
                 &   &             &      & (v, b) \in Q, Q \in S, \\
                 &   &             &      & (v', b') \in Q', Q' \in S'\}
\end{array} \end{aligned}
$$

That is, $E_{\mathrm{v_b}}$ consists of all variable pairs $(v, v')$ for which there exist a
design-rule $(v, b)$ within design rule line $Q$ within when-clauses $S$ and a design-rule $(v',
b')$ within design-rule-set $Q'$ within then-clauses $S'$. In other words, if there exists a case in
which the value of $v'$ is bound by the the value of $v$.

#### Behavior dependencies between components

The set of behavior dependencies between variables $E_{\mathrm{v_b}}$ can used to derive the set of
behavior dependencies between components $E_{\mathrm{c_b}}$, which is given by:

$$
\begin{aligned} \begin{array}{rclcl} E_{\mathrm{c_b}} &   =   & \{ (c_i, c_j)  & \mid & (c_i,
c'_i, V_i, S_i) \in \mathcal{G}, (c_j, c'_j, V_j, S_j) \in \mathcal{G}, \\
                  &       &                &      & v_i \in V_i, v_j \in V_j , \exists path(v_i, v_j, E_{\mathrm{v_b}}), \\
                  &       &                &      & \forall v_k \in path(v_i, v_j, E_{\mathrm{v_b}}), v_k \neq v_i, v_k \neq v_j, \\
                  &       &                &      &  (v, t) \notin M, (v, g) \notin M, t \in \mathcal{T}, g \in \mathcal{G} \} \\
                  & \cup  & \{ (c_i, c_j)  & \mid & (c_i, V_i, V'_i, S_i) \in \mathcal{T}, (c_j, V_j, V'_j, S_j) \in \mathcal{T}, \\
                  &       &                &      & v_i \in V'_i, v_j \in V'_j , \exists path(v_i, v_j, E_{\mathrm{v_b}}), \\
                  &       &                &      & \forall v_k \in path(v_i, v_j, E_{\mathrm{v_b}}), v_k \neq v_i, v_k \neq v_j, \\
                  &       &                &      &  (v, t) \notin M, (v, g) \notin M, t \in \mathcal{T}, g \in \mathcal{G} \} \\
                  & \cup  & \{ (c_i, c_j)  & \mid & (c_i, c'_i, V_i, S_i) \in \mathcal{G}, (c_j, V_j, V'_j, S_j) \in \mathcal{T}, \\
                  &       &                &      & v_i \in V_i, v_j \in V'_j , \exists path(v_i, v_j, E_{\mathrm{v_b}}), \\
                  &       &                &      & \forall v_k \in path(v_i, v_j, E_{\mathrm{v_b}}), v_k \neq v_i, v_k \neq v_j, \\
                  &       &                &      &  (v, t) \notin M, (v, g) \notin M, t \in \mathcal{T}, g \in \mathcal{G} \} \\
                  & \cup  & \{ (c_i, c_j)  & \mid & (c_i, V_i, V'_i, S_i) \in \mathcal{T}, (c_j, c'_j, V_j, S_j) \in \mathcal{G}, \\
                  &       &                &      & v_i \in V'_i, v_j \in V_j , \exists path(v_i, v_j, E_{\mathrm{v_b}}), \\
                  &       &                &      & \forall v_k \in path(v_i, v_j, E_{\mathrm{v_b}}), v_k \neq v_i, v_k \neq v_j, \\
                  &       &                &      &  (v, t) \notin M, (v, g) \notin M, t \in \mathcal{T}, g \in \mathcal{G} \} \\
\end{array} \end{aligned}
$$

wherein, the first set of component pairs denotes those pairs $c_i$ and $c_j$ for which there exist
a goal $(c_i, c'_i, V_i, S_i) \in \mathcal{G}$ and a goal $(c_j, c'_j, V_j, S_j) \in \mathcal{G}$
such that there exists a path over the set of behavior dependencies between variables
$E_{\mathrm{v_b}}$ between a variable $v_i$ in $V_i$ and a variable $v_j$ in $V_j$, such that all
intermediate variables $v_k$ part of this path do not relate to any other transformation $g$ or
transformation $t$. In other words, the value of a flow to be transferred by component $c_j$ is
bound by the value of a flow to be transferred by component $c_i$, without any transformations or
goals in between.

The second set of component pairs denotes those component pairs $c_i$ and $c_j$ for which there
exist a transformation $(c_i, V_i, V'_i, S_i) \in \mathcal{T}$ and a transformation $(c_j, V_j,
V'_j, S_j) \in \mathcal{T}$, such that there exists a path over the set of behavior dependencies
between variables $E_{\mathrm{v_b}}$ between a variable $v_i$ in $V_i$ and a variable $v_j$ in
$V_j$, such that all intermediate variables $v_k$ part of this path do not relate to any other
transformation $g$ or transformation $t$.

The third set of component pairs denotes those pairs $c_i$ and $c_j$ for which there exist a goal
$(c_i, c'_i, V_i, S_i) \in \mathcal{G}$ and a transformation $(c_j, V_j, V'_j, S_j) \in
\mathcal{T}$, such that there exists a path over the set of behavior dependencies between variables
$E_{\mathrm{v_b}}$ between $v_i$ in $V_i$ and a variable $v_j$ in $V'_j$ , such that all
intermediate variables $v_k$ part of this path do not relate to any other transformation $g$ or
transformation $t$.

The fourth set is similar to the third set except here a flow in $V_j$ is bound by an output flow in
$V'_i$.

#### Behavior dependencies between goals

The set of behavior dependencies between goals $E_{\mathrm{g_b}}$ is given by:

$$
\begin{aligned} \begin{array}{rclcl} E_{\mathrm{g_b}} &   =   & \{ (g_i, g_j)  & \mid & g_i =
(c_i, c'_i, V_i, S_i) \in \mathcal{G}, \\
                  &       &                &      & g_j = (c_j, c'_j, V_j, S_j) \in \mathcal{G},   \\
                  &       &                &      & v_i \in V_i, v_j \in V_j, \exists path(v_i, v_j, E_{\mathrm{v_b}}), \\
                  &       &                &      & \forall v_k \in path(v_i, v_j, E_{\mathrm{v_b}}), v_k \neq v_i, v_k \neq v_j, \\
                  &       &                &      &  (v, t) \notin M, (v, g) \notin M, t \in \mathcal{T}, g \in \mathcal{G} \} \\
\end{array} \end{aligned}
$$

That is, $E_{\mathrm{g_b}}$ contains all pairs of goals $g_i$ and $g_j$ for which there exists a
path over the set of behavior dependencies between variables $E_{\mathrm{v_b}}$ between a variable
$v_i$ in $V_i$ and a variable $v_j$ in $V_j$, such that all intermediate variables $v_k$ part of
this path do not relate to any other transformation $g$ or transformation $t$. In other words, if at
least one variable in $V_j$ is bound by a variable in $V_i$.

#### Behavior dependencies between transformations

Similar to goal behavior dependencies, the set of behavior dependencies between transformations
$E_{\mathrm{t_b}}$ is given by:

$$
\begin{aligned} \begin{array}{rclcl} E_{\mathrm{t_b}} &   =   & \{ (t_i, t_j) & \mid & t_i =
(c_i, V_i, V'_i, S_i) \in    \mathcal{T},  t_j = (c_j, V_j,  V'_j, S_j) \in \mathcal{T},  \\
                  &       &                &      & v_i \in V'_i, v_j \in V'_j , \exists path(v_i, v_j, E_{\mathrm{v_b}}), \\
                  &       &                &      & \forall v_k \in path(v_i, v_j, E_{\mathrm{v_b}}), v_k \neq v_i, v_k \neq v_j, \\
                  &       &                &      &  (v, t) \notin M, (v, g) \notin M, t \in \mathcal{T}, g \in \mathcal{G} \} \\
\end{array} \end{aligned}
$$

That is, transformation $t_i$ has a behavior dependency with transformation $t_j$ if there exists a
path over the set of behavior dependencies between variables $E_{\mathrm{v_b}}$ between a variable
$v_i$ in $V'_i$ and a variable $v_j$ in $V'_j$, such that all intermediate variables $v_k$ part of
this path do not relate to any other transformation $g$ or transformation $t$. In other words, the
the output of $t_j$ is bound by the output of $t_i$.

#### Behavior and coordination dependencies between behaviors

Finally, the set of dependencies between behaviors $E_{\mathrm{h_b}}$ is given by:

$$
\begin{aligned} \begin{array}{rclcl} E_{\mathrm{h_b}}   &   =   & \{ (h_i, h_j)  & \mid & (S_i,
S'_i) \in h_i, h_i \in  \mathcal{H}, \\
                    &       &                &      & (S_j, S'_j) \in h_j, h_j \in \mathcal{H}, \\
                    &       &                &      & V'_i = \{ v \mid (v,b) \in Q', Q'  \in S'_i \}, \\
                    &       &                &      & V_j = \{ v \mid (v,b) \in Q, Q \in S_j \}, \\
                    &       &                &      & V'_i \cap V_j \neq \emptyset, h_i \neq h_j \}
\end{array} \end{aligned}
$$

That is, two behaviors $h_i$ and $h_j$ have a behavior dependency if at least one variable that is
used within then-clauses $S'_i$ of $h_i$ is used within when-clauses $S_j$ of $h_j$. Such a
dependency implies that the result of $h_j$ is bounded by the result of $h_i$.

Similarly, coordination dependencies $E_{\mathrm{h_c}}$ are given by:

$$
\begin{aligned} \begin{array}{rclcl} E_{\mathrm{h_c}}   &   =   & \{ (h_i, h_j)  & \mid & (S_i,
S'_i) \in h_i, h_i \in  \mathcal{H}, \\
                    &       &                &      & (S_j, S'_j) \in h_j, h_j \in \mathcal{H},  \\
                    &       &                &      & V'_i = \{ v \mid (v,b) \in Q', Q' \in S'_i \}, \\
                    &       &                &      & V'_j = \{ v \mid (v,b) \in Q', Q' \in S'_j \}, \\
                    &       &                &      & V'_i \cap V'_j \neq \emptyset, , h_i \neq h_j \}
\end{array} \end{aligned}
$$

That is, two behaviors $h_i$ and $h_j$ have a coordination dependency if at least one variable that
is used within a then-clauses $S'_i$ of $h_i$ is used within a then-clauses $S'_j$ of $h_j$. Such a
dependency implies that both behaviors aim to set the values of a least one shared variable.

#### Mapping relations

The mapping relations between behaviors and other ESL elements can simply be derived based on shared
variables. Let $v_Q$ be a function that collects all variables that are used within a design rule
line $Q$, and be given by:

$$v_Q\colon \mathcal{\mathcal{Q}} \rightarrow \mathcal{V} = \{v\ |\ (v,b)\in Q\}$$

Let $v_h$ be a function that collects all variables used within a behavior requirement $h$ and be
given by

$$
v_h\colon \mathcal{H} \rightarrow \mathcal{V} = \{v\ |\ (v,b)\in Q, Q\in(S\cup S'), (S, S')\in
h\}
$$

Now the set of mapping relations of a behaviors is given by:

$$
\begin{aligned} \begin{array}{rcl} M_h & = & \{(c,h)\ |\ c=(V_c, N_c, C_c, G_c, T_c, D_c, R_c,
H_c)\in\mathcal{C}, h\in \mathcal{H}, V_c\cap v_h(h) \not=\emptyset\} \\
 & \cup & \{(g,h)\ |\ g=(c_g, c'_g, V_g, S_g)\in\mathcal{G},
                h\in \mathcal{H}, V_g\cap v_h(h) \not=\emptyset\} \\
 & \cup & \{(t,h)\ |\ t=(e_t, V_t, V'_t, S_t)\in\mathcal{T},
                h\in \mathcal{H}, (V_t\cup V'_t)\cap v_h(h) \not=\emptyset\} \\
 & \cup & \{(d,h)\ |\ d=(Q_d, S_d)\in\mathcal{D},
                h\in \mathcal{H}, v_Q(Q_d)\cap v_h(h) \not=\emptyset\} \\
 & \cup & \{(r,h)\ |\ r=V_r\in\mathcal{R},
                h\in \mathcal{H}, V_r\cap v_h(h) \not=\emptyset\}
\end{array} \end{aligned}
$$

where $\mathcal{C}$ is the set of all components, $\mathcal{G}$ is the set of all goals,
$\mathcal{T}$ is the set of all transformations, $\mathcal{D}$ is the set of all designs,
$\mathcal{R}$ is the set of all relations, and $\mathcal{V}$ is the set of all variables over an
instantiated ESL specification. $V_c$ is the set of all variables used by component $c$, $V_g$ is
the set of all variables used by goal $g$, $V_t$ and $V'_t$ are, respectively, the sets of input and
output variables of transformation $t$, $V_d$ is the set of all variables used by design
specification $d$, and $V_r$ is the set of all variables used by relation $r$.

## Backwards Compatibility

Behavior requirements are an addition to ESL. No changes to existing language concepts are required.
So backwards compatibility with respect to processing specifications written in ESL version 1.0 is
not an issue.

The compiler output, however, contains additional behavior objects and and behavior dependencies.
Post processing tools should be adapted to handle these objects and dependencies.

## Proof of concept

In this section, four examples are shown which illustrate the usage of behavior-specifications.

The following examples build on some common types, verbs and a torque-curve-model relation. We start
with a rudimentary specification without any behavior-requirements. The [Example rudiments
block](#example-rudiments) shows an illustrative example containing a (wireless) `switch` and a
(electric) `motor`. The `switch` must send a `motor-control-signal` to `motor`. The `motor` must
internally convert `power` into `torque`.

### Example rudiments

```elephant title="world.esl"
--8<-- "esl0001-0.esl"
```

These lines are assumed to be given for each example and therefore left out of their respective code
blocks.

### Example 1

In ESL behavior is expressed in terms of (bounded) flows. In the code block below, a
behavior-requirement is added for the `electric-motor`. There, the requirement `motor-torque-output`
denotes that `torque` must equal to _60 Nm_ when `motor-control-signal` is equal to _'on'_. When
`motor-control-signal` is equal to _'off'_, `torque` must be equal to _0 Nm_. Thus, this
behavior-requirement denotes a logical dependency between the value of `motor-control-signal` and
`torque`. Here the values of `motor-control-signal` and `torque` are both discrete.

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "esl0001-1.esl:defname"
      ...
    ---8<--- "esl0001-1.esl:behavior"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "esl0001-1.esl"
    ```

The figure below shows a component - function spec - behavior spec - variable multi-domain-matrix.
The piecharts within the figure denote the logical dependencies and mapping dependencies that have
been derived from the example specification.

<figure markdown>
  ![Example 1's Component-Function-Behavior-Variable MDM showing logical dependencies.](./assets/esl0001_logical_mdm_1.svg)
  <figcaption>
    Example 1's Component-Function-Behavior-Variable MDM showing logical dependencies.
  </figcaption>
</figure>

Component `motor` logically depends on component `switch` (row 2, col 1) as the amount of torque the
motor must produce depends on the control signal sent by the `switch`. This dependency is seen
within the function domain as a logical dependency of transformation specification
`convert-power-into-torque` on goal specification `send-control-signal` (row 3, col 4). Similarly,
within the variable domain (rows, cols 6-8) one can see that variable `torque` logically depends on
variable `motor-control-signal`.

### Example 2

The previous [Example 1](#example-1) contains no information on when the value of
`motor-control-signal` should be equal to _'on'_ or _'off'_. In this example we added
transformation-requirement `convert-voltage-into-control-signal` and behavior-requirement
`motor-control-signal-value`. The transformation requirement denotes that `switch` must internally
convert `switch-circuit-voltage` into `motor-control-signal`.

The behavior-requirement denotes that `motor-control-signal` must be equal to _'on'_ or _'off'_
depending on the `switch-circuit-voltage` value. That is, the moment the `switch-circuit-voltage`
becomes above _1 V_, for example by pressing the switch, `motor` will switch on and produce _60 Nm_
of torque. The moment the `switch-circuit-voltage` drops below _1 V_, for example by releasing the
switch, `motor` will turn off.

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "esl0001-2.esl:switch"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "esl0001-1.esl"
    ```

The figure below shows the updated MDM showing logical dependencies
between components, function specs, behavior specs and variables which are derived from behavior
specs `motor-control-signal-values` and `motor-torque-output`.

<figure markdown>
  ![Example 2's Component-Function-Behavior-Variable MDM showing logical dependencies.](./assets/esl0001_logical_mdm_2.svg)
  <figcaption>
    Example 2's Component-Function-Behavior-Variable MDM showing logical dependencies.
  </figcaption>
</figure>

!!! note

    Transformation-requirement `convert-voltage-to-control-signal` (row 3), behavior-requirement
    `motor-control-signal-value` (row 6), and variable `switch-current-voltage` are added to the
    MDM. Within the behavior-domain (rows, cols 6-7) one can see that behavior spec
    `motor-torque-output` depends on behavior spec `motor-control-signal-value`. Within the variable
    domain (rows, cols 8-11) one can see the logical dependency chain from `switch-circuit-voltage`
    to `motor-control-signal` to `torque`.

### Example 3

In the previous [Example 2](#example-2) the desired of values of `torque` are discrete and time
independent. In certain cases one may want to define continuous time dependent flow values. ESL has
no time concept. Hence on cannot directly express time dependent flow values. However, one can make
use of relations.

In the next code block the behavior requirement `motor-torque-output` is modified and behavior
requirement `motor-state-transitions`, relation `torque-curves`, and variables `start-up-curve` and
`shut-down-curve` are added to the specification.

Relation `torque-curves` returns variables `start-up-curve` and `shut-down-curve` which represent
the continuous time dependent torque values during start-up and shut down of `motor`.

Behavior-requirement `motor-state-transitions` defines the different values of `motor-state`, being
_'start-up'_, _'steady-state'_, _'shut-down'_, and _'off'_, depending on the values of `torque` and
`motor-control-signal`.

Behavior-requirement `motor-torque-output` defines the desired (continuous) values of `torque`
depending on the value of `motor-state`.

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "esl0001-3.esl:motor"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "esl0001-3.esl"
    ```

The next figure shows the updated component - function spec - behavior spec - variable MDM. Note the
additional elements and dependencies within the behavior (rows, cols 6-8) and variable (rows, cols
9-13) domains. Especially, note the bi-directional dependencies between behavior-requirements
`motor-state-transitions` and `motor-torque-output` and variables `motor-state` and `torque`. Hence,
we introduced a feedback mechanism.

<figure markdown>
  ![Example 3's Component-Function-Behavior-Variable MDM showing logical dependencies.](./assets/esl0001_logical_mdm_3.svg)
  <figcaption>
    Example 3's Component-Function-Behavior-Variable MDM showing logical dependencies.
  </figcaption>
</figure>

## Rejected ideas

The following ideas where rejected.

### If-clauses

If-clauses attached to goal-, transformation and design requirements and constraints, as shown in
the listing below.

```elephant
goal-requirement
  provide-foo: blob must provide foo to blab
    if
      < conditions >
    end
```

In this example, the condition relates to the validity of the goal-requirement and not the the
actual value of flow `foo`. This is not consistent with the definitions of function, behavior and
state given in Section [Rationale](#rationale). Additionally, it is preferred to explicitly separate
the function specifications from the behavior specifications, as is common in the literature. Hence
it has been rejected.

### External files

Defining dynamic behavior in external files and linking them via relations. This approach does not
support the derivation of the dynamic structure since we have no control over the structure of the
external files. Hence it has been rejected.

### Extending the relation concept

Extending the `relation` concept to include behavior. For example, a relation definition:

```elephant
define relation xy
  with parameters
    x, y
  is defined as
    when x is equal to 1
      then y must be equal to 1
    when x is equal to 2
      then y must be equal to 2
```

In the systems engineering domain behavior-requirements and relations have different semantics
despite that they are mathematically equivalent. Therefore, we prefer to have behavior and relations
as separate concepts within ESL.

For now, relations remain a means to define miscellaneous dependencies between flow and design
variables. In a later development stage, we foresee a role for relations equivalent to response
functions ( `resfunc` ) of the Psi language[@tosserams_specification_2010]. Thereby, bridging the
gap between requirement engineering, system architecting and MDO applications in an engineering
design context.

### Nested cases

Nested cases, as shown in the code example below, are rejected. In this example, cases `A1` and `A2`
are subordinate to case `A`. That is, for case `A1` to apply the when-clauses of `A` and the
when-clauses of `A1` must hold. For case `A2` to apply the when-clauses of `A` and the when-clauses
of `A2` must hold.

```elephant
behavior-requirement
  case A:
    when
      ...
    then
      case A1:
        when
          ...
        then
          ...
      case A2:
        when
          ...
        then
          ...
  case B:
    ...
```

Nested cases enable users to create very complex nested requirements of arbitrary depth This is not
in line with the general rule of thumb to strive for atomic requirements. As such, for now, we chose
to keep it simple and reject nested cases. If the current solution turns out to be insufficient, we
may reconsider this idea at a later stage.

## Open issues

The following open issues are identified.

### Completeness and determinism

**Completeness** and **determinism** are important aspects concerning behavior requirements. A
complete specification states the desired behavior over the full value range of the stimuli. In a
deterministic specification, at most one set of conditions related to the value of a variable
evaluates to true at the same time.

Automated checking for completeness and determinism is desirable when a specification matures.
However, ESL specifications grow and evolve over the course of a design project. As such, it is
unlikely that an ESL specification is (ever) complete and deterministic during the course of a
design project. Moreover, the actual value of a variable $x$ may depend on many other variables and
may be given by a relation defined in an external file written in another formalism. As such,
evaluating when a condition that includes $x$ evaluates to true is currently not within the
possibilities of ESL.

### Events

In defining control logic on often uses the concept of events. That is, state transitions of
elements within the system trigger state transition of other components within the system.

In [Example 1](#example-1) for example one has to keep pressing the switch for the motor to produce
torque. This may be undesirable since it may quickly drain the battery of the switch. It may be
preferred that pressing the button when the motor is off results in the motor turning on. While
pressing the button when the motor is on results in the motor shutting down. In this case, the state
change of the switch (event) dictates the change in motor control signal rather than the actual
state of the switch. Currently it is not possible to define such behavior.
