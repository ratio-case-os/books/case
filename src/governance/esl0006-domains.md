# ESL0006 - Domains

| Field           | Value                            |
| :-------------- | :------------------------------- |
| **Author(s)**   | T. Wilschut                      |
| **Reviewer(s)** | A.T. Hofkamp, T.J.L. Schuijbroek |
| **Status**      | Final                            |
| **Type**        | Standard (S)                     |
| **Created**     | 13-12-2019                       |
| **Finalized**   | t.b.d.                           |

!!! warning

    This LEP's syntax enhancement is written using an EBNF syntax as opposed to the current PEG
    syntax since it was finalized before the switch.

## Abstract

In [ESL0001](./esl0001-behavior-requirements-and-constraints.md) behavior requirements and
constraints have been introduced which gave raise to the need of being able the verify whether a
specification is complete over the full range of stimuli. Such functionality requires more precise
specification of the domain of variables.

In this LEP, a syntax extension is proposed that enables users to define a domain for each variable
type. All variables inherit the domain of their type. This is a first step in enabling completeness
checks.

## Motivation

In [ESL0001](./esl0001-behavior-requirements-and-constraints.md) behavior requirements and
constraints have been introduced which gave raise to the need of being able the verify whether a
specification is complete over the full range of stimuli. Such functionality requires more precise
specification of the domain of variables. The current implicit domain of $(-\infty, \infty)$ for
integers and reals is insufficient, as no flow in any real system is ever near $\pm\infty$. The
implicit domain of any sequence of characters for strings does not suffice either. Similarly,
feasibility studies require clearly defined domains. Therefore, this LEP proposed a syntax extension
that enables users to explicitly define domains for variable types, which is a first step in
enabling completeness checks and feasibility studies.

## Rationale

Semantically a _type_ denotes the kind of values that a variable of that type can assume. For
example, a real number. A domain denotes the set of possible values that a variable of a certain
type may take. For example, all non-negative real values. In math, the domain of continuous data is
usually indicated by lower and upper boundaries. For discontinuous data enumerations are often used
that list the set of possible values. Constants are typically used to define data that can only
assume a single value. In this LEP, we add such functionality to ESL.

### Intervals

The domain of continuous data may be defined by a closed interval, an open interval or multiple
disjunct intervals. It is proposed to define closed intervals in the following manner:

```elephant
define type
  foo is a real of at least 0.0 and at most 10.0
```

That is, the definition of a type is extended with a lower and upper bound. In this case, a variable
of type `foo` can only assume real values of at least 0.0 and at most 10.0. By omitting either the
lower or upper bound one can create an open interval. For example:

```elephant
define type
  faa is a real of at least 0.0
```

indicates that variables of type `faa` can assume real values of at least 0.0. One can define two
disjunct open intervals by using an `or` construct. For example:

```elephant
define type
  fuu is a real of at most 0.0 or at least 10.0
```

indicates that a variable of type `fuu` can assume real values of at most 0.0 or least 10.0. In
general, the `or` construct can be used to define multiple (open) intervals. For example:

```elephant
define type
  fee is a real of at least -1.0 and at most 0.0 or at least 10.0 and at most 20.0 or at least 100.0
```

indicates that a variable of type `fee` can assume real values of at least -1.0 and at most 0.0 or
at least 10.0 and at most 20.0 or at least 100.0. In other words, the domain of a variable of type
`fee` is defined by two closed and one open interval that are disjunct.

!!! note

    An interval spanned by domain boundaries is different from an interval
    spanned by a set of design-requirements or constraints. An interval
    spanned by domain boundaries indicates all possible values that exist
    while an interval spanned by a set of design-requirements or constraints
    denotes all values that are desired or feasible. Therefore, an interval
    spanned by a set of design-requirements or constraints with respect to a
    certain variable must overlap with the interval spanned by domain
    boundaries of the type of that variable. Otherwise, the design space is
    infeasible.

#### Interval checking

The ESL compiler checks for empty intervals, overlapping intervals and inconsistencies in
sub-intervals. For example:

```elephant
define type
  feo is a real of at least 10.0 and at most 9.0 # ERROR, empty domain.
```

yields an empty interval as no value can satisfy the lower and upper bound. This is not allowed and
yields an error. Since this implies that no value exists for all variables of type `feo` within a
specification. As such, by definition, the specification spans an infeasible design space.

Overlapping domains yield a warning. For example:

```elephant
define type
  fea is a real of at least 0.0 and at most 6.0 or at least 4.0 and at most 10.0
  # WARNING, overlapping domains.
```

defines an overlapping domain as any real value of at least 0.0 and at most 10.0 satisfies the lower
and upper bounds.

Inconsistencies in intervals yield an error. For example,

```elephant
define type
  feu is a real of at least 0.0 and at most 6.0
  foi is a feu of at least 7.0 and at most 8.0 # ERROR, infeasible sub-interval
```

yields an error as the interval defined for `foi` is not a sub-interval of its parent domain `feu`.

#### Interval checking

The ESL compiler checks for empty intervals, overlapping intervals and inconsistencies in
sub-interval. For example:

```elephant
define type
  feo is a real of at least 10.0 and at most 9.0 # ERROR, empty domain.
```

yields an empty interval as no value can satisfy the lower and upper bound. This is not allowed and
yields an error. Since this implies that no value exists for all variables of type `feo` within a
specification. As such, by definition, the specification spans an infeasible design space.

Overlapping domains yield a warning. For example:

```elephant
define type
  fea is a real of at least 0.0 and at most 6.0 or at least 4.0 and at most 10.0
  # WARNING, overlapping domains.
```

defines an overlapping domain as any real value of at least 0.0 and at most 10.0 satisfies the lower
and upper bounds.

Inconsistencies in intervals yield an error. For example,

```elephant
define type
  feu is a real of at least 0.0 and at most 6.0
  foi is a feu of at least 7.0 and at most 8.0 # ERROR, infeasible sub-interval
```

yields an error as the interval defined for `foi` is not a sub-interval of its parent domain `feu`.

### Enumerations

The domain of discontinuous data is usually defined by at set of distinct values. Enumerations are
added to ESL to define the set of distinct values that a discontinuous variable may take. For
example:

```elephant
define type
  fii is an enumeration of red, blue, 1, 2
```

indicates that a variable of type `fii` may assume a value of `red`, `blue`, `1` or `2`. The values
of an enumeration may have heterogenous types.

### Constants

Constants are defined by assigning a distinct value to a variable type. For example:

```elephant
define type
  gravitational-constant is a real equal to 9.81
```

implies that all variables of type `gravitational-constant` have a real value of 9.81.

### Units

By default the implicit `[-]` unit is assumed for all values in domain boundaries, enumerations and
constants. However, as explained in [ESL0003](./esl0003-units.md), in feasibility studies units are
required to verify whether a comparison between a variable and a literal value, or between two
variables makes any sense. Such a verification remains impossible of no units are attached to the
values within the domain of a variable. That is, an upper bound of 10 m/s is different from an upper
bound of 10 km/s. Therefore, units can be specified after the values used within domain boundaries,
enumerations and constants.

For example, the following listing shows several good and bad examples of how to define units within
domain boundaries:

```elephant
define type
  speed is a real with units m/s, km/h
  human-speed is a speed of at least 0.0 [m/s] and at most 10 [m/s]  # Ok
  car-speed is a speed of at least 0.0 [km/h] and at most 220 [km/h] # Ok
  boat-speed is a speed of at least 0.0 [knots] and at most 20.0 [knots] # ERROR,  unit not allowed.
  air-speed is a real of at least 0.0 [km/h] and at most 960.1 [km/h] # ERROR, unit not allowed.
```

That is, first one has to define a type that represents the physical quantity of interest and its
allowed units. Here being `speed` with units `m/s` and `km/h`. Subsequently, one can define types
that represent subdomains of type `speed`. For instance, types `human-speed`, `car-speed` and
`boat-speed` denote subdomains of type `speed`. Here `boat-speed` will yield an error as the used
unit `knots` is not allowed for the physical quantity `speed`. Type `air-speed` will yield an error
as well as the build in type `real` has the default `[-]` unit.

Similarly, one can specify units after values used within constants. For example, in the following
listing the physical quantity `acceleration` is defined with unit `m/s^2`. The type
`gravitational-constant` represents the value of 9.81 `m/s^2` within the `acceleration` domain.

```elephant
define type
  acceleration is a real with unit m/s^2
  gravitational-constant is an acceleration equal to 9.81 [m/s^2]
```

Enumerations are slightly different as the values within an enumeration may be heterogenous in type
and therefore do not span a sub-domain of a parent type. As such, the unit of a value may be
specified directly after the value. For example:

```elephant
define type
   engine-outputs is an enumeration of 10 [Nm], 20 [Nm], 30 [Nm]
```

## Specification

The LEP introduces new syntax for the definition of domain boundaries, enumerations and constants.
These additions effect the `type-definition` section syntax as shown in the following listing:

```ebnf
type-definition ::=
    "define" "type" \n
  { new-type-name \n }+

new-type-name ::=
    TYPE-NAME
  | TYPE-NAME "is" "an" enumeration-specification
  | TYPE-NAME "is" ( "a" | "an" ) TYPE-NAME
      [ unit-specification | range-specification ]

unit-specification ::=
    "with" ( "unit" | "units" ) UNIT { "," UNIT }

enumeration-specification ::=
   "enumeration" "of" VALUE [ "["  UNIT "]" ] { "," VALUE [ "["  UNIT "]" ]}

range-specification ::=
    ( interval-specification | constant-specification )

interval-specification ::=
   "of" interval { "or" interval }

interval ::=
    ( open-interval | closed-interval )

open-interval ::=
    ( lower-bound | upper-bound )

closed-interval ::=
    lower-bound "and" upper-bound

lower-bound ::=
    "at" "least" VALUE [ "["  UNIT "]" ]

upper-bound ::=
    "at" "most" VALUE [ "["  UNIT "]" ]

constant-specification ::=
    "equal" "to" VALUE [ "["  UNIT "]" ]
```

where `enumeration-specification` and `range-specification` are the two major additions. The
`enumeration-specification` allows one to define arbitrary combinations of values as explained in
the [Enumerations](#enumerations) section. A `range-specification` consists of an
`interval-specification` or an `constant-specification`. The `interval-specification` syntax enables
on the define multiple (open) intervals. The `constant-specification` syntax enables one to define a
constant. As explained in [Intervals](#intervals) and [Constants](#constants), respectively.

## Backwards Compatibility

Domain specifications are optional. Therefore no compatibility issues are expected.

## Proof of concept

The section will be added once the syntax modifications have been implemented.

## Rejected ideas

The following ideas where rejected.

### Ranges

In certain situations one may want to define a domain that only contains the even numbers between 1
and 90. In such a case, it becomes elaborate to enumerate all allowed values. In programming
language ranges are usually used to define such a sequence. In ESL could, for example, be defined
as:

```elephant
define type
  foo is a real from 2 to 90 with step size 2
```

However, this is for the time being considered to be too 'techy' for ESL. Therefore, it is currently
not implemented.

### Type unions and intersections

With the addition of domains variable types become sets of variables. As such, one may want to
define new types based on the union or intersection of existing types. However, this is for the time
being considered to be too 'techy' for ESL. Therefore, it is currently not implemented.

## Open issues

Please see the Open Issues section of [ESL0003](./esl0003-units.md).
