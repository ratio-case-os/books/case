# Why ESL?

!!! quote

    _Engineering design concerns the process of creating and optimizing solutions in the form of
    technical systems for problems within the design space spanned by needs, requirements, and
    constraints that are set by material, technological, economic, legal, environmental and
    human-related considerations._ [@pahl_engineering_2013]

The needs, requirements, and constraints are usually specified in text documents written in natural
language, and are often referred to as system specifications[@hull_requirements_2017].
Design engineers have to interpret these system specifications and convert them into technical
designs.

An important step in the conversion of needs, requirements and constraints into technical solutions
is the design of the system's architecture, for multiple reasons[@eggert_engineering_2005]. First of
all, the system's architecture has a significant impact on the system's performance, e.g.,
reliability, availability, maintainability, and cost of the
system[@jiao_product_2007;@lough_risk_2009]. Secondly, insight into the system's architecture is
required for effective change management[@giffin2009change]. That is, requirements often change
during the design process, which may cause re-design of one or more components. Such design changes
may propagate through the system yielding re-design of other components[@jarratt_engineering_2011].
A model of the system architecture can be used to evaluate the impact of design
changes[@clarkson_predicting_2004].

It is common practice to relate needs, requirements, and constraints to technical aspects of the
system[@pahl_engineering_2013;@hull_requirements_2017]. Therefore, engineers need
methods to structure, visualize, and analyze the relations between system architecture, needs,
requirements, and constraints.

_Ulrich_[@ulrich_role_1995] defines system architecture as the mapping of a system's functions to
the physical components within the system, and to the dependencies between those components. The
work of _Wilschut et al._[@wilschut_automated_2018a] shows that the intended system architecture
can be derived from structured function specifications. That is, using a fixed grammar to describe
component functions, we can automatically derive dependencies between components, between functions,
between variables, and combinations thereof, and visualize those dependencies. This presents an
effective method to generate a dependency structure matrix (DSM) model of the system architecture.

This method[@wilschut_automated_2018a], however, is limited to a single level description of the
system. The functions are specified at a single granularity level. Design processes are usually
hierarchical in nature[@estefan2007survey]. Different parts of a system are usually described at
different levels of granularity in system specifications[@maier_model_2017]. As the design process
progresses, the level of granularity of system specifications may change. That is, as more details
of the system become apparent, the grain size at which a system is described becomes
smaller[@eppinger_improving_2014].

Furthermore, in the piece by _Wilschut et al._[@wilschut_automated_2018a] functions of the system at
hand are described and therefore only intended functional dependencies can be derived. In
engineering design, however, the design, behavior and physics of systems is specified and analyzed
as well. It is essential to have insight in the network of design dependencies, e.g. a spatial
dependency between two components that have to fit in a predefined space, logical dependencies, e.g.
(software) dependencies between sensors and actuators, and physical dependencies, e.g., heat
exchange due to dissipation of energy.

## Enter ESL

Therefore, we took the concepts of the earlier piece[@wilschut_automated_2018a] as a basis and
developed the Elephant Specification Language (ESL). ESL is a computer readable language for writing
multiple level system specifications that describe the function, behavior, design and physics of a
systems and its subcomponents at various levels of granularity.

An [ESL-compiler](https://raesl.ratio-case.nl) has been developed which checks the consistency of
ESL specifications and derives dependencies between components, variables, needs,
goal-specifications, transformation-specifications, behavior-specifications, design-specifications,
relations, and combinations thereof across all decomposition levels of the specification, following
a predefined set of mathematical rules.

!!! tip

    For the most up-to-date reference of ESL we invite you to take a look at the
    [Reference](../reference/README.md) pages for the syntax and semantics as well as dependency
    derivation rules. For the evolution of the language and the latest additions, you can check the
    [Governance](../governance/README.md) pages.

## Where to from here?

- ESL itself is explained in more detail in the [Tutorials](../tutorials/README.md),
  [How-to guides](../how-to-guides/README.md) and the [ESL Reference](../reference/README.md).
- The [System architecture modeling and analysis](./02-system-architecture-applications.md) page
  shows how the resulting dependency network can be used to help structure and coordinate your
  design process and enable you to improve your design.
