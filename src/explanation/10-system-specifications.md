# System specifications in detail

Writing "high quality" system specifications is essential to the success of a design
project[@buede_engineering_2009]. Inconsistencies and ambiguities in system specifications may cause
costly and lengthy design iterations. Much has been written on structuring and managing system
specifications by academia and industry
alike[@hooks_writing_1994;@grady2010system;@grady2010system].

This page dives into the context of writing system specifications in general and why a formal
language such as the Elephant Specification Language (ESL) is a valuable addition in the Systems
Engineering landscape. It starts with a dissection of typical system specification concepts and
concludes with a review of available methods tools to capture these. From this, we conclude with the
concepts that ESL aimed to capture at its inception.

!!! tip

    ESL is an evolving language. With each additional enhancement, it's expressive capability grows,
    making it easier to capture detailed requirements of your system. You can keep track of the
    evolution on the [Governance](../governance/README.md) page.

## Needs, requirements, and constraints

A design project starts with capturing the Voice-of-the-Customer in a series of statements usually
referred to as needs[@eppinger2015product]. Needs describe what is wanted by the customer. Needs are
typically qualitative, imprecise, and ambiguous due to their linguistic
origins[@jiao_customer_2006].

In the literature, the term requirement is often used interchangeably with the term need, though
several scholars define requirements to be structured and formalized needs
[@ericson_product-service_2009;@cascini_situating_2013]. Similarly, some describe a process of
gathering customer needs and subsequent elicitation, analysis, and specification of formalized
requirements[@jiao_customer_2006]. What is more, the term constraint is often used interchangeably
with the term requirement[@glinz_non-functional_2007]. In the Oxford dictionary[@oxford2009oxford],
a requirement is defined as a thing demanded, whereas a constraint is defined as a limitation or
restriction. These definitions are consistent with definitions by
Koelsch[@koelsch_requirements_2016]. Constraints may, for instance, be imposed by the laws of
nature, material properties, and the environment in which the system is ought to function.

In ESL, we consider needs to be informal (qualitative) statements on what is desired; requirements
to be formal (quantitative) statements on what is desired; and constraints to be formal statements
that impose limits on what is desired.

_Hull et al._[@hull_requirements_2017] point out the benefits of using consistent language in
specifying needs, requirements, and constraints. These benefits have been recognized by many
authors. For example, _Cohen_[@cohen2012programming] defined linguistic equivalents for mathematical
inequality operators, _Hooks_[@hooks_writing_1994] debates the usage of the word 'shall' in
requirements, and _Van Vliet_[@vliet_software_2008] introduced the MoSCoW method
(Must-o-Should-Could-o-Won't) to create subtle priority differences in requirements. For example, to
distinguish between requirements and preferences. Furthermore, several engineers advocate the usage
of boilerplates, i.e., a fixed layout in which requirements must be
written[@arora2014requirement;@mahmud2017specification].

## Function, behavior, and design

Needs, requirements, and constraints, in the following referred to as specifications, can be divided
into two groups: functional specifications and non-functional
specifications[@koelsch_requirements_2016]. Functional specifications are descriptions of what a
system must do[@fernandes_requirements_2016]. Non-functional specifications describe what a system
must comply to.

_Eisenbart et al._[@eisenbart_functional_2012] reviewed twelve different definitions of system
functions and partitioned them into two groups. The first group are functions that describe a goal
of a system. The second group are functions that describe a transformation of flow, such as
electrical energy or information. In _Automated generation of a function-component-parameter
Multi-Domain Matrix from textual function specifications_[@wilschut_automated_2018a], we defined a
specific grammar for function specifications in both groups to reduce ambiguity. That is, a specific
grammar for writing goal-functions to describe the goal of components with respect to other
components in the system, and a specific grammar for writing transformation-functions to describe
transformations of flow within components. To reduce ambiguity even further, we made use of the
functional basis, developed by _Stone et al._[@stone2000development] and _Hirtz et
al._[@hirtz_functional_2002], to restrict the usage of verb synonyms.

Modern engineering systems are usually dynamic in nature. That is, they change behavior and/or state
in response to stimuli such as operator input or changes in their environment. As such, there is a
strong need to be able to specify when a system should exhibit what behavior and fulfil which
functions. In other words, there is a strong need to be able to specify dynamic bounds on flows.
Therefore, system behavior is often modelled in conjunction with system functions and system states.
See, for example, the Function-Behavior-State model[@umeda_supporting_1996], the
Structure-Behavior-Function model[@goel_structure_2009;@komoto_framework_2012] and the
requirements engineering book of Hull[@hull_requirements_2017].

The literature has less consensus on non-functional specifications which are referred to by a
variety of terms such as performance, quality, and safety
specifications[@koelsch_requirements_2016]. This motivates the increasing popularity of
ontology-driven requirement engineering[@chen_ontology_2013;@dermeval_applications_2015]. That is,
prior to writing a specification one defines the classes of requirements that may occur within a
specification.

We interpret non-functional specifications as descriptions of the conceptual, embodiment, and
detailed design of a system or derivative properties thereof such as capacity, reliability,
availability, and cost of components. The conceptual, embodiment, and detailed design denote
different stages of maturity of a design[@pahl_engineering_2013]. Hence, we refer to non-functional
specifications as _design specifications_ in the remainder of this article.

_Hull et al._[@hull_requirements_2017] note that function and design specifications are often combined
in single sentences. For example, the sentence 'The power source must provide power with a
reliability of 98%' contains the sub-clause 'with a reliability of 98%' that is subordinate to the
main clause 'The power source must provide power'. In this case, the main clause describes the
function, whereas the subclause describes a performance bound on the function.

## Dependencies

In engineering design, systems are usually decomposed into more manageable
components[@pahl_engineering_2013]. It is important to specify and actively manage dependencies
between components to ensure that the components eventually will fit and function
together[@eppinger_improving_2014].

Component dependencies may relate to a wide variety of engineering disciplines such as mechanical,
electrical, thermal, and software engineering[@tilstra_high_2012], making it impossible for a
single person to oversee all dependencies[@sosa_are_2007]. Therefore, dependencies between
components are often visualized and analyzed using graphical models. A major challenge in this
process is to ensure and maintain the consistency between the written specifications and the
graphical models as the design process progresses. This motivated the development of graphical
specification methods such as SysML[@friedenthal2014practical].

Written documents, however, remain the primary means of documentation and communication in
engineering design[@tomiyama_making_2013]. This may be due to the limited scalability of graphical
specification methods[@tosserams_specification_2010]. Therefore, we consider a direct relation
between the written specification and the graphical models as a desirable feature in engineering
design.

## Hierarchies

In the literature one finds many design process models such as the waterfall
model[@royce1987managing], spiral-model[@boehm1988spiral], V-model[@forsberg1991relationship], and
the onion model[@childers1994concurrent]. All these models have in common that one starts with a
high-level description of a system, its functions, and variables. Subsequently, one decomposes the
system, its functions, and variables, until one obtains a specification (design) that is
sufficiently detailed for manufacturing purposes.

Many scientists advocate the usage of separate decomposition trees for the system's components,
functions, and variables[@suh_axiomatic_1998;@dym_brown_2012;@pahl_engineering_2013], and the
subsequent manual mapping of components to functions and variables. Obtaining such a mapping is far
from trivial[@crilly_function_2013]. That is, components may have multiple functions and functions
may be fulfilled by multiple components and involve multiple variables. For that reason, others
introduced new function terminology such as the terms exogenous and endogenous
functions[@crilly_function_2013]. These terms are similar in meaning to the terms goal-function and
transformation function[@eisenbart_functional_2012] which are used by _Wilschut et
al._[@wilschut_automated_2018a]. Exogenous functions describe the purpose of a component with respect
to the components around it, while endogenous functions describe processes internal to the
component.

The paper by _Crilly_[@crilly_function_2013] shows that exogenous (goal) functions may propagate
through nested systems. That is, a function may state that a power source provides power to a lamp.
If one subsequently decomposes the power source into a battery and a holder, one may find out that
it is actually the battery that provides power to the lamp. This means, the function `provide power
to the lamp` is not decomposed into sub-functions but simply assigned to a sub-component of the
power-source.

Endogenous (transformation) functions, however, do not propagate through nested systems but are
decomposed into sub-transformations. For example, a petrol engine must convert gasoline (liquid
material flow) into torque (mechanical energy flow) at a system level. Internally, the subcomponents
of the engine transform gasoline into vapor which is burned to produce heat. The heat is transformed
into pressure which in turn is transformed into torque. In other words, the top-level transformation
is described by a series of sub-level transformations. In systems engineering research, the
importance traceability of top-level functions to functions of sub-components is a frequently
discussed topic. Traceability is required to ensure during the design process that the systems will
meet all top-level functional requirements.

## Existing system specification methods and tools

As discussed in in the previous sections, we argue that a system specification method or tool should
enable a user to specify function specifications, design specifications, and combinations thereof in
terms of needs, requirements, and constraints. Moreover, we consider it desirable to have a direct
relation between written specifications and graphical models to display dependencies between
components, functions, variables, and combinations thereof. Finally, one should be able to easily
decrease the granularity level of the specification as the design process progresses and enable
functions to propagate through the nested systems.

In the literature, a variety of requirement management tools are found, see the piece by _De Gea et
al._[@carrillo_de_gea_requirements_2012] for an extensive overview and classification. As the name
suggests, these tools mainly focus on the management of large volumes of requirements; they do not
consider the conciseness and preciseness of the written specifications themselves. As a consequence,
specifications may still be vague and ambiguous. What is more, in most available tooling users have
to manually create the links between components, functions, and variables (and many other types of
artifacts that one can create within these tools). Typically, no direct relation between the written
specification and the dependency structure exists.

To improve the preciseness and conciseness of the written specifications, several authors advocate
the use of boilerplates[@arora2014requirement;@mahmud2017specification;@hull_requirements_2017].
Boilerplates predefine a fixed format in which the specifications should be written, though, do not
constrain the actual content of the specifications. _Ghosh et al._[@ghosh2016arsenal] and _Mustafa
et al._[@mustafa_automated_2017] develop tooling to automatically convert natural language
specifications such that they are formatted following at set of predefined boilerplates. Manual
checking is needed to verify the correctness of these conversions.

Other researchers aim at improving the quality of manually written specifications using controlled
natural languages. _Kuhn et al._[@kuhn2014survey] provides an overview of more than a hundred
controlled natural languages. Most controlled natural languages are used to simplify and to automate
the translation of user and service manuals in a variety of languages. Only a few focus on system
specifications.

_Clark et al._[@clark2005acquiring], for example, developed a controlled natural language for
writing knowledge databases for artificial intelligence systems. They noticed that many engineers
have difficulties with writing the logical expressions in mathematical form which are stored in such
a database. They developed a language that enables engineers to write natural language rules and to
automatically convert them into logical expressions. _Fuchs et al._[@fuchs2008attempto] developed
Attempto Constrained English (ACE) as a natural language specification tool. ACE supports automatic
conversion to first-order-logic. To our knowledge ACE does not support the structured multi-level
description of systems. _Mavin et al._[@mavin2009easy] developed the Easy Approach to Requirements
Syntax (EARS) which, as we understand them, are automated boilerplates. That is, the tool identifies
keywords such as `shall`,`if`, `where`, and `while`, but the text between those keywords is not
constrained to a predefined format. _Fieler et al._[@feiler2016requirement] developed the language
ReqSpec which resembles a programming language and is specifically built for the construction
industry to perform spatial analysis and design of buildings. Hence, ReqSpec does not support the
concept of system functions. Similar to ReqSpec, the psi-language _Tosserams et
al._[@tosserams_specification_2010] and the z-language _Woodcock et al._[@woodcock1996using] are
domain specific languages. The former is used for specifying the structure of multi-disciplinary
optimization problems and the latter is used for formally describing computing systems.

## Requirements for a formal system specification language

Based on the aforementioned reviews of literature on specification methods and tools, we postulate
that the area of engineering design would benefit from a domain specific language that supports the
following features:

1. The specification of function specifications, behavior specifications, design specifications, and
   combinations thereof in terms of needs, requirements, and constraints of new and existing systems
   at multiple levels of granularity.
1. The formal derivation of dependencies between components, function specifications, behavior
   specifications, design specifications, variables and combinations thereof.
1. The propagation and traceability of function specifications through out the system decomposition
   tree.

Therefore, we present the [Elephant Specification Language (ESL)](../reference/README.md) which is a
language specifically designed to support these features.

## Where to from here?

- ESL itself is explained in more detail in the [Tutorials](../tutorials/README.md),
  [How-to guides](../how-to-guides/README.md) and the [ESL Reference](../reference/README.md).
- Read more about why ESL is a useful addition with respect to existing tools at
  [Why ESL?](./11-why-esl.md).
- The [System architecture modeling and analysis](./02-system-architecture-applications.md) page shows
  how the resulting dependency network can be used to help structure and coordinate your design process and
  enable you to improve your design.
