# System architecture modeling and analysis

This chapter provides a brief overview of system architecture modelling and analysis applications
for the Dependency Structure Matrix (DSM).

## Design process organization and coordination

The main reason for creating a system architecture model is to gain better understanding of the
system such that one can organize and coordinate the design process more effectively and
efficiently. Thereby, improving system quality and reducing engineering time.

The section on
[Dependency Structure Matrices](./01-engineering-design.md#dependency-structure-matrices)
already briefly explained how one can modularize a system by analyzing a product DSM with a
clustering algorithm and how one can schedule design activities by analyzing a process DSM with a
sequencing algorithm. Furthermore, clustering and sequencing results can be used to determine how to
organize design teams or to determine which parts of a system or process can be outsourced.

The figure below shows a generalized overview of DSM types. In addition to product and process DSM,
one can create organization DSM models, multi-domain-matrix (MDM) models, and structure comparison
DSM models.

<figure markdown>
  ![Dependency structure matrix types.](./assets/dsm_types.png)
  <figcaption>
    Dependency structure matrix types.
  </figcaption>
</figure>

An organization DSM shows for example the interactions between people within an organization. In MDM
model multiple DSM types are combined into a single figure. An off-diagonal matrix in an MDM is
named a domain-mapping-matrix (DMM) and relates elements from one domain to elements in another
domain. For example one can combine a product DSM and process DSM into a single figure where the DMM
relates components to activities in the design process.

A structure comparison DSM can be used the compare system architectures of multiple systems. An
$\Sigma$-DSM emphasizes commonalities between system architectures whereas a $\Delta$-DSM emphasizes
differences.

## Modularity Analysis and interface management

One of the most common applications of DSM methods is modularity analysis and interface management.
That is, one aims to determine how to best modularize a system such that the different modules can
be designed as independently as possible. The interfaces that remain between the different modules
need to be carefully managed.

The following figure shows for example that one can identify an integrative (bus) module and two
component modules by reordering of the rows and columns of the DSM.

<figure markdown>
  ![A product DSM example.](./assets/Product_DSM_example.png)
  <figcaption markdown>A product DSM example.[@wilschut_similarity_2018]</figcaption>
</figure>

The two component modules can be independently designed, and possibly out-sourced, as long as proper
agreements are made concerning the interfaces with bus component $c_4$.

The colors of the dependencies within the matrix indicate which engineering disciplines are required
to design the modules and thus need to be part of the design teams.

## Risk analysis

A key performance indicator (KPI) is a variable that represents the quality of a system in terms of
a certain aspect, e.g., its reliability, availability or cost of ownership. In design processes,
resources are often too limited to perform (quantitative) KPI risk analysis for the whole system. A
projection dependency structure matrix (P-DSM) enables one to quickly visually identify modules of
components critical to various KPI's. The critical modules are the focus area of subsequent
(quantitative) KPI risk analysis.

<figure markdown>
  ![Two projection DSMs indicating the areas of interest with respect to reliability and life-cycle-cost, respectively.](./assets/pdsm_example.png)
  <figcaption markdown>Two projection DSMs indicating the areas of interest with respect to reliability and life-cycle-cost, respectively.[@wilschut_similarity_2018]</figcaption>
</figure>

For example, this figure shows a regular DSM on which reliability (left)
and life-cycle-costs (right) impact estimates are projected. These estimates may be a simple low
impact (1), medium impact (3) or high impact (9) rating. The brighter the shade of red, the greater
the impact of that combination of components on the respective KPI. The reliability P-DSM on the
left shows that the combination of components $c_4$, $c_1$ and $c_3$ has the greatest impact on the
reliability of the system. The life-cycle cost (LCC) P-DSM shows that the combination of components
$c_1$, $c_3$ and $c_5$ has the greatest impact on (LCC). The P-DSMs clearly show the area of interest
regarding the respective KPI.

## System architecture comparison

During any design process one has to make many design decisions, that may impact the architecture
and performance of the system. To visualize the effect of design choices on the system architecture,
one can use a comparison dependency structure matrix (C-DSM) as depicted in the
figure below.

<figure markdown>
  ![Schematic comparison DSM showing the differences between two alternative system architectures.](./assets/comparison_dsm.png)
  <figcaption markdown>Schematic comparison DSM showing the differences between two alternative system architectures.[@meeusen_evaluation_2019]</figcaption>
</figure>

The C-DSM displays two system architectures in a single figure. The green and red shaded areas
contain the elements and dependencies of the two options. Grey elements (7-12) are shared among both
architectures. Pink elements (1-3) are only present in the First option. Black elements (13-14) are
only present in the second option. Yellow elements (4-6) are present in both options but have
different dependencies.

As such, the C-DSM provides a compact overview of the architectural differences between two design
alternatives. This overview serves as a 'discussion figure' during expert meetings. That is, the
system experts can discuss the benefits and down sides of each design alternative by systematically
following the figure. This enables them to focus on details while maintaining an overall overview.

## Product family design

A major challenge in product family design is to select a realization strategy that balances the
external variety offered to the customer with the internal complexity of managing the design,
fabrication and maintenance of many different product variants[@jiao_product_2007].

The figure below shows several realization strategies in relation to the input required for the
realization of a product using each strategy. An Engineer-to-Order strategy only requires
legislation and customer requirements as input. Subsequently, each product is re-engineered. An
Assembly-to-Order strategy requires standard products and customer requirements as input.
Subsequently the standard product that best fits the customer requirements is assembled.

<figure markdown>
  ![Several realization strategies.](./assets/realization_strategies.png)
  <figcaption markdown>Several realization strategies.</figcaption>
</figure>

Many companies chose a make-to-order or a configure-to-order strategy to offer the customer variety
and customization options while minimizing internal complexity. The foundation of such a strategy is
a basic product architecture which contains the common core of all product family members. That is,
the set of modules of components and their (functional) dependencies that are present in all
products. The common core may be extended with optional modules. The basic product architecture
should account for these dependencies with optional modules as well to ensure proper and fast
component integration once a customer order has been received. Moreover, a basic product
architecture enables one to standardize common core and optional modules and the dependencies
between them and create a product family platform which further reduces internal complexity.

## Product portfolio similarity analysis

The design of a product family requires the creation of a basic product architecture from which all
product family members can be derived. By analyzing the similarity of products within an existing
product portfolio one can get an indication on which and how many product variants have to be
supported by the architecture.

The figure below shows a three step method to find groups of similar
products within a diverse product portfolio. Each group represents a variant which must be supported
by the basic product architecture.

<figure markdown>
  ![The three steps of the product portfolio similarity analysis.](./assets/similarity_example.png)
  <figcaption markdown>The three steps of the product portfolio similarity analysis.[@wilschut_similarity_2018]</figcaption>
</figure>

First, one has to build a binary characteristic mapping matrix **M**, relating characteristics $k_i$
to products $p_i$. Next, one can calculate a product similarity matrix **S**, indicating how similar
two product are based on the selected characteristics. The third step comprises the pruning of
**S**, joining it with **M** and reordering the matrix with a cluster algorithm. The result provides
groups of similar products and shows which characteristics they possess. Here, the products in the
first group possess $k_1$, $k_2$ and possibly $k_3$. Products in the second group only possess
$k_3$.

## Product portfolio commonality analysis

A product portfolio commonality analysis reveals which modules of components belong to the _common
core_ and which are optional within a group of products that form a product family. The commonality
analysis starts with building $n$ binary dependency structure matrices (DSM), one for each variant.
Next, one can take the sum of the $n$ DSMs to obtain a $\Sigma$-DSM. The higher the value of a
dependency within the $\Sigma$-DSM, the more common that combination of components is with the
product family. Finally, by clustering the $\Sigma$-DSM, one can find modules of components that are
common and modules that are optional. For example, in the figure below, five DSMs are summed into a
$\Sigma$-DSM. Clustering reveals that components $c_4$, $c_1$, $c_3$ and $c_5$ are present in almost all
variants, while $c_2$ and $c_6$ are optional. The common components and their dependencies shape the basic
product architecture for all variants.

<figure markdown>
  ![Example $\Sigma$-DSM](./assets/sigma_dsm.png)
  <figcaption markdown>Example $\Sigma$-DSM.[@wilschut_similarity_2018]</figcaption>
</figure>

## Tooling

To make life a little bit easier we have created several Python Packages for handling, visualizing
and analyzing ESL specifications in particular and graphs in general. You can:

- Check out the [RaGraph Python Package Docs](https://ragraph.ratio-case.nl) to create, manipulate,
  and analyze graphs consisting of nodes and edges that represent system architectures.
- Check out the [RaESL Python Package Docs](https://raesl.ratio-case.nl) to parse and compile ESL
  files. The language itself is explained in more detail in the [Tutorials](../tutorials/README.md),
  [How-to guides](../how-to-guides/README.md) and the [ESL Reference](../reference/README.md).
