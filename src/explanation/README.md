# Explanation

In this section, you will find articles and pages that go more in depth to both the context of our
methods as well as the reasoning behind them. If you follow the pages in chronological order, you
will find some background information regarding engineering design and system architecture modeling.
This is followed by a deep dive into system specifications and our own Elephant Specification
Language (ESL).
