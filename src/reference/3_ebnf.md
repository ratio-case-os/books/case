# EBNF syntax definition

!!! warning

    The new tooling will be using a Parsing Expression Grammar (PEG) instead. For now, the EBNF
    syntax definition is still included here for archiving purposes. The up-to-date PEG can be found
    at [grammar.pest](https://gitlab.com/ratio-case/rust/esl/-/blob/main/esl-compiler/src/grammar.pest).

| Notation                     | Should be read as                     |
| :--------------------------- | :------------------------------------ |
| `"abc"`                      | Literal text `"abc"`                  |
| `\n`                         | Continue at the next line             |
| `DEF`                        | Terminal `DEF`                        |
| `ghi`                        | Expansion of rule `ghi`               |
| `a b`                        | `a` followed by `b`                   |
| <code>a &#124; b</code>      | Either `a` or `b`                     |
| <code>a ( b &#124; c)</code> | `a` followed by either `a` or `b`     |
| `[a]`                        | `a` or nothing                        |
| `{a}`                        | Sequence of zero or more times `a`    |
| `{a}+`                       | Sequence of one or more times `a`     |
| `x ::= b c`                  | `x` is defined as `b` followed by `c` |
| `/* note */`                 | Comment or additional constraint      |

```ebnf title="EBNF syntax definition"
---8<--- "grammar.ebnf"
```
