# ESL Reference

Welcome to the Elephant Specification Language (ESL) reference documentation. ESL is a formal
language to write extremely structured system specifications. These pages explain the fundamental
scientific concepts which form the foundations of ESL, the formal syntax, semantics, and the
dependency derivation rules. It gets into the nitty gritty of things.

!!! tip

    It is recommended to have read the
    [System specifications in detail](../explanation/10-system-specifications.md)
    first, so you have an understanding of the landscape that ESL operates in,
    as well as [Why ESL?](../explanation/11-why-esl.md) for the reason for ESL.

!!! info "User's introduction"

    If you're looking for an introduction from a user's perspective you are probably
    better served by the [tutorials](../tutorials/README.md) or
    [how-to guides](../how-to-guides/README.md).

!!! note

    This reference documentation as well as ESL itself is based on [System Specification and design
    structuring methods for a lock product platform, PhD Thesis, Eindhoven University of Technology,
    Chapter 6, T. Wilschut, 2018](https://research.tue.nl/files/110949154/20181128_Wilschut.pdf).

    It is expanded on and updated according to the
    [ESL Enhancement proposals](../governance/README.md) over time.

## Overview

The language reference is split into two parts. The syntax and semantics of the various concepts of
ESL are explained in the [Syntax and semantics](./1_syntax-semantics.md) section.
In the [Dependency derivations](./2_dependency-derivations.md) section we discuss the mathematical
rules that are used to derive dependencies between ESL concepts such as components, functions, and
variables.
