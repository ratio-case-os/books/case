# Syntax and semantics

In this section, we introduce in an informal way the syntax and semantics of the various ESL
concepts. The syntax and semantics is presented for a selection of ESL concepts from the full ESL
[Parsing Expression Grammar (PEG)](https://pest.rs/book/grammars/peg.html) definition.

!!! tip

    The most up-to-date grammar definition in PEG format can always be found over at
    [the language's tools GitLab project](https://gitlab.com/ratio-case/rust/esl) in the
    [grammar.pest](https://gitlab.com/ratio-case/rust/esl/-/blob/main/esl-compiler/src/grammar.pest) file.

## Components

Many engineers experience difficulties in defining and mapping system, function and variable
decompositions to each other[@crilly_function_2013]. Therefore, ESL only allows for the
definition of a single decomposition: a system decomposition tree (product breakdown structure)
consisting of components. The components represent parts of the system.

Each component must have a definition and must be instantiated as a child component within a
(higher-level) parent component. ESL has the built-in `world` component which is the root of the
decomposition tree. The `world` is not part of the system but represents the environment in which
the system operates, which allows user to describe interactions between the system and its
environment. The component tree (system decomposition) forms the central structure of an ESL
specification. All ESL statements are placed within the component tree. For example, shows a `world`
containing a single component `drive-mechanism` which is an `ElectricalDriveMechanism`, which in
turn is `empty`, i.e., it does not contain any child components (yet).

```esl
world
  component
    drive-mechanism is an ElectricalDriveMechanism

define component ElectricalDriveMechanism
  empty
```

In the listing below, two child components are instantiated within the definition of the
`ElectricalDriveMechanism`. As such, `drive-mechanism` is no longer empty but consists of
`electric-motor`, which is a `BrushlessMotor`, and `power-source`, which is a `Battery`. In turn,
one could define and instantiate child components within the `BrushlessMotor` definition and the
`Battery` definition to add additional layers to the system decomposition.

```esl
world
  component
    drive-mechanism is an ElectricalDriveMechanism

define component ElectricalDriveMechanism
  components
    electric-motor is a BrushlessMotor
    power-source is a Battery

define component BrushlessMotor
  empty

define component Battery
  empty
```

Since the system decomposition tree is the central structure in an ESL specification, one takes a
system centered modelling perspective in writing an ESL specification. That is, components are
viewed as black boxes with inputs and outputs `erden_review_2008`. This may seem to be in
contradiction to scholars who advocate 'solution-free' modelling, such as [@pahl_engineering_2013] and[@stone2000development]. In solution-free modelling, one usually
takes a process centered modelling perspective, in which _processes_ are viewed as black-boxes with
inputs and outputs `erden_review_2008`. However, in an ESL specification, a component may represent
a conceptual thing that must do something within a process. A conceptual component does not impose a
solution. For example, in the example above, one could instantiate a `general-drive-mechanism`
instead of an `ElectricalDriveMechanism` in case one does not want to specify the working-principle
of `drive-mechanism`.

## Types and variables

In engineering design, it is common practice to describe a design in terms of variables. ESL enables
the user to declare variables within components. The variables may represent flows from the
interaction basis [@tilstra_high_2012], such as electrical energy and information, or
properties (attributes) of components, such as length, weight, cost, and reliability.

Each variable must have a type, which needs to be defined by the user. The type of a variable
provides additional information about the nature of that variable. For example, whether the variable
represents a solid or a liquid material flow. Moreover, the variable types enable consistency checks
as variables are passed along multiple levels of the system decomposition tree. ESL supports the
mechanism of actual and formal parameters to pass variables across levels of the decomposition tree,
which is common in programming languages.

In the following ESL example, the type `electrical-energy-flow` is defined in line 2. In line 6, the
variable `power` is declared which has type `electrical-energy-flow`. This actual variable is passed
along as an argument to components `electric-motor` and `power-source` (lines 10, 13). Upon
instantiation of `BrushlessMotor` and `Battery` the formal parameters `input-power` (line 17) and
`output-power` (line 21) are replaced by the actual variable `power`.

```esl
define type
  electrical-energy-flow is a real

define component ElectricalDriveMechanism
  variable
    power is an electrical-energy-flow

  components
    electric-motor is a BrushlessMotor with arguments
      * power

    power-source is a Battery with arguments
      * power

define component BrushlessMotor
  parameter
    input-power is an electrical-energy-flow

define component Battery
  parameter
    output-power is an electrical-energy-flow
```

### Domains

Semantically a _type_ denotes the kind of values that a variable of that type can assume. For
example, a real number. A domain denotes the set of possible values that a variable of a certain
type may take. For example, all non-negative real values. In math, the domain of continuous data is
usually indicated by lower and upper boundaries. For discontinuous data enumerations are often used
that list the set of possible values.

In ESL, the domain of continuous data may be defined by a closed interval, an open interval or
multiple disjunct intervals. Closed intervals can be defined in the following manner:

```esl
define type
  foo is a real of at least 0.0 and at most 10.0
```

That is, the definition of a type is extended with a lower and upper bound. In this case, a variable
of type `foo` can only assume real values of at least 0.0 and at most 10.0. By omitting either the
lower or upper bound one can create an open interval. For example:

```esl
define type
  faa is a real of at least 0.0
```

indicates that variables of type `faa` can assume real values of at least 0.0. One can define two
disjunct open intervals by using an `or` construct. For example:

```esl
define type
  fuu is a real of at most 0.0 or at least 10.0
```

indicates that a variable of type `fuu` can assume real values of at most 0.0 or least 10.0. In
general, the `or` construct can be used to define multiple (open) intervals. For example:

```esl
define type
  fee is a real of at least -1.0 and at most 0.0 or at least 10.0 and at most 20.0 or at least 100.0
```

indicates that a variable of type `fee` can assume real values of at least -1.0 and at most 0.0 or
at least 10.0 and at most 20.0 or at least 100.0. In other words, the domain of a variable of type
`fee` is defined by two closed and one open interval that are disjunct. The ESL compiler checks for
empty intervals, overlapping intervals and inconsistencies in sub-intervals.

> An interval spanned by domain boundaries is different from an interval spanned by a set of
> design-requirements or constraints. An interval spanned by domain boundaries indicates all
> possible values that exist while an interval spanned by a set of design-requirements or
> constraints denotes all values that are desired or feasible. Therefore, an interval spanned by a
> set of design-requirements or constraints with respect to a certain variable must overlap with the
> interval spanned by domain boundaries of the type of that variable. Otherwise, the design space is
> infeasible.

The domain of discontinuous data is usually defined by at set of distinct values. In ESL,
enumerations can be used to define the set of distinct values that a discontinuous variable may
take. For example:

```esl
define type
  fii is an enumeration of red, blue, 1, 2
```

indicates that a variable of type `fii` may assume a value of `red`, `blue`, `1` or `2`. The values
of an enumeration may have heterogenous types.

### Units

By default the implicit `[-]` unit is assumed for all values in domain boundaries, enumerations and
constants. However, as explained in LEP-0003, in feasibility studies units are required to verify
whether a comparison between a variable and a literal value, or between two variables makes any
sense. Such a verification remains impossible of no units are attached to the values within the
domain of a variable. That is, an upper bound of 10 m/s is different from an upper bound of 10 km/s.
Therefore, units can be specified after the values used within domain boundaries, enumerations and
constants.

For example, the following listing shows several good and bad examples of how to define units within
domain boundaries:

```esl
define type
  speed is a real with units m/s, km/h
  human-speed is a speed of at least 0.0 [m/s] and at most 10 [m/s]  # Ok
  car-speed is a speed of at least 0.0 [km/h] and at most 220 [km/h] # Ok
  boat-speed is a speed of at least 0.0 [knots] and at most 20.0 [knots] # ERROR,  unit not allowed.
  air-speed is a real of at least 0.0 [km/h] and at most 960.1 [km/h] # ERROR, unit not allowed.
```

That is, first one has to define a type that represents the physical quantity of interest and its
allowed units. Here being `speed` with units `m/s` and `km/h`. Subsequently, one can define types
that represent subdomains of type `speed`. For instance, types `human-speed`, `car-speed` and
`boat-speed` denote subdomains of type `speed`. Here `boat-speed` will yield an error as the used
unit `knots` is not allowed for the physical quantity `speed`. Type `air-speed` will yield an error
as well as the build in type `real` has the default `[-]` unit.

Similarly, one can specify units after values used within constants. For example, in the following
listing the physical quantity `acceleration` is defined with unit `m/s^2`. The type
`gravitational-constant` represents the value of 9.81 `m/s^2` within the `acceleration` domain.

```esl
define type
  acceleration is a real with unit m/s^2
  gravitational-constant is an acceleration equal to 9.81 [m/s^2]
```

Enumerations are slightly different as the values within an enumeration may be heterogenous in type
and therefore do not span a sub-domain of a parent type. As such, the unit of a value may be
specified directly after the value. For example:

```esl
define type
   engine-outputs is an enumeration of 10 [Nm], 20 [Nm], 30 [Nm]
```

### Properties

In engineering design it is common practice to define properties of components, such as the total
weight, maximum length, and cost. Therefore, ESL contains the `property` keyword. For example, in
the code block below, the `property` keyword is added behind the `length` parameter declaration
(line 14). By doing so, the variable `drive-length` (line 2), which is mapped onto formal parameter
`length` as it is an argument of `drive-mechanism` (line 10), is explicitly marked as being a
property of component `drive-mechanism`. A variable can be a property of at most one component.

```esl
define type
  geometry is a real with unit [mm]

world
  variable
    drive-length is a geometry

  component
    drive-mechanism is an electric-drive-mechanism with argument
      drive-length

define component electric-drive-mechanism
  parameter
    length is a geometry property
```

## Bundles and groups

Many programming and modelling languages contain container data types that enable users to pass
along information in packages. Similarly, ESL contains bundles and groups. Bundles are a special
variable type that enables users to declare multiple variables at once. For example, in the listing
below the type `electrical-power-bundle` (line 5) is a bundle of two fields. One named `U` which is
a `voltage` and one named `I` which is a `current`. The declaration of variable `P` of type
`electrical-energy-bundle` at line 11 implies te declaration of the variables `P.V` and `P.I`.

```esl
define type
  voltage is a real
  current is a real

  electrical-power-bundle is a bundle of
    * U is a voltage
    * I is a current

world
  variable
    P is an electrical-power-bundle
```

Groups can be used to (re)group already declared variables. A variable may be part of multiple
groups. For example, in the listing below the variables `P1` and `P2` of type
`electrical-power-bundle` are declared at line 11. This implicitly implies the declaration of
variables `P1.V`, `P1.I`, `P2.V`, and `P2.I`. At lines 14 tot 20 these variables are re-grouped to
form a group of voltages and a group of currents.

```esl
define type
  voltage is a real
  current is a real

  electrical-power-bundle is a bundle of
    * U is a voltage
    * I is a current

world
  variables
    P1, P2 is an electrical-power-bundle

  variable-groups
    voltages is a group of
      * P1.V
      * P2.V

    currents is a group of
      * P1.I
      * P2.I
```

## Verbs and prepositions

The many synonyms of verbs in natural language may cause ambiguity in system specifications
`deng_function_2002`. Therefore, ESL enforces users to define all verb-preposition combinations that
are allowed in ESL function specifications. It is advised to use verbs from the functional basis of
[@hirtz_functional_2002], which all have a distinct definition.

## Needs

Needs are informal statements on what is desired[@jiao_customer_2006]. A need specification
within ESL must start with a reference to an instantiated component or a reference to a declared
variable. The component or the variable is the subject of the need. All words that follow the
subject are unconstrained, i.e., a user may write pure natural language. A need may, for instance,
describe desired functionality.

## Function specifications

The goal- and transformation-function grammars by [@wilschut_automated_2018a] fit well within
the system centered modelling perspective of ESL as goal- and transformation-functions are
formulated in terms of components and variables. The concepts of goal- and transformation-functions,
as presented by [@wilschut_automated_2018a], are therefore woven into ESL.

Goal-function specifications denote the purpose of components with respect to other components
within the system. ESL enables the specification of goal-functions in requirement form and
constraint form. _Goal-requirements_ state what a to be realized component must do, while
_goal-constraints_ state what an existing component (design) does. The listing below shows an
example goal-function in requirement form (`gr-0`) and in constraint form (`gc-0`). Note that the
only difference between these two forms are the verbs `must` and `does`.

```esl
goal-requirement
  gr-0: Battery must provide power to lamp

goal-constraint
  gc-0: Battery does provide power to lamp
```

The general form of a goal-specification literal (sentence) is defined in the listing below, which
presents the grammar in [Parsing Expression Grammar (PEG)](https://pest.rs/book/grammars/peg.html)
notation:

```pest
goal_line = {
    some_component ~              // Active/sending component
    kw_aux ~                      // "does" (constraint) or "shall"/similar (requirement)
    some_verb ~                   // I.e. "provide"
    some_preposition ~            // I.e. "to"
    some_variable ~               // Variable indicating a flow from active to passive
    (some_variable ~ kw_enum)* ~  // Additional variables
    some_component ~              // Passive/receiving component
    with_subclauses?              // Optional additional clauses
}
```

The first `some_component` denotes the active component, i.e., the component that actively fulfills
what is described by the goal-specification. The `kw_aux` must be one of _shall_, _must_,
_should_, _could_, or _won't_. The `VERB` and its associated `PREPOSITION` denote the action
performed by the active component. The `argument-references` is a list of arguments, i.e., either
variables or parameters, that quantify the goal specification. The second `COMPONENT-NAME` denotes
the passive component, i.e., the component that passively contributes to fulfilling what is
described by the goal-specification. The `subclauses` are optional and state additional design
specifications that are subordinate to the main clause. For example, goal-requirement `g1` in the
listing below states that active component `drive-mechanism-d` must transfer `torque` to passive
component `pump-p` with a `steady-state-torque` equal to 40 Nm (indicating a single operating point)
and `reliability` of at least 95%. T

```esl
goal-requirement
  g1: drive-mechanism-d must transfer torque to pump-p with subclauses
      * c1: steady-state-torque must be equal to 40 Nm
      * c2: reliability must be at least 95 %
```

The grammar of `subclauses` is defined in the listing below:

```ebnf
subclauses ::=
  "with" "subclauses" \n
  { "*" SUBCLAUSE-NAME ":"
    comparison-rule-line \n
  }+
```

The subclause section starts with the keywords `with subclauses` followed by a new line and one or
more subclauses, each consisting of a `SUBCLAUSE-NAME` and a `comparison-rule-line`. The
`comparison-rule-line` is defined in section [Design Specifications](#design-specifications).

Transformation specifications describe the internal conversion processes within a component. ESL
provides for the specification of _transformation-requirements_ and _transformation-constraints_.
Similar to the goal specifications, transformation-requirements describe what a desired component
must do internally, while transformation-constraints describe what an existing component (design)
does internally. The listing below shows an example transformation-function in requirement form
(`tr-0`) and in constraint form (`tc-0`). Note that again the only difference between the two forms
are the verbs `must` and `does`.

```esl
transformation-requirement
  tr-0: must convert power to light

transformation-constraint
  tc-0: does convert power to light
```

The grammar of a transformation literal (sentence) is defined in the listing below.

```ebnf
transformation-literal ::=
  ( "does"           % constraint
  | auxiliary-verb ) % requirement
    VERB argument-references
    PREPOSITION argument-references
  ( \n | subclauses )
```

Transformation literals are to be specified in the body of the component _definition_. Therefore, a
`transformation-literal` starts with the verb `does`, in case one wishes to specify a constraint, or
starts with an `auxiliary-verb`, in case one wishes to specify a requirement. The transformation
literal subsequently consists of a user-defined `VERB` with `argument-references` representing the
transformation input parameters, a user defined `PREPOSITION` followed by `argument-references`
representing the transformation outputs, and optionally `subclauses`.

For example, transformation-requirement in the listing below states that each instantiated
`BrushlessMotor` must internally convert `power` into `torque`.

```esl
define component BrushlessMotor
  parameter
     power is an electrical-energy-flow
     torque is a mechanical-energy-flow

  transformation-requirement
    t1: must convert power into torque
```

## Behavior specifications

Modern engineering systems, however, are usually dynamic in nature. That is, they change behavior
and/or state in response to stimuli such as operator input or changes in their environment. As such,
there is a strong need to be able to specify when a system should exhibit what behavior. In other
words, there is a strong need to be able to specify dynamic bounds on flows.

In the literature, system behavior is often modelled in conjunction with system functions and system
states. See, for example, the Function-Behavior-State model [@umeda_supporting_1996], the
Structure-Behavior-Function model [@goel_structure_2009;@komoto_framework_2012] and
the requirements engineering book of Hull[@hull_requirements_2017]. These models all have
their own definitions of behavior, function and state.

In light of the literature and the flow based foundation of ESL, function, behavior and state are
defined as:

- **Function:** everything what a 'thing' must be able to do, i.e., all transfers and
  transformations of flow which a 'thing' must be able to perform.
- **Behavior:** everything what a 'thing' must do in response to stimuli, i.e., when which flows
  need to be transferred and transformed in what quantity in response to stimuli. Stimuli are
  changes in (un)controllable flows (state changes).
- **State:** description of the behavior of a 'thing' given certain bounded stimuli, i.e., what
  flows are being transferred and transformed in what quantity given certain bounded stimuli. A
  state change happens when a stimulus crosses the defined boundaries. For example, a sensor signal
  (information flow) switches from high to low.

!!! note

    When following the state definition above, a state is a representation of the values of a set of
    flows within certain bounds. In ESL all flows are represented by variables. Bounds are variables
    as well. A state in ESL is specified as a variable bounded by other variables.

In the literature one can find many boilerplates for behavior requirements, see for example [@goel_structure_2009], `mavin2009easy` and[@arora2014requirement]. In general, boilerplates
have a _when \< A \> then \< B \>_ like structure. The `when` keyword indicates a set of conditional
statements _A_ that indicate under which conditions requirement set _B_ must be satisfied.

Furthermore, behavior requirements usually describe a set of alternative cases. Such requirements
follow a _when \< A_~1~ _\> then \< B_~1~ _\> or \... or when \< A_~n~ _\> then \< B_~n~ _\>_
structure. The order of the cases in such a requirement is usually meaningless. That is, the order
of case specification does not impose any precedence constraints.

Following the literature, the behavior syntax as shown in the example below is proposed. In this
example, a behavior requirement named `door-state` is specified. The requirement comprises the two
cases `door-is-closed` and `door-is-open`. The case order has no semantics. Each case is composed of
when-clauses and then-clauses. The when-clauses state the conditions under which the then-clauses
apply. That is, when condition `c1` holds then requirements `r1` an `r2` must be satisfied. When
condition `c2` holds requirements `r3` and `r4` must be satisfied.

```esl
behavior-requirement
  door-state:
    case door-is-closed:
      when
        c1: door-sensor-voltage is greater than 0.9 V
      then
        r1: door-open must be equal to False
        r2: door-closed must be equal to True

    case door-is-open:
      when
        c2: door-sensor-voltage is at most 0.9 V
      then
        r3: door-open must be equal to True
        r4: door-closed must be equal to False
```

In writing behavior requirements one should strive for complete and deterministic requirements.
Complete implies that the conditions cover the complete range of stimuli. Deterministic implies that
only one case may apply simultaneously. Note that the example above is complete and deterministic as
conditions `c1` and `c2` cover the full range of possible values of `door-sensor-voltage` and can
never hold simultaneously.

### Fall-back cases

In striving for completeness, exhaustively specifying all possible cases may become a cumbersome and
time consuming task. As such, it is desirable to be able to specify a _fall-back case_. A fall-back
case is similar to the `else` construct used in many programming languages.

A special `when no other case applies` clause is used to enable users to specify fall-back cases.
The example below specifies behavior requirement `output-x` which contains cases `A`, `B` and `C`.
Case `C` is the fall-back case that applies when neither `A` nor `B` applies. In other words, `C`
applies when `y` is at least 5 and at most 10.

```esl
behavior-requirement
  output-x:
    case A:
      when
        c1: y is smaller than 5
      then
        r1: x must be at most 5
    case B:
      when
        c2: y is larger than 10
      then
        r2: x must be at most 10
    case C:
      when no other case applies
      then
        r3: x must be equal to 0
```

Note that the `when no other case applies` when-clause is a short-hand notation for negation of the
when-clauses of case `A` and `B`.

### Behavior, goal and transformation specifications

Behavior specifications are complimentary to goal and transformation specifications. In fact, the
addition of behavior requirements allows for more natural transformation specifications. For
example, assume we have the following three goal-requirements:

```esl
goal-requirements
  send-control-signal: motor-controller must send motor-control-signal to motor
  provide-power: power-supply must provide power to motor
  provide-torque: motor most provide torque to pump
```

which are dependent on one another. In ESL version 1.0 one has to define a
transformation-requirement `convert-power`, as shown below, to ensure that goal `provide-torque` has
a functional dependency with goals `send-control-signal` and `provide-power`.

```esl
define component electric-motor
  parameters
    motor-control-signal is an information-flow
    power is an electrical-energy-flow
    torque is a mechanical-energy-flow

  transformation-requirement
    convert-power: must convert motor-control-signal and power into torque
```

Physically, however, `motor-control-signal` is not really transformed into `torque` by `motor`. As
such, transformation requirement `convert-power` may seem flawed by readers.

By adding a behavior-requirement, as shown in the listing below, one can separate the physical
function of transforming power into torque from the logical behavior that `motor` must only provide
torque when needed.

```esl
define component ElectricMotor
  parameters
    motor-control-signal is an information-flow
    power is an electrical-energy-flow
    torque is a mechanical-energy-flow

  transformation-requirement
    convert-power: must convert power into torque

  behavior-requirement
    motor-behavior:
      case motor-on:
        when
          c1: motor-control-signal is equal to 'on'
        then
          r1: torque must be equal to 60 NM
      case motor-off:
        when
          c2: motor-control-signal is equal to 'off'
        then
          r2: torque must be equal to 0 NM
```

That is, variable `motor-control-signal` is no longer an input argument of `convert-power`. Instead,
it is the subject of conditions `c1` and `c2`. This implies that the desired value of `torque`,
which is an output argument of `convert-power`, is conditionally dependent on the value of
`motor-control-signal`. Consequently, goal `provide-torque` torque has a functional dependency on
goal `provide-power` and a behavior dependency on `send-control-signal`. Since `power` is needed to
provide torque and `motor-control-signal` denotes **when** `torque` must be provided.

### Syntax

The [Behavior EBNF]() listing below shows the syntax of an ESL behavior section in Extended
Backus-Naur Form (EBNF). A behavior section starts with a behavior type keyword, either
`behavior-requirement` or `behavior-constraint`, followed by a new line. The next line starts with a
`behavior-NAME`, i.e., the identifier of the behavior statement, followed by a colon, a
`behavior-clause` and a new line. Such a pattern is repeated one or more times. A `behavior-clause`
starts with the keyword `case` followed by a `CASE-NAME`, a colon and a new line. On the new line
one may find a `regular-case-clause` or a `fall-back-case-clause`. This pattern may repeat itself
one or more times. A `behavior-clause` may contain at most one `fall-back-case-clause`.

A `regular-case-clause` is composed of a `when-clause` followed by a `then-clause`. The
`when-clause` states the case conditions and is composed of the keyword `when` followed by a new
line. The new line starts with a `CASE-CONDITION-NAME` followed by a colon, a `comparison-rule-line`
and a new line. A `when-clause` must contain at least one condition.

A `then-clause` states the case design-rules and starts with the keyword `then` followed by a new
line, a `CASE-RULE-NAME`, a colon, a `comparison` and a new line. Each `then-clause` must contain at
least one design rule. The then-clauses of the cases within the scope of a behavior-requirement must
have the same set of subject variables.

A `fall-back-case-clause` starts with the keyword sequence `when` `no` `other` `case` `applies` and
a new line where one finds a `then-clause`.

```{#Behavior EBNF .ebnf caption="Syntax of an ESL behavior section in Extended Backus-Naur Form (EBNF)."}
behavior-section ::=
  behavior-type \n
  { BEHAVIOR-NAME ":" \n
    behavior-clause
  }+

behavior-type ::=
  "behavior-requirement" | "behavior-constraint"

behavior-clause ::=
  {
    "case" CASE-NAME ":" \n
    ( regular-case-clause
    | fall-back-case-clause
    )
  }+ /* At most one fall-back-case-clause per behavior-clause. */

regular-case-clause ::=
  when-clause
  then-clause

when-clause ::=
  "when" \n
    { "*" CASE-CONDITION-NAME ":" comparison-rule-line \n }+ /* Only 'is' as auxiliary verb. */

then-clause ::=
  "then" \n
    { "*" CASE-RULE-NAME ":" comparison \n }+

fall-back-case-clause ::=
  "when" "no" "other" "case" "applies" \n
  then-clause
```

## Design specifications

_Design specifications_ denote bounds on the values of variables. The listing below shows an example
design-specification in requirement form and in constraint form. The verbs `must` and `is` indicate
the difference between the requirement form (`dr-0`) and the constraint form (`dc-0`).

```esl
design-requirement
  dr-0: power must be at least 300 [W]

design-constraint
  dc-0: power is at least 300 [W]
```

The grammar of a `design-rule-line` is defined as:

```ebnf
design-section ::=
    design-rule-type \n
  { DESIGN-RULE-NAME ":"
    design-rule-line }+

design-rule-type ::=
    "design-requirement"
  | "design-constraint"

design-rule-line ::=
  comparison-rule-line
  ( \n | subclauses )

comparison-rule-line ::=
  comparison
  { "or" comparison }

comparison ::=
  argument-name
  ( constraint-rule-literal | requirement-rule-literal )

constraint-rule-literal ::=
  "is" compare-op bound

requirement-rule-literal ::=
  auxiliary-verb "be"
  ( compare-op bound | objective )

compare-op ::=
    "smaller" "than"
  | "greater" "than"
  | "not" "equal" "to"
  | "equal" "to"
  | "at" "least"
  | "at" "most"
  | "approximately" /* Only in a requirement-rule-literal */

bound ::=
    argument-name
  | VALUE [ unit ]
  | "t.b.d." [ unit ]

unit ::=
    UNIT-NAME
  | "[" UNIT-NAME "]"

objective ::=
    "maximized"
  | "minimized"
```

A `design-rule-line` consists of a `comparison-rule-line` followed by a new line or `subclauses` .
An `comparison-rule-line` consists of one or more `comparison`. A `comparison` starts with an
`argument-name` followed by a `constraint-rule-literal` or a `constraint-rule-literal`. An
`constraint-rule-literal` starts with the word `is` followed by a `compare-op` and a `bound`. An
`requirement-rule-literal` starts with an `auxiliary verb` and `be` followed by an `compare-op` and
a `bound` or an `objective`.

For example, design-requirement `d1` in the listing below states that the value of `water-flow-1`
must be at least 10 \[l/s\], while `d2` states that `water-flow-1` must be smaller than
`water-flow-2` or `water-flow-1` must be smaller than `water-flow-3` . The `...` denotes line
continuation.

```esl
design-requirement
  d1: water-flow-1 must be at least 10 l/s
  d2: water-flow-1 must be smaller than water-flow-2 or ...
      water-flow 1 must be smaller than water-flow-3
```

## Relations

In engineering design detailed (mathematical) analysis is usually required to ensure that a system
will function according to the set requirements. An analysis often consists of defining a
mathematical function that requires a set of input arguments and returns a set of output arguments.
Based on the values of the input arguments, the values of the output arguments are computed. The
values of the output arguments are subsequently (manually) verified against the set requirements.

The amount of information one has one a relation between variables may change during the course of a
design project. At the start of a design project, one may only be aware of the existence of a
relation. Later, one may obtain additional information that denotes the input-output relations.
Therefore, the proposed relation syntax allows for the specification of directed and undirected
relations. This enables users to convert undirected relations into directed relations during the
course of the design project.

For example, the listing below shows an example specification in which two relations are defined and
instantiated following the proposed syntax. At line 2, relation `ohms-law` is defined which requires
parameters `current` and `resistance` as input and returns parameter `voltage` as output. At line 9,
relation `newtons-second-law` is defined which relates parameters `force`, `mass`, and
`acceleration`.

At line 21, `ohms-law` is instantiated as relation `r1` which requires arguments `I` and `R` and
returns argument `U`. At line 28, `newtons-second-law` is instantiated as relation `r2` which
relates arguments `F`, `m`, and `a`.

```esl
define relation
  ohms-law
    requiring parameters
      * current is a real
      * resistance is a real
    returning parameter
      * voltage is a real

 newtons-second-law
   relating parameters
     * force is a real
     * mass is a real
     * acceleration is a real

 world
   variables
     I, R, U is a real
     F, m, a is a real

 relation
   r1: ohms-law
     requiring arguments
       * I
       * R
     returning argument
       * U

   r2: newtons-second-law
     relating arguments
       * F
       * m
       * a
```

The relations between the parameters of `ohms-law` are directed and follow a clear input-output
pattern. The relation between the parameters of `newtons-second-law` are undirected. That is, the
`requiring` keyword indicates that all parameters that follow are inputs to the relation. The
`returning` keyword indicates that all parameters that follow are outputs of the relation. The
`relating` keyword indicates that one has not decided whether the parameters that follow are input
or output of the relation.

All parameters within relation definitions are assigned a type. The type of each parameter must
match the type of the respective positional argument upon instantiation. For example, the type of
argument `a` must be equal to the type of parameter `acceleration`. This mechanism allows for
automated type checking. That is, the compiler can automatically check if all relations are
instantiated with the correct number and correct type of arguments.

So far, the proposed syntax requires the user to define all relation parameters a priori. However,
many mathematical formulas can take an arbitrary number of arguments. For example, a summation or
balance equation. Therefore, the `one or more` keywords are introduced which indicate that a
relation requires one or more parameters of a certain type. For example, in the following listing
the relation `cost-summation` requires one or more `cost` arguments as input and returns the
variable `total-cost` as argument.

```esl
define relation
  cost-summation
    requiring parameters
      * one or more cost is a real
    returning parameter
      * total-cost is a real
```

## Comments

ESL has two kinds of comments: annotation comments and code comments. Annotation comments are part
of the system specification and are attached as documentation to components, variables, needs, goal,
transformation, design, and relation specifications. That is, annotation comments can be used to
specify additional arbitrary textual information that is not captured by the ESL concepts
themselves. Code comments are not part of the specification but are used to write down information
that is relevant to the engineers who are creating and reading the specification.
