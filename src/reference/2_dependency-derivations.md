# Dependency derivations

In this section the formal rules are presented which are used to derive dependencies between
components, variables, needs, goal-specifications, transformation-specifications,
behavior-specifications design-specifications, relations, and combinations thereof.

The dependency sets presented in this section contain dependencies between elements across all
levels of the decomposition tree. Hence, the presented sets are independent of the view one aims to
obtain of the system. By selecting and visualizing sub-sets of the presented dependency sets one can
create different views on the system.

## Graphical examples

The [decomposition-function view](#fig-decomposition-function),
[decomposition-function-behavior view](#fig-decomposition-function-behavior), and
[decomposition-relation-design view](#fig-decomposition-relation-design) show graphical representations
of three separate example specifications which are used to illustrate the derivation rules. In the
figures, components are represented by rectangles, variables are represented by circles, two
components connect by a variable form a goal specification, two variables connected by a component
form a transformation specification. Behavior specifications are represented by a diamond. Relations
are represented a parallelogram connected to variables. Variable comparisons (design specifications)
are represented by triangles connected to variables. The
[decomposition-function view](#fig-decomposition-function) illustrates the derivation of function
dependencies, the [decomposition-function-behavior view](#fig-decomposition-function-behavior)
illustrates the derivation of behavior dependencies, and
[decomposition-relation-design view](#fig-decomposition-relation-design) illustrates the derivation
of design dependencies.

<figure markdown>
  <a id="fig-decomposition-function"></a>
  ![System decomposition tree and function specifications combined into a single hierarchical specification view.](./assets/fig_6_1.svg)
  <figcaption markdown>System decomposition tree and *function* specifications combined into a single hierarchical specification view.</figcaption>
</figure>

The figure above shows an example system decomposition tree composed of components $c_1$, $c_2$, and
$c_3$ at decomposition level $k=1$. Component $c_2$ is decomposed into components $c_{21}$ and
$c_{22}$. In turn, component $c_{22}$ is decomposed into components $c_{221}$ and $c_{222}$.
Component $c_3$ is decomposed into $c_{31}$ and $c_{32}$. Additionally, the example contains the
graphical representation of five goal-specifications ($g_1$ through $g_5$) and six
transformation-specifications ($t_1$ through $t_6$) indicating the transfer and transformation of
five flow variables ($v_1$ through $v_5$).

In the hierarchical specification view, displayed in the earlier figure, the component decomposition
tree and function specifications are combined, visualizing the flow of variables through the various
components and decomposition layers. That is, $c_1$ provides $v_1$ to $c_2$ ($g_1$), that must
internally transform $v_1$ into $v_2$ ($t_1$) and subsequently provide $v_2$ to $c_3$. Internal to
$c_2$, $v_1$ is passed on to child component $c_{21}$ which transforms $v_1$ into $v_3$ ($t_2$) and,
subsequently, provides $v_3$ to $c_{22}$ ($g_3$), which must transform $v_3$ into $v_2$ ($t_3$).
Flow $v_3$ is passed on to child component $c_{221}$ which transforms $v_3$ into $v_4$ ($t_4$) and,
subsequently, provides $v_4$ to $c_{222}$ ($g_4$). Component $c_{222}$ transforms $v_{4}$ into
$v_{2}$ ($t_{5}$). $v_{2}$ is passed to parent components $c_{22}$ and $c_2$. Component $c_2$
provides $v_2$ to $c_3$ ($g_2$), which passes $v_2$ on to child component $c_{31}$. Finally,
$c_{31}$ transforms $v_2$ into $v_5$ ($t_6$) and provides $v_5$ to $c_{32}$. Note that the function
chain $t_2$ $\rightarrow$ $g_3$ $\rightarrow$ $t_3$ is a more detailed representation of
transformation $t_1$. Similarly, the function chain $t_4$ $\rightarrow$ $g_4$ $\rightarrow$ $t_5$ is
a more detailed description of transformation $t_3$.

<figure markdown>
  <a id="fig-decomposition-function-behavior"></a>
  ![System decomposition tree and *function* and *behavior*  specifications combined into a single hierarchical specification view.](./assets/fig_6_1a.svg)
  <figcaption markdown>System decomposition tree and *function* and *behavior* specifications combined into a single hierarchical specification view.</figcaption>
</figure>

The figure above shows three components $c_1$, $c_2$, and $c_3$, where $c_1$ must provide $v_1$ and
$v_2$ to $c_2$ ($g_1$ and $g_2$ respectively) and $c_2$ must provide $v_3$ to $c_3$. Component $c_2$
must internally convert $v_1$ into $v_3$, where the value of $v_3$ is defined by behavior
specification $h_1$ with $v_2$ as input.

<figure markdown>
  <a id="fig-decomposition-relation-design"></a>
  ![System decomposition tree and *relation* and *design-rule*  specifications combined into a single hierarchical specification view.](./assets/fig_6_2.svg)
  <figcaption markdown>System decomposition tree and *relation* and *design-rule*  specifications combined into a single hierarchical specification view.</figcaption>
</figure>

The figure above presents a subsection of the decomposition tree shown in the
[decomposition-function view](#fig-decomposition-function), the graphical representation of a
relation specification ($r_1$) and two design specifications ($q_1$ and $q_2$), which are combined
into a single nested specification view. In this nested view the mapping of actual variables to
formal parameters is shown. For example, actual variable $v_8$, which 'lives' in world component
$c_\mathrm{w}$, is mapped onto formal parameters $v_8$ that'live' in components $c_1$, $c_2$, and
$c_{21}$, respectively. Relation $r_1$ relates variables $v_5$, $v_6$, and $v_7$ to one another.
Design-specifications $q_1$ binds the value of $v_8$ with the value of $v_9$ and
design-specification $q_2$ binds the value of $v_9$ with the value of $v_{10}$. Variables $v_8$ and
$v_9$ are marked as being properties of components $c_{21}$ and $c_{211}$, respectively.

## Nomenclature

For compact notation, we define the following nomenclature. Let $\mathcal{C}$ be the set of
components, $\mathcal{V}$ the set of variables, $\mathcal{K}$ the set of variables types,
$\mathcal{N}$ the set of needs, $\mathcal{G}$ the set of goal function specifications, $\mathcal{T}$
the set of transformation function specifications, $\mathcal{F} = \mathcal{G} \cup \mathcal{T}$, the
set of all function specifications, $\mathcal{H}$ the set of all behavior specifications,
$\mathcal{D}$ the set of design specifications, $\mathcal{Q}$ the set of comparison-rules,
$\mathcal{S}$ the set of subclauses, and $\mathcal{R}$ the set of relations specified in an
instantiated ESL-specification.

An arbitrary instantiated component $c=(V_c, P_c, N_c, C_c, G_c, T_c, H_c D_c, R_c) \in \mathcal{C}$
has variables $V_c\subseteq \mathcal{V}$, has properties $P_c\subseteq \mathcal{V}$ need
specifications $N_c\subseteq \mathcal{N}$, child components $C_c\subseteq \mathcal{C}$, goal
function specifications $G_c\subseteq \mathcal{G}$, transformation function specifications
$T_c\subseteq \mathcal{T}$, behavior specification $H_c \subseteq \mathcal{H}$, design
specifications $D_c\subseteq \mathcal{D}$, and relations $R_c\subseteq \mathcal{R}$.

A need specification $n= (s_n, \textit{text}_n) \in N_c$ has subject $s_n$ and $\textit{text}_n$
which is the unconstrained sentence specified by the user. A goal specification $g=(c_g, c'_g, V_g,
S_g)\in G_c$ has subject component $c_g \in C_c$, indirect object component $c'_g \in C_c$,
parameters and variables $V_g \subseteq V_c$, and sub-clauses $S_g \subseteq \mathcal{S}$. A
transformation specification $t=(c_t, V_t, V'_t, S_t) \in T_c$, has subject component $c_t = c$,
input variables $V_i\subseteq V_c$, output variables $V'_i\subseteq V_c$, and sub-clauses $S_t
\subseteq \mathcal{S}$. An behavior specification $h = \{ A_1, A_2, \ldots, A_n \} \in
\mathcal{H_c}$ has $n$ alternative cases. A case $A = (S, S')$ is composed of a set of when clauses
$S$ and a set of then clauses $S'$. A design specification $d = (Q_d, S_d) \in D_c$ has design-rule
set $Q_d$ and subclauses $S_d \subseteq \mathcal{S}$. Design-rule $q =(v_q, b_q) \in Q_d$ has
subject variable $v_q \in V_c$ and bound $b_q$. Bound $b_q$ may be a variable $v'_\mathrm{q} \in
\mathcal{V}$ or a real, integer, string or boolean value. A subclause $s = Q_m \in \mathcal{S}$ is a
design-rule set $Q_m \subseteq \mathcal{Q}$, where $m \in \mathcal{G} \cup \mathcal{T} \cup
\mathcal{D}$ is the target main clause. Design-rule $q =(v_q, b_q) \in Q_m$ has subject variable
$v_q$ and has bound $b_q$. A relation $r = r = (V, V', V'') \in R_c$, where $V \subseteq \in
\mathcal{V}$ is the set of input variables, $V' \in \mathcal{V}$ is the set of output variables and
$V'' \in \mathcal{V}$ is the set of undirected variables.

To select the components within a (sub-)branch of the decomposition tree, we define the $tree$
operator. In an instantiated system, the set of components $C \subseteq \mathcal{C}$ form a rooted
directed tree using the parent-child relation, with world component $c_\mathrm{w}\in C$ as root. The
set of components of a (sub-)tree with a component $c \in C$ as root is defined as:

$$\textit{tree}(c) = \{c\} \cup \bigcup_{c'\in C_c} \textit{tree}(c')$$

For the example shown in $\textit{tree}(\mathrm{c}_{2}) = \{\mathrm{c}_{2}, \mathrm{c}_{21},
\mathrm{c}_{22}, \mathrm{c}_{221}, \mathrm{c}_{222}\}$ and $\textit{tree}(\mathrm{c}_{3})  =
\{\mathrm{c}_{3}, \mathrm{c}_{31}, \mathrm{c}_{32} \}$.

To obtain the position of a component within a decomposition tree with root component $c'$, we
define the $depth$ operator. The instantiation depth of a component $c\in C$ with respect to a root
component $c'$ is given by:

$$
\begin{aligned} depth(c', c) = \left\{\begin{array}{ll} 0                  & \textrm{if}\ c = c'
\\
1 + depth(c'', c)  & \textrm{if}\ c'' \in C_{c'}, c \in tree(c'')  \\
\textrm{undefined} & \textrm{otherwise} \end{array} \right. \end{aligned}
$$

for $c'=(V_{c'}, P_{c'}, N_{c'}, C_{c'}, G_{c'}, T_{c'}, D_{c'}, R_{c'})$ and $c=(V_c, P_c, N_c,
C_c, G_c, T_c, H_c, D_c, R_c)$. The $depth$ operator 'walks' down branches of the tree with root
$c'$ until it finds $c$. For example, $\textit{depth}(\mathrm{c}_\mathrm{w}, \mathrm{c}_{221}) = 3$,
since at decomposition level $k=1$, we have $\mathrm{c}_2 \in C_\mathrm{c_w}$ and $\mathrm{c}_{221}
\in tree(\mathrm{c}_2)$, at decomposition level $k=2$, we have $\mathrm{c}_{22} \in
C_\mathrm{c_{2}}$ and $\mathrm{c}_{221} \in tree(\mathrm{c}_{22})$, at decomposition level $k=3$ we
have $\mathrm{c}_{221} \in C_\mathrm{c_{22}}$ and $\mathrm{c}_{221} \in tree(\mathrm{c}_{221})$, and
finally, we have $\mathrm{c}_{221} = \mathrm{c}_{221}$, yielding
$\textit{depth}(\mathrm{c}_\mathrm{w}, \mathrm{c}_{221}) = 3$. Note that,
$\textit{depth}(\mathrm{c}_{2}, \mathrm{c}_\mathrm{3}) = \mathrm{NaN}$, since $c_3$ is not a member
of the tree of $c_{2}$.

The following sections present the derivation rules for deriving dependencies between ESL elements.
All dependencies are by definition directed and represented as a tuple of source and target. For
example, the dependency tuple $(a, b)$ implies that element $b$ depends on element $a$. If this
dependency is bidirectional the tuple $(b, a)$ is added to the overall set of dependencies as well.

## Dependencies between variables

Three types of dependencies between variables are derived from an ESL specification: functional
dependencies, i.e.,component dependencies derived from function specifications, behavior
dependencies, i.e., dependencies derived from behavior specifications, and design dependencies,
i.e., component dependencies derived from design specifications and from relations.

### Functional dependencies

A functional dependency between two variables $v_i$ and $v_j$ indicates that flow $v_i$ must be
transformed into flow $v_j$ to establish the desired functionality of the system.

Transformation specifications $\mathcal{T}$ are used to derive functional flow dependencies between
variables. Variable functional flow dependency set $E_\mathrm{v_f}$ is given by:

$$
\begin{aligned} \begin{array}{rlll} E_\mathrm{v_f} = & \{(v_i, v_j) &\mid& (c_t, V_t, V'_t, S_t)
\in \mathcal{T}, \\
                 &              &    & c_t = (V_{c_t}, N_{c_t}, C_{c_t}, G_{c_t}, T_{c_t}, D_{c_t}, R_{c_t}) \in \mathcal{C} \\
                 &              &    & v_i \in V_t, v_j \in V'_t, v_i \neq v_j, C_{c_t} = \emptyset \}  \\
\end{array}\label{eqn:vf} \end{aligned}
$$

That is, variable $v_j$ depends on variable $v_i$ if $v_i$ is a member of input variable set $V_t$,
$v_j$ is a member of output variable set $V_t'$, and the set of child component $C_{c_t}$ is an
empty set. In other words, only transformations of leaf components (those with no sub-components)
are used to derive the functional dependencies between variables.

The derivation of this set for the example shown in
[decomposition-function view](#fig-decomposition-function) is straightforward, it contains all
variable pairs that are connected via a transformation function fulfilled by a leaf component. That
is, it equals $\{(\mathrm{v}_{1}, \mathrm{v}_{3}), (\mathrm{v}_{3}, \mathrm{v}_{4}),
(\mathrm{v}_{4}, \mathrm{v}_{2}), (\mathrm{v}_{2}, \mathrm{v}_{5}) \}$.

### Behavior dependencies

A behavior dependency between two variables $v_i$ and $v_j$ indicates that the value of a variable
$v_i$ is dynamically bound by the value of a variable $v_j$. The set of behavior dependencies
between variables $E_{\mathrm{v_b}}$ is given by:

$$
\begin{aligned} \begin{array}{rclcl} E_{\mathrm{v_b}} & = & \{ (v, v')  & \mid & (S, S') \in h, h
\in \mathcal{H}, \\
                 &   &             &      & (v, b) \in Q, Q \in S, \\
                 &   &             &      & (v', b') \in Q', Q' \in S'\}
\end{array} \end{aligned}
$$

That is, $E_{\mathrm{v_b}}$ consists of all variable pairs $(v, v')$ for which there exist a
design-rule $(v, b)$ within design rule line $Q$ within when-clauses $S$ and a design-rule $(v',
b')$ within design-rule-set $Q'$ within then-clauses $S'$. In other words, if there exists a case in
which the value of $v'$ is dynamically bound by the the value of $v$.

For the [decomposition-function-behavior view](#fig-decomposition-function-behavior) this set
consists of equals $\{(\mathrm{v}_{2}, \mathrm{v}_{3}\}$ as the value of $\mathrm{v}_{3}$ is
dynamically bound by the value of $\mathrm{v}_{2}$ via behavior specification $\mathrm{h}_{1}$.

### Design dependencies

A design dependency between two variables $v_i$ and $v_j$ indicates that the value of $v_i$ is
statically bound by, or directly correlated with, the value of $v_j$. Design-rules $\mathcal{Q}$ and
relations $\mathcal{R}$ are used to derive design dependencies between variables. Variable design
dependency set $E_\mathrm{v_d}$ is given by:

$$
\begin{aligned} \begin{array}{rlll} E_\mathrm{v_d} = & \{(v_i, v_j) &\mid& (v_i, v_j) \in
\mathcal{Q} \lor  (v_j, v_i) \in \mathcal{Q}\} \\
\cup             & \{(v_i, v_j) &\mid& (V, V', V'') \in \mathcal{R}, \\
                 &              &    & v_i \in (V \cup V''), v_j \in (V' \cup V'') \} \\
\end{array} \end{aligned}
$$

That is, two variables $v_i$ and $v_j$ have a dependency if there exists a design rule $(v_i, v_j)
\in \mathcal{Q}$, a design rule $(v_j, v_i) \in \mathcal{Q}$, or there exists a relation $r = (V,
V', V'') \in \mathcal{R}$ such that $v_i$ is a member of the union of the input variables $V$ and
undirected variables $V''$ and $v_j$ is a member of the union of the output variables $V'$ and
undirected variables $V''$ of $r$. Note that if $V''$ is empty for all $r \in \mathcal{R}$ that all
dependencies between variables derived from relations become directed. Dependencies derived from
design-rules always result in a bi-directional dependency.

For the [decomposition-relation-design view](#fig-decomposition-relation-design) this set equals
$\{(\mathrm{v}_{5}, \mathrm{v}_{6}), (\mathrm{v}_{7}, \mathrm{v}_{6}), (\mathrm{v}_{8},
\mathrm{v}_{9}), (\mathrm{v}_{9}, \mathrm{v}_{8}), (\mathrm{v}_{9}, \mathrm{v}_{10}),
(\mathrm{v}_{10}, \mathrm{v}_{9}) \}$, where the first two pairs result from relation $r_1$ and the
last four pairs results from design rules $q_1$ and $q_2$, respectively.

## Dependencies between components

Four types of component dependencies are derived from an ESL specification: functional dependencies,
i.e.,component dependencies derived from function specifications, behavior dependencies, i.e.,
dependencies derived from behavior specifications, design dependencies, i.e., component dependencies
derived from design specifications and relations relations, and coordination dependencies, i.e.
dependencies based on shared variables.

### Functional dependencies

Functional component dependencies are derived from the goal and transformation specifications, $G$
and $T$ respectively, as they define the flows through the system. The set of functional component
dependencies $E_\mathrm{c_f}$ (edges) is given by:

$$
\begin{aligned} \begin{array}{rlll} E_\mathrm{c_f} = & \{(c_g, c'_g)   &\mid& (c_g, c'_g, V_g,
S_g)\in \mathcal{G}, c_g \neq c'_g \} \\
\cup             & \{(c_g, c)      &\mid& (c_g, c'_g, V_g, S_g)\in \mathcal{G}, \\
                 &                 &    & (c_t, V_t, V'_t, S_t) \in \mathcal{T}, \\
                 &                 &    & c \in \textit{tree}(c'_g), c_t \in \textit{tree}(c), \\
                 &                 &    & V_g \cap V_t \neq  \emptyset, c'_g \neq c \} \\
\cup             & \{(c, c'_g)     &\mid& (c_g, c'_g, V_g, S_g)\in \mathcal{G}, \\
                 &                 &    & (c_t, V_t, V'_t, S_t) \in \mathcal{T}, \\
                 &                 &    &  c \in \textit{tree}(c_g), c_t \in \textit{tree}(c), \\
                 &                 &    & V_g \cap V'_t \neq  \emptyset, c_g \neq c \} \\
\cup             & \{(c_i,c_j)  &\mid& (c_t, V_t, V'_t, S_t) \in \mathcal{T}, \\
                 &                 &    & (c_{t'}, V_{t'}, V'_{t'}, S_{t'}) \in \mathcal{T},  \\
                 &                 &    &  c_i \notin tree(c_j), c_j \notin tree(c_i) \\
                 &                 &    &  c_t \in tree(c_i), c_{t'} \in tree(c_j), \\
                 &                 &    &  V'_t\cap V_{t'} \neq  \emptyset \} \\
\end{array}\label{eqn:compdep} \end{aligned}
$$

which is composed of four component pair sets. The first set of denotes pairs $(c_g, c'_g)$ for
which subject component $c_g$ and indirect object component $c'_g$ are part of goal-specification
$g$, and therefore exchange a flow.

For the [decomposition-function view](#fig-decomposition-function), this set equals
$\{(\mathrm{c}_1, \mathrm{c}_2), (\mathrm{c}_2, \mathrm{c}_3), (\mathrm{c}_{21}, \mathrm{c}_{22}),
(\mathrm{c}_{221}, \mathrm{c}_{222}), (\mathrm{c}_{31}, \mathrm{c}_{32}) \}$, which are all
component pairs used in the respective goal function specifications. Note that the two components in
such a pair are of the same decomposition level.

The second set of component pairs in the equation above denotes pairs $(c_g, c)$ for which all
following conditions hold: $c_g$ is a subject component in a goal function specification $g$, while
$c$ is member of the tree of the indirect object component $c'_g$ in $g$; the tree of $c$ contains a
component $c_t$ which is the subject of a transformation function specification $t$; the
intersection of the set of variables $V_g$ associated with goal function $g$ and the set of
variables $V_t$ associated with transformation function $t$ is non-empty; and $c'_g$ is a different
component than $c$. The component pairs that obey these conditions represent components that exist
at different decomposition levels, where the second component, at a deeper decomposition level,
depends on the input from the first component.

For the [decomposition-function view](#fig-decomposition-function), the pairs that obey these
conditions are $\{(\mathrm{c}_1,\mathrm{c}_{21}), (\mathrm{c}_{21}, \mathrm{c}_{221}),
(\mathrm{c}_2,\mathrm{c}_{31})\}$. Consider for instance the first pair: $\mathrm{c}_1$ provides
$\mathrm{v}_1$ to $\mathrm{c}_2$. Component $\mathrm{c}_2$ is the parent of $\mathrm{c}_{21}$ and
$\mathrm{c}_{22}$, where $\mathrm{c}_{21}$ takes $\mathrm{v}_1$ as input to perform transformation
$\mathrm{t}_2$. Thus $\mathrm{c}_{21}$ requires $\mathrm{v}_{1}$ as input from $\mathrm{c}_{1}$.
Similarly, $\mathrm{c}_{21}$ provides input to $\mathrm{c}_{221}$.

The third set of component pairs is complementary to the second set, with similar conditions, but
now from the perspective of the indirect object component $c'_g$. This set denotes components pairs
that exist at different decomposition levels but have an output flow dependency.

For the [decomposition-function view](#fig-decomposition-function), this set equals
$\{(\mathrm{c}_{222}, \mathrm{c}_{3}), (\mathrm{c}_{22}, \mathrm{c}_{3})\}$. For the first pair, the
output of $\mathrm{c}_{222}$ is required by $\mathrm{c}_{3}$, that is $\mathrm{c}_{222}$ carries out
transformation $\mathrm{t}_5$ to yield $\mathrm{v}_2$ which is passed by grandparent $\mathrm{c}_2$
to $\mathrm{c}_3$. Similarly, viewed at the decomposition level of $\mathrm{c}_{22}$,
$\mathrm{c}_{22}$ generates output $\mathrm{v}_2$ that is passed on to $\mathrm{c}_3$ by parent
$\mathrm{c}_2$.

Finally, the fourth set contains pairs $(c_i, c_j)$ for which their exist a transformation $t$ with
subject $c_t$ which is in the tree of $c_i$; their exist a transformation $t'$ with subject
components $c_{t'}$ which is in the tree of $c_j$; and the intersection of variable sets $V'_t$ and
$V_{t'}$ or the intersection of variable sets $V_t$ and $V'_{t'}$ is non-empty. Additionally $c_i$
and $c_j$ may not be a member of each others trees. This set contains component pairs that have an
input/output flow dependency based on the transformation functions they perform.

For the [decomposition-function view](#fig-decomposition-function), this set equals
$\{(\mathrm{c}_{2}, \mathrm{c}_{3}), (\mathrm{c}_{2}, \mathrm{c}_{31}), (\mathrm{c}_{22},
\mathrm{c}_{3}), (\mathrm{c}_{22}, \mathrm{c}_{31}), (\mathrm{c}_{222}, \mathrm{c}_{3}),
(\mathrm{c}_{222}, \mathrm{c}_{31}), (\mathrm{c}_{221}, \mathrm{c}_{222}), (\mathrm{c}_{21},
\mathrm{c}_{221})\}$. For instance, $\mathrm{c}_{222}$ performs transformation $\mathrm{t}_5$ which
yields $\mathrm{v}_2$. In turn, $\mathrm{v}_2$ is consumed by transformation $\mathrm{t}_6$
performed by $\mathrm{c}_{31}$, which is a child of $\mathrm{c}_3$. This yields dependency pairs
$(\mathrm{c}_{222}, \mathrm{c}_{3})$ and $(\mathrm{c}_{222}, \mathrm{c}_{31})$. Moreover,
$\mathrm{c}_{222}$ is a child of $\mathrm{c}_{22}$ and a grandchild of $\mathrm{c}_{2}$, which
yields the additional dependency pairs $(\mathrm{c}_{22}, \mathrm{c}_{3})$, $(\mathrm{c}_{22},
\mathrm{c}_{31})$, $(\mathrm{c}_{2}, \mathrm{c}_{3})$, and $(\mathrm{c}_{2}, \mathrm{c}_{31})$.

Note that the fourth set contains dependency pairs that are also part of the first, second, or third
dependency set. The reason for this is that goal-functions explicitly denote the presence of flow
between components, while transformation functions implicitly denote the presence of flow between
components. This redundancy in information enables us to derive the same dependencies in multiple
ways and may provide us with the means to create automated specification completeness checks. For
example, to check the completeness of a functional flow path between two components. These automated
completeness checks are a topic of further research.

The union of the aforementioned component pair sets yields the set of functional component
dependencies $E_\mathrm{c_f}$ between components throughout the decomposition tree based on the
transfer and transformation of flows.

!!! note

    The dependency sets derived in this section are all directed. In the DSM literature,
    dependencies between components are usually visualized undirected. That is, product DSMs are
    typically symmetric. A symmetric product DSM can easily be obtained by simply duplicating the
    dependencies and switching the source and target.

### Behavior dependencies

A behavior dependency between two components $c_i$ and $c_j$ indicates that the dynamic functional
output of component $c_j$ dependents on the input received from component $c_i$. The set of behavior
dependencies between variables $E_{\mathrm{v_b}}$ can used to derive the set of behavior
dependencies between components $E_{\mathrm{c_b}}$, which is given by:

$$
\begin{aligned} \begin{array}{rclcl} E_{\mathrm{c_b}} &   =   & \{ (c_i, c_j)  & \mid & (c_i,
c'_i, V_i, S_i) \in \mathcal{G}, (c_j, c'_j, V_j, S_j) \in \mathcal{G}, \\
                  &       &                &      & v_i \in V_i, v_j \in V_j , \exists path(v_i, v_j, E_{\mathrm{v_b}}), \\
                  &       &                &      & \forall v_k \in path(v_i, v_j, E_{\mathrm{v_b}}), v_k \neq v_i, v_k \neq v_j, \\
                  &       &                &      &  (v, t) \notin M, (v, g) \notin M, t \in \mathcal{T}, g \in \mathcal{G} \} \\
                  & \cup  & \{ (c_i, c_j)  & \mid & (c_i, V_i, V'_i, S_i) \in \mathcal{T}, (c_j, V_j, V'_j, S_j) \in \mathcal{T}, \\
                  &       &                &      & v_i \in V'_i, v_j \in V'_j , \exists path(v_i, v_j, E_{\mathrm{v_b}}), \\
                  &       &                &      & \forall v_k \in path(v_i, v_j, E_{\mathrm{v_b}}), v_k \neq v_i, v_k \neq v_j, \\
                  &       &                &      &  (v, t) \notin M, (v, g) \notin M, t \in \mathcal{T}, g \in \mathcal{G} \} \\
                  & \cup  & \{ (c_i, c_j)  & \mid & (c_i, c'_i, V_i, S_i) \in \mathcal{G}, (c_j, V_j, V'_j, S_j) \in \mathcal{T}, \\
                  &       &                &      & v_i \in V_i, v_j \in V'_j , \exists path(v_i, v_j, E_{\mathrm{v_b}}), \\
                  &       &                &      & \forall v_k \in path(v_i, v_j, E_{\mathrm{v_b}}), v_k \neq v_i, v_k \neq v_j, \\
                  &       &                &      &  (v, t) \notin M, (v, g) \notin M, t \in \mathcal{T}, g \in \mathcal{G} \} \\
                  & \cup  & \{ (c_i, c_j)  & \mid & (c_i, V_i, V'_i, S_i) \in \mathcal{T}, (c_j, c'_j, V_j, S_j) \in \mathcal{G}, \\
                  &       &                &      & v_i \in V'_i, v_j \in V_j , \exists path(v_i, v_j, E_{\mathrm{v_b}}), \\
                  &       &                &      & \forall v_k \in path(v_i, v_j, E_{\mathrm{v_b}}), v_k \neq v_i, v_k \neq v_j, \\
                  &       &                &      &  (v, t) \notin M, (v, g) \notin M, t \in \mathcal{T}, g \in \mathcal{G} \} \\
\end{array} \end{aligned}
$$

wherein, the first set of component pairs denotes those pairs $c_i$ and $c_j$ for which there exist
a goal $(c_i, c'_i, V_i, S_i) \in \mathcal{G}$ and a goal $(c_j, c'_j, V_j, S_j) \in \mathcal{G}$
such that there exists a path over the set of behavior dependencies between variables
$E_{\mathrm{v_b}}$ between a variable $v_i$ in $V_i$ and a variable $v_j$ in $V_j$, such that all
intermediate variables $v_k$ part of this path do not relate to any other transformation $g$ or
transformation $t$. In other words, the value of a flow to be transferred by component $c_j$ is
bound by the value of a flow to be transferred by component $c_i$, without any transformations or
goals in between.

The second set of component pairs denotes those component pairs $c_i$ and $c_j$ for which there
exist a transformation $(c_i, V_i, V'_i, S_i) \in \mathcal{T}$ and a transformation $(c_j, V_j,
V'_j, S_j) \in \mathcal{T}$, such that there exists a path over the set of behavior dependencies
between variables $E_{\mathrm{v_b}}$ between a variable $v_i$ in $V_i$ and a variable $v_j$ in
$V_j$, such that all intermediate variables $v_k$ part of this path do not relate to any other
transformation $g$ or transformation $t$.

The third set of component pairs denotes those pairs $c_i$ and $c_j$ for which there exist a goal
$(c_i, c'_i, V_i, S_i) \in \mathcal{G}$ and a transformation $(c_j, V_j, V'_j, S_j) \in
\mathcal{T}$, such that there exists a path over the set of behavior dependencies between variables
$E_{\mathrm{v_b}}$ between $v_i$ in $V_i$ and a variable $v_j$ in $V'_j$ , such that all
intermediate variables $v_k$ part of this path do not relate to any other transformation $g$ or
transformation $t$.

The fourth set is similar to the third set except here a flow in $V_j$ is bound by an output flow in
$V'_i$.

For the [decomposition-function-behavior view](#fig-decomposition-function-behavior) this set equals
$\{(\mathrm{c}_1, \mathrm{c}_2)\}$ since $(\mathrm{v}_{2}, \mathrm{v}_{3}) \in E_{\mathrm{v_b}}$
holds.

### Design dependencies

Design dependencies denote that designs of two components are dependent on each other but are not
necessarily functionally connected. For example, two components may have to fit in a predefined
space but do not exchange any (functional) flows. In this work, we refer to such dependencies as
design dependencies. These dependencies are derived using variable set $V_c$ of components and the
set of design dependencies between variables $\in E_\mathrm{v_d}$ Design dependency set
$E_\mathrm{c_d}$ is given by:

$$
\begin{aligned} \begin{array}{rlll} E_\mathrm{c_d} = & \{(c_i, c_j)&\mid& c_i = (V_i, P_i, N_i,
C_i, G_i, T_i, D_i, R_i) \in \mathcal{C}, \\
                 &             &    & c_j = (V_j, P_j, N_j, C_j, G_j, T_j, D_j, R_j)\in \mathcal{C} , \\
                 &             &    &  c_i \notin tree(c_j), c_j \notin tree(c_i), \\
                 &             &    & (v_i, v_j) \in E_\mathrm{v_d}, v_i \in V_i \cup P_i, v_j \in V_j \cup P_j\}
\end{array}\label{eqn:compdep1} \end{aligned}
$$

in which contains those pairs of components $(c, c )$ for which $c_i$ and component $c_j$ are not in
each others trees and their exist two variables $v_i \in V_i \cup P_i$ and $v_j \in V_j \cup P_j$
which have a design dependency.

For the [decomposition-relation-design view](#fig-decomposition-relation-design) this set equals
$\{(\mathrm{c}_{1}, \mathrm{c}_{3}) (\mathrm{c}_{21}, \mathrm{c}_{22}), (\mathrm{c}_{21},
\mathrm{c}_{221}), (\mathrm{c}_{221}, \mathrm{c}_{222}) \}$ since the values of properties $v_8$ and
$v_9$ are bound by one another due to design-rule $q_1$.

### Coordination dependencies

Coordination dependencies denote the presence of a shared variable between two components of which
the value has to be coordinated. The set of coordination dependencies between components
$E_\mathrm{c_c}$ is given by:

$$
\begin{aligned} \begin{array}{rlll} E_\mathrm{c_c} = & \{(c_i, c_j)&\mid& c_i = (V_i, N_i, C_i,
G_i, T_i, D_i, R_i) \in \mathcal{C}, \\
                 &             &    &  c_j = (V_j, N_j, C_j, G_j, T_j, D_j, R_j)\in \mathcal{C} , \\
                 &             &    &  c_i \notin tree(c_j), c_j \notin tree(c_i), \\
                 &             &    &  (V_i \cup P_i) \cap (V_j \cup P_j) \neq \emptyset \}
\end{array}\label{eqn:compdep3} \end{aligned}
$$

The this set contains pairs $(c_i,c_j)$ such that component $c_i$ and component $c_j$ are not in
each others trees and the intersection of variable sets $V_i \cup P_i$ and $V_j \cup P_j$ is
non-empty, i.e., $c_i$ and $c_i$ share one or more variables. In other words, this set contains
components pairs that share variables.

For the example shown in the [decomposition-relation-design view](#fig-decomposition-relation-design),
this set equals $\{(\mathrm{c}_{1}, \mathrm{c}_{2}), (\mathrm{c}_{1}, \mathrm{c}_{21})\}$ since
these component pairs share variable $v_8$. Note that component pair $(\mathrm{c}_{2},
\mathrm{c}_{21})$ is not part of this set as $\mathrm{c}_{21} \in tree(\mathrm{c}_{2})$. We exclude
these coordination dependencies as by definition the design of a parent component has to be
coordinated with the design of its children.

## Dependencies between function specifications

Three types of dependencies between function specifications are derived: traceability dependencies,
i.e., dependencies that show which function specifications specified at level $k+1$ contribute to
the fulfillment of function specifications specified at level $k$, functional dependencies, i.e.,
dependencies that denote the function chains within the system, and behavior dependencies, i.e.,
dependencies that are derived from behavior specifications and denote logical dependencies.

### Traceability dependencies

Traceability dependencies are derived based on functional dependency paths between variables. All
function specifications that contribute to the fulfillment of a transformation-specification $t \in
\mathcal{T}$, referred to as the children of $t$ and are denoted by $F(t) \subset \mathcal{F}$, have
to be part of a function chain starting with an input variable $v_i \in V_t$ and ending at an output
variable $v_j \in V_t'$. The set of all variables that are part of a path starting at an input
variable $v_i \in V_t$ and ending at an an output variable $v_j \in V_t'$ over the scope of subject
component $c_t$ of transformation $t$ is given by:

$$V(v_i, v_j, c_t) = \bigcup_{(v', v'') \in \textit{path}(v_i, v_j, c_t)} \{v', v''\}$$

$V(v_i, v_j, c_t)$ is used to compute the set of children of transformation functions $F(t) \subset
\mathcal{F}$. The set of children of an arbitrary transformation specification $t$ is given by:

$$
\begin{aligned} \begin{array}{rcrcl} F(t)  &   =   & \{ t' & \mid & t = (c_t, V_t, V_t', S_t) \in
\mathcal{T}, \\
      &       &       &      & t' = (c_{t'}, V_{t'}, V_{t'}', S_{t'}) \in \mathcal{T}, \\
      &       &       &      & t \neq t', c_{t'} \in C_{c_t},  v_i \in V_t, v_j \in V_t', \\
      &       &       &      & V_{t'} \cap V(v_i, v_j, c_t) \neq \emptyset, \\
      &       &       &      & V_{t'}' \cap V(v_i, v_j, c_t) \neq \emptyset \}  \\
      & \cup  & \{ g  & \mid & t = (c_t, V_t, V_t', S_t) \in \mathcal{T}, \\
      &       &       &      & g = (c_g, c_g', V_g, S_g) \in \mathcal{G}, \\
      &       &       &      & c_{g} \in C_{c_t}, c_{g}' \in C_{c_t}, v_i \in V_t, v_j \in V_t', \\
      &       &       &      & V_g \cap V(v_i, v_j, c_t) \neq \emptyset\}
\end{array} \end{aligned}
$$

That is, a transformation specification $t'$ is a child of transformation specification $t$ if the
subject component $c_t'$ is a child of subject component $c_t$, there exists a path from input
variable $v_i$ to output variable $v_j$ over the scope of $c_t$ such the the intersection of the set
of input variables $V_{t'}$ and the set of path variables $V(v_i, v_j, c_t)$ and the intersection of
output variables $V_{t'}'$ and the set of path variables $V(v_i, v_j, c_t)$ are non empty sets. In
other words, $t'$ is a child of transformation specification $t$ if it is part of the function chain
describing the transformation of an input variable $v_i$ to an output variable $v_j$.

A goal-specification $g$ is a child of transformation-specification $t$ if the subject component
$c_g$ and indirect object component $c_g'$ are children of subject component $c_t$ and the
intersection of the set of goal variables $V_g$ with $V(v_i, v_j, c_t)$ is a non empty set. In other
words, if one or more variables in $V_g$ are path of an input-output path.

The function $F(t)$ is used to determine the set of traceability dependencies $E_\mathrm{f_t}$,
which is given by:

$$
\begin{aligned} \begin{array}{rcrcl} E_\mathrm{f_t} & = & & \{ (t, f) & \mid & t \in \mathcal{T},
f \in F(t) \subset \mathcal{F} \} \\
\end{array} \end{aligned}
$$

For the [decomposition-function view](#fig-decomposition-function) this set equals
$\{(\mathrm{t}_1, \mathrm{t}_2), (\mathrm{t}_1, \mathrm{g}_2), (\mathrm{t}_1, \mathrm{t}_3),
(\mathrm{t}_3, \mathrm{t}_4), (\mathrm{t}_3, \mathrm{g}_4), (\mathrm{t}_3, \mathrm{t}_5) \}$. As
such, transformation specifications $\mathrm{t}_1$ and $\mathrm{t}_3$ can be traced down to lower
level function specifications.

Note that goal- and transformation-specifications can be part of multiple function chains describing
multiple parent transformations. For example, the
[example figure below](#fig-traceability-dependencies) shows two transformations $t_i$ and $t_j$
with subject component $c$ describing transformations from $x$ to $y$ and $x$ to $z$, respectively.

The three transformations $t'$, $t''$, and $t'''$, where $c_{t'}, c_{t''}, c_{t'''} \in C_c$,
describe these transformations in more detail. $t'$ describes a transformation from $x$ to $q$,
$t''$ describes a transformation from $q$ to $y$, and $t'''$ describes a transformation from $q$ to
$z$. So, $t'$, $t''$, and $t'''$ describe the two transformation paths $x \rightarrow q \rightarrow
y$ and $x \rightarrow q \rightarrow z$. As such, $t' \in F(t_i)$ and $t' \in F(t_j)$ both hold.

Therefore, $t'$ has two parent functions ($t_i$ and $t_j$) as it contributes to the fulfillment of
both of these functions. In other words, the function hierarchies form directed acyclic graphs
rather than tree structures.

<a id="fig-traceability-dependencies"></a>

```mermaid
---8<--- "esl0007-traceability-dependencies.mmd"
```

<figcaption markdown>Traceability dependency derivation for a component with subcomponents.</figcaption>

### Functional dependencies

Function dependencies denote the transfer and transformation of flows through the system. That is,
they denote the function chains that are being fulfilled by the various components in the system.
The set of all functional dependencies between functions $E_\mathrm{f_f}$ is given by:

$$
\begin{aligned} \begin{array}{rclcl} E_\mathrm{f_f} & =    & \{ (t_i, t_j) & \mid & t_i = (c,
V_i, V'_i, S_i) \in \mathcal{T}, \\
             &      &               &      & t_j = (c, V_j, V'_j, S_j) \in \mathcal{T}, \\
             &      &               &      & t_i \neq t_j, \\
             &      &               &      & V'_i \cap V_j \neq \emptyset \} \\
             & \cup & \{ (t, g)     & \mid & t = (c_t, V_t, V_t', S_t) \in \mathcal{T}, \\
             &      &               &      & g = (c_g, c_g', V_g, S_g) \in \mathcal{G}, \\
             &      &               &      & V_t' \cap V_g \neq \emptyset \} \\
             & \cup & \{ (g, t)     & \mid & g = (c_g, c_g', V_g, S_g) \in \mathcal{G}, \\
             &      &               &      & t = (c_t, V_t, V_t', S_t) \in \mathcal{T}, \\
             &      &               &      & V_g \cap V_t \neq \emptyset \}
\end{array} \end{aligned}
$$

That is, transformation-specification $t_j$ depends on transformation-specification $t_i$ if the
intersection of output variable set $V_{t_i}'$ and input variable set $V_{t_j}$ $t_j$ is a non-empty
set. Note that, no direct goal-specification dependencies $(g_i, g_j)$ are derived to avoid
bypasses.

A goal-specification $g$ depends on transformation-specification $t$ if the intersection of output
variable set $V_t'$ with variable set $V_g$ is not empty.

Similarly, transformation specification $t$ depends on goal-specification $g$ if the intersection of
variable set $V_g$ with input variable set $V_t$ is not empty.

For the [traceability dependencies example](#fig-traceability-dependencies) this set equals to $\{
(\mathrm{g}_1, \mathrm{t}_1), (\mathrm{t}_1, \mathrm{g}_2), (\mathrm{g}_1, \mathrm{t}_2),
(\mathrm{g}_2, \mathrm{t}_6), (\mathrm{t}_6, \mathrm{g}_5), (\mathrm{t}_2, \mathrm{g}_3),
(\mathrm{g}_3, \mathrm{t}_3), (\mathrm{t}_3, \mathrm{g}_2), (\mathrm{g}_3, \mathrm{t}_4),
(\mathrm{t}_4, \mathrm{g}_4), (\mathrm{g}_4, \mathrm{t}_5), (\mathrm{g}_5, \mathrm{g}_2)\}$. A such,
at each level of the decomposition a closed function chain is found.

These new dependency derivations rules imply that only transformation-specifications that share the
same subject component can have direct dependencies. Transformation-specifications that do not have
the same subject component can only have indirect dependencies via goal-specifications.
Goal-specifications can only have indirect dependencies via transformation-specifications.

As a consequence, a user must specify transformation functions at all decomposition levels to obtain
complete function chains and function hierarchies. This differs from the current convention of only
specifying transformation-specifications within leaf components ($\{ c \mid C_c = \emptyset \}$).
This is a price we are willing to pay as it provides functional traceability and allows for
completeness checks.

The set $E_\mathrm{v_f}$ can be used to check the functional completeness of a specification. For
example, assume we have a transformation specification $t = (c_t, V_t, V_t', S_t)$ for which
$C_{c_t} \neq \emptyset$ then $t$ is traceable if $path(v_i, v_j, c_t) \neq \emptyset \ \forall v_i
\in V_t, v_j \in V_t'$. That is, the transformations specified within leaf components of the tree
with root $c_t$ should describe a transformation path for all input variables $v_i \in V_t$ to all
output variables $v_j \in V_t'$. The non-existence of such a path implies that the transformation
$t$ that should be fulfilled by $c_t$ is not traceable to the subcomponents of $c_t$. Thus, the
specification is incomplete.

### Behavior dependencies

Behavior dependencies denote logical dependencies between functions. For example, the amount of
torque delivered by an electric-motor may depend on a control-signal. Such a dependency is not
physical but logical. The set of behavior dependencies between functions $E_{\mathrm{f_b}}$ is given
by:

$$
\begin{aligned} \begin{array}{rclcl} E_{\mathrm{f_b}} &   =   & \{ (g, t)  & \mid & g = (c_g,
c'_g, V_g, S_g) \in \mathcal{G}, \\
                  &       &            &      & t = (c_t, V_t, V'_t, S_t) \in \mathcal{T},   \\
                  &       &                &      & v_i \in V_g, v_j \in V'_t, \exists path(v_i, v_j, E_{\mathrm{v_b}}), \\
                  &       &                &      & \forall v_k \in path(v_i, v_j, E_{\mathrm{v_b}}), v_k \neq v_i, v_k \neq v_j, \\
                  &       &                &      &  (v, t) \notin M, (v, g) \notin M, t \in \mathcal{T}, g \in \mathcal{G} \} \\
                  & \cup  & \{ (t, g) &\mid & t = (c_t, V_t, V'_t, S_t) \in \mathcal{T}, \\
                  &       &            &      &  g = (c_g, c'_g, V_g, S_g) \in \mathcal{G},  \\
                  &       &                &      & v_i \in V'_t, v_j \in V_g, \exists path(v_i, v_j, E_{\mathrm{v_b}}), \\
                  &       &                &      & \forall v_k \in path(v_i, v_j, E_{\mathrm{v_b}}), v_k \neq v_i, v_k \neq v_j, \\
                  &       &                &      &  (v, t) \notin M, (v, g) \notin M, t \in \mathcal{T}, g \in \mathcal{G} \} \\
                  & \cup & \{(t_i, t_j)& \mid & t_i = (c, V_i, V'_i, S_i) \in \mathcal{T}, \\
                  &      &               &      & t_j = (c, V_j, V'_j, S_j) \in \mathcal{T}, \\
                  &      &               &      & v_i \in V'_i, v_j \in V'_j, \exists path(v_i, v_j, E_{\mathrm{v_b}}), \\
                  &       &                &      & \forall v_k \in path(v_i, v_j, E_{\mathrm{v_b}}), v_k \neq v_i, v_k \neq v_j, \\
                  &       &                &      &  (v, t) \notin M, (v, g) \notin M, t \in \mathcal{T}, g \in \mathcal{G} \} \\
\end{array} \end{aligned}
$$

which consists of three sets. The first set contains those goal- and transformation specification
pairs $(g, t)$ for which there exists a path over the set of behavior dependencies between variables
$E_{\mathrm{v_b}}$ between a variable $v_i \in V_g$ and a variable $v_j \in V'_t$, such that all
intermediate variables $v_k$ part of this path do not relate to any other transformation $g$ or
transformation $t$.

The second set contains those transformation-goal pairs for which there exists a path over the set
of behavior dependencies between variables $E_{\mathrm{v_b}}$ between a variable $v_i \in V'_t$ and
a variable $v_j \in V_g$, such that all intermediate variables $v_k$ part of this path do not relate
to any other transformation $g$ or transformation $t$.

The third set contains those transformation-transformation pairs for which there exists a path over
the set of behavior dependencies between variables $E_{\mathrm{v_b}}$ between a variable $v_i$ in
$V'_i$ and a variable $v_j$ in $V'_j$, such that all intermediate variables $v_k$ part of this path
do not relate to any other transformation $g$ or transformation $t$. In other words, the the output
of $t_j$ is bound by the output of $t_i$.

For the [decomposition-function-behavior view](#fig-decomposition-function-behavior) this set equals
$\{(\mathrm{g}_1, \mathrm{t}_1)\}$ since the value of $\mathrm{v}_3$ depends on the value of
$\mathrm{v}_2$ as defined by $\mathrm{h}_1$.

## Dependencies between behavior specifications

Two types of dependencies between behavior specifications are derived: behavior dependencies and
coordination dependencies.

### Behavior dependencies

The set of dependencies between behaviors $E_{\mathrm{h_b}}$ is given by:

$$
\begin{aligned} \begin{array}{rclcl} E_{\mathrm{h_b}}   &   =   & \{ (h_i, h_j)  & \mid & (S_i,
S'_i) \in h_i, h_i \in  \mathcal{H}, \\
                    &       &                &      & (S_j, S'_j) \in h_j, h_j \in \mathcal{H}, \\
                    &       &                &      & V'_i = \{ v \mid (v,b) \in Q', Q'  \in S'_i \}, \\
                    &       &                &      & V_j = \{ v \mid (v,b) \in Q, Q \in S_j \}, \\
                    &       &                &      & V'_i \cap V_j \neq \emptyset, h_i \neq h_j \}
\end{array} \end{aligned}
$$

That is, two behaviors $h_i$ and $h_j$ have a behavior dependency if at least one variable that is
used within then-clauses $S'_i$ of $h_i$ is used within when-clauses $S_j$ of $h_j$. Such a
dependency implies that the result of $h_j$ is bounded by the result of $h_i$.

### Coordination dependencies

Coordination dependencies $E_{\mathrm{h_c}}$ are given by:

$$
\begin{aligned} \begin{array}{rclcl} E_{\mathrm{h_c}}   &   =   & \{ (h_i, h_j)  & \mid & (S_i,
S'_i) \in h_i, h_i \in  \mathcal{H}, \\
                    &       &                &      & (S_j, S'_j) \in h_j, h_j \in \mathcal{H},  \\
                    &       &                &      & V'_i = \{ v \mid (v,b) \in Q', Q' \in S'_i \}, \\
                    &       &                &      & V'_j = \{ v \mid (v,b) \in Q', Q' \in S'_j \}, \\
                    &       &                &      & V'_i \cap V'_j \neq \emptyset, , h_i \neq h_j \}
\end{array} \end{aligned}
$$

That is, two behaviors $h_i$ and $h_j$ have a coordination dependency if at least one variable that
is used within a then-clauses $S'_i$ of $h_i$ is used within a then-clauses $S'_j$ of $h_j$. Such a
dependency implies that both behaviors aim to set the values of a least one shared variable.

## Dependencies between need, design, and relation specifications

In this section the dependency sets between needs, design, and relation specifications are given.
These dependencies set are not the main focus of this work, but provide opportunities to create
other views on the network of dependencies if desired. Need dependency set $E_\mathrm{n}$ is given
by:

$$
\begin{aligned} \begin{array}{rll} E_\mathrm{n} = \{(n_i, n_j) &\mid& n_i= (s, \textit{text}_i)
\in \mathcal{N}, \\
                            &    & n_j= (s, \textit{text}_j) \in \mathcal{N}, n_i \neq n_j    \}
\end{array} \end{aligned}
$$

That is, two needs $n_i$ and $n_j$ have a dependency if the needs have the same subject $s$.

Design specification dependency set $E_\mathrm{d}$ is given by:

$$
\begin{aligned} \begin{array}{rll} E_\mathrm{d} = \{(d_i, d_j) &\mid& d_i = (Q_i, S_i) \in
\mathcal{D}, \\
                            &    & d_j = (Q_j, S_j) \in \mathcal{D},\\
                            &    & q_i = (v_i, v'_i) \in Q_i, \\
                            &    &  q_j = (v_j, v'_j) \in Q_j  \\
                            &    & \{v_i, v'_i\} \cap \{v_j, v'_j\} \neq \emptyset, d_i \neq d_j \}
\end{array}\label{eqn:vddep} \end{aligned}
$$

That is, two design specifications $d_i$ and $d_j$ have a dependency if they share at least one
variable. For the example this implies that the design specifications $d_i$ and $d_j$ for which,
$q_1 \in Q_i$ and $q_2 \in Q_j$ holds, have a dependency as they share variable $v_9$.

Relation specification dependency set $E_\mathrm{r}$ is given by:

$$
\begin{aligned} \begin{array}{rll} E_\mathrm{r} =\{(r_i, r_j) &\mid& r_i = (V_i, V_i', V_i'') \in
\mathcal{R},  r_j=(V_j, V_j', V_j'') \in \mathcal{R}, \\
                           &    &  ( V_i' \cup V_i'' ) \cap ( V_j \cup V_j'' ) \neq \emptyset, \\
                           &    &  r_i \neq r_j \}
\end{array} \end{aligned}
$$

That is, two relations $r_i$ and $r_i$ have a dependency if the intersection of the union of the
output variables $V_i'$ and undirected variables $V_i''$ of relation $r_i$ with the union of the
input variables $V_j$ and undirected variables $V_j''$ of relation $r_j$ is not empty. In other
words, if an output or undirected variable of $r_i$ is an input or undirected variable of $r_j$,
then $r_j$ depends on $r_i$. Note that if $V''$ is empty for all $r \in \mathcal{R}$ that all
relation dependencies become directed.

## Mapping relations

In the previous sections component dependency sets $E_\mathrm{c_f}$ and $E_\mathrm{c_d}$, variable
dependency sets $E_\mathrm{v_f}$ and $E_\mathrm{v_d}$, goal specification dependency set
$E_\mathrm{g}$, need dependency set $E_\mathrm{n}$, transformation specification dependency set
$E_\mathrm{t}$, design specification set $E_\mathrm{d}$, the relation specification dependency set
$E_\mathrm{r}$ are defined. These sets all denote dependencies between ESL elements of the same
class, e.g., between components and between variables.

In this section, the focus is on connections between ESL elements of different classes. For example,
a connection between a component and a variable or a connection between a goal specification and a
design specification. These connections are referred to as mapping relations, which are undirected
by definition. These relations are required to gain insight into the relations between the different
ESL elements. For example, to gain insight into which flow and design variables a component relates
to. The derivation of mapping relations is based on containment or shared variables. For instance,
in the [decomposition-function view](#fig-decomposition-function) goal-specification $g_1$
contains component $c_1$, component $c_2$, and variable $v_1$, as such, $g_1$ has mapping relations
with $c_1$, $c_2$, and $v_1$. The set of mapping relations between all ESL elements $M$ is given by:

$$
\begin{aligned}
\begin{array}{rllll}
      M &  =  & \{(v, c) &\mid& c=(V_c, P_c, N_c, C_c, G_c, T_c, D_c, R_c) \in C, v \in V_c \cup P_c\} \\
        &\cup & \{(v, n) &\mid& n = (v, text_\mathrm{n}) \in \mathcal{N}\}  \\
        &\cup & \{(v, g) &\mid& g = (c_g, c'_g, V_g, S_g) \in \mathcal{G}, \\
        &     &          &    & ( v \in V_g \lor ( Q_g \in S_g, ( (v, b) \in Q_g \lor (v_q,       v) \in Q_g ) ) ) \} \\
        &\cup & \{(v, t) &\mid& t=(c_t, V_t, V'_t, S_t) \in \mathcal{T}, (v \in (V_t \cup       V'_t) \lor \\
        &     &          &    & ( Q_t \in S_t, ( (v, b) \in Q_t \lor (v_q, v) \in Q_t ) ) )       \} \\
        &\cup &\{(v, h)  &\mid& v \in V_\mathrm{h}(h), h \in \mathcal{H} \} \\
        &\cup & \{(v, d) &\mid& d= (Q, S_d) \in \mathcal{D}, ( ( (v, v'_q) \in Q \ \lor (v_q,       v) \in  Q)  \lor  ( Q_d \in S_d, \\
        &     &          &    & ( (v, b) \in Q_d \lor (v_q, v) \in Q_d ) ) ) \} \\
        &\cup & \{(v, r) &\mid& r= (V , V',  V'') \in \mathcal{R}, v \in V \cup V' \cup V''\} \\
        &\cup & \{(c, n) &\mid& n = (c, text_\mathrm{n}) \in \mathcal{N} \ \lor ( n = (v,       text_\mathrm{n}) \in \mathcal{N} \ \land v \in V_c \cup P_c ) \} \\
        &\cup &  \{ (c, g)  &\mid& g = (c, c'_g, V_g, S_g) \in \mathcal{G} \lor  g = (c_g, c, V_g, S_g) \in \mathcal{G},   \\
        &\cup &  \{ (c, g)  &\mid& g = (c_g, c'_g, V_g, S_g) \in \mathcal{G}, t = (c, V_t, V'_t, S_t) \in \mathcal{T},   \\
        &     &             &    & ( V_g \cap V_t \neq \emptyset \land c \in tree(c'_g) ) \lor (V_g \cap V'_t \neq \emptyset \land c \in tree(c_g)), c \neq c_g \}  \\
        &\cup & \{(c, t) &\mid& t = (c, V_t, V'_t, S_t) \in \mathcal{T} \ \lor (t = (c_t,       V_t, V'_t, S_t) \in \mathcal{T}, \\
        &     &          &    & (V_t \cup V'_t) \cap V_c \neq \emptyset) \}   \\
        & \cup & \{(c,h)\ &\mid&\ c=(V_c, P_c, N_c, C_c, G_c, T_c, D_c, R_c, H_c)\in\mathcal{C},
                   h\in \mathcal{H}, V_c\cap V_\mathrm{h}(h) \not=\emptyset\} \\
        &\cup & \{(c, d) &\mid& c=(V_c, P_c, N_c, C_c, G_c, T_c, D_c, R_c) \in C, d= (Q_d, S_d)       \in \mathcal{D}, \\
        &     &          &    & (v_q, v'_q) \in Q_d, (v_q \in V_c \cup P_c \lor  v'_q \in V_c \cup P_c) \}   \\
        &\cup & \{(c, r) &\mid& c=(V_c, P_c, N_c, C_c, G_c, T_c, D_c, R_c) \in C, r = (V , V',  V'') \in       \mathcal{R}, \\
        &     &          &    & V_c \cap (V , V',  V'') \neq \emptyset \} \\
        &\cup & \{(n,g)  &\mid& n = (s_n, text_\mathrm{n}) \in \mathcal{N}, g = (c_g, c'_g,       V_g, S_g) \in \mathcal{G}, s_n \in V_g\} \\
        &\cup & \{(n,t)  &\mid& n = (s_n, text_\mathrm{n}) \in \mathcal{N}, t = (c_t, V_t,       V'_t, S_t) \in \mathcal{T},  s_n \in (V_t \cup V'_t)\} \\
        &\cup & \{(n,d)  &\mid& n = (s_n, text_\mathrm{n}) \in \mathcal{N}, d = (Q_d, S_d)       \in \mathcal{D}, \\
        &     &          &    & ((s_n, v'_q) \in Q_d \ \lor (v_q, s_n) \in  Q_d)\}  \\
        &\cup & \{(n,r)  &\mid& n = (s_n, text_\mathrm{n}) \in \mathcal{N}, r = (V , V',  V'') \in       \mathcal{R}, s_n \in V \cup V' \cup V''\} \\
        & \cup & \{(g,h)\ &\mid&\ g=(c_g, c'_g, V_g, S_g)\in\mathcal{G},
                   h\in \mathcal{H}, V_g\cap V_\mathrm{h}(h) \not=\emptyset\} \\
        &\cup & \{(g,d)  &\mid& g = (c_g, c'_g, V_g, S_g) \in \mathcal{G},  d= (Q_d, S_d) \in       \mathcal{D}, \\
        &     &          &    & (v_q, v'_q) \in Q_d, v_q \in V_g \lor v'_q \in V_g\} \\
        &\cup & \{(g,r)  &\mid& g = (c_g, c'_g, V_g, S_g) \in \mathcal{G}, r = (V , V',  V'') \in       \mathcal{R}, V_g \cap (V , V',  V'') \neq \emptyset \} \\
         & \cup & \{(t,h)\ &\mid&\ t=(e_t, V_t, V'_t, S_t)\in\mathcal{T},
                   h\in \mathcal{H}, (V_t\cup V'_t)\cap V_\mathrm{h}(h) \not=\emptyset\} \\
        &\cup & \{(t,d)  &\mid& t = (c_t, V_t, V'_t, S_t) \in \mathcal{T},  d= (Q_d, S_d) \in       \mathcal{D}, \\
        &     &          &    & (v_q, v'_q) \in Q_d, v_q \in (V_t \cup V'_t) \lor v'_q \in       (V_t \cup V'_t)\} \\
        &\cup & \{(t,r)  &\mid& t = (c_t, V_t, V'_t, S_t) \in \mathcal{T}, r = (V , V',  V'') \in       \mathcal{R}, \\
        &     &          &    & (V_t \cup V'_t) \cap (V , V',  V'') \neq \emptyset\} \\
        & \cup & \{(d,h)\ &\mid&\ d=(Q_d, S_d)\in\mathcal{D},
                   h\in \mathcal{H}, v_Q(Q_d)\cap V_\mathrm{h}(h) \not=\emptyset\} \\
        &\cup & \{(d,r)  &\mid& d= (Q_d, S_d) \in \mathcal{D}, (v_q, v'_q) \in Q_d, \\
        &     &          &    & r = (V , V',  V'') \in \mathcal{R},  v_q \in V \cup V' \cup V'' \lor v'_q \in V \cup V' \cup V''\} \\
        & \cup & \{(r,h)\ &\mid&\ r=(V , V',  V'')\in\mathcal{R},
                   h\in \mathcal{H}, (V , V',  V'')\cap V_\mathrm{h}(h) \not=\emptyset\}
      \end{array}
\end{aligned}
$$

In which the function $V_\mathrm{h}(h)$ collects all variables used within a behavior requirement
$h$ and is given by:

$$
V_\mathrm{h}(h)\colon \mathcal{H} \rightarrow \mathcal{V} = \{v\ |\ (v,b)\in Q, Q\in(S\cup S'),
(S, S')\in h\}
$$

$M$ contains all mapping relations between all combinations of variables, components, needs,
function-specifications, design-specifications, and relations. In the remainder of this article, we
often refer to a subset of mapping relations between two distinct ESL element classes, which is
given by:

$$
\begin{aligned} \begin{array}{rllll} M_{\mathcal{X}\mathcal{Y}} & = & \{(x,y) &\mid& \mathcal{X},
\mathcal{Y}  \in \{\mathcal{V}, \mathcal{C}, \mathcal{N}, \mathcal{F}, \mathcal{D}, \mathcal{R}\},
\\
&   &         &     &\mathcal{X} \neq \mathcal{Y}, x \in \mathcal{X}, y \in \mathcal{Y},  \\
&   &         &     & ((x,y) \in M \lor (y,x) \in M) \} \end{array} \end{aligned}
$$

where, $\mathcal{X}$ and $\mathcal{Y}$ are the element sets for which the subset of mapping
relations is used. For example, $M_{\mathcal{C}\mathcal{F}}$ is the subset of mapping relations
between components $\mathcal{C}$ and functions $\mathcal{F}$.
