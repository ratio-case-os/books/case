## User

<div style="display:flex;gap:1rem;place-items:center">
<button id="btn-login-logout" class="md-button md-button--primary">Checking auth...</button>
<div id="user-email"></div>
</div>

## Environments

This list shows the current environments you have explicit access to.

<div id="user-environments" style="display:flex;gap:1rem;"></div>

## Roles

The list below contains the currently assigned roles to your account.

<ul id="user-roles">
</ul>
