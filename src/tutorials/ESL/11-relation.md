# Relations

Variables are powerful in themselves, but they can be interdependent, too. They may be coupled via
mathematical equations, models, laws of physics, or any other means of interdependency. We signal
this by defining relations between them. Relations can have names and arguments. The explicit math
or implementation is not covered in ESL. This is intentional, as the language is not designed to
replace all kinds of complex computation environments and languages. Instead, we provide means to
accurately capture the dependencies.

Lets say we want to describe the battery's efficiency as a relation between power potential
(chemical) and the output power (electrical):

## Definition

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "11-relation.esl:definition"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "11-relation.esl"
    ```

Here we define an undirected relation using the syntax `[name] relating parameters` followed by an
ordered parameter list using `* [name] is a [type]`.

### Relating, requiring, returning

By using a `relating parameters` section, you include "undirected" parameters to a relation. This
means that these variables are related, but the (model to do the) calculation does not distinguish
typical input or output parameters. Think Newton's second law of physics, $F = m \cdot a$, where you
could calculate each of the variables if you were given the other two. It isn't a one-way computer
program existing somewhere next to the ESL specification, but rather an interaction you could signal
between the given parameters.

However, when you want to express that some relation or model (partially) works in a directed
fashion, you can also include a `requiring parameters` section on a new line for typical input
parameters and a `returning parameters` section on another new line for typical output parameters.
You are allowed to include all three sections in a relation, but only one of each kind such that
this is a valid example including all three:

```elephant
define relations
  Foo
    relating parameters
      * bar is a string  # undirected

    requiring parameters
      * baz is a real    # inputs

    returning parameters
      * quux is a boolean   # outputs
```

## Instantiation

This relation is then instantiated in the `relations` section of the `Battery` component definition,
which now looks like:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "11-relation.esl:battery"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "11-relation.esl"
    ```

!!! note

    Note the use of `relating arguments` during instantiation versus `relating parameters` during
    definition.

    See the [Parameters versus arguments](./10-param-prop.md#parameters-versus-arguments) section
    from before for the explanation for this distinction.

---8<--- "next.md"
