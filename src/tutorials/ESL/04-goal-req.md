# Goal requirements

Great! With the groundwork set, we can start adding our first requirement! A `goal-requirement`
defines the goal of a component with respect to another. In case of our pump, we would like to
specify that it is the `drive-mechanism` that provides `torque` to the `pump`.

You can express this using the sentence: `drive-mechanism must provide torque to pump`, thus
following the format `[active component] must [verb] [variable] [preposition] [passive component]`.

So the `world` definition illustrating this becomes:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "04-goal-req.esl:highlight"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "04-goal-req.esl"
    ```

We also added a goal requirement inside the `ElectricalDriveMechanism` definition, which you can
review under the "File" tab.

!!! note

    Note how the goal requirement has a label `provide-torque`. Requirements need to have a label
    for easy identification. This label only has to be unique within the definition of that single
    component (or the world) and can thus be re-used in any other definition.

---8<--- "next.md"
