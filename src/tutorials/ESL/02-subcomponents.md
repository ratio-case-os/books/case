# Nested components

Components can be defined with any number of subcomponents. This allows for a hierarchical layout of
your specification. As such, you can start with your top-level components and work your way down as
more details become available.

Let's say that our `ElectricalDriveMechanism` consists of a `BrushlessMotor`, a `battery` to power
it and a `switch` to turn it on. We then have to add those component definitions and can add them as
subcomponents to the drive mechanism like so:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "02-subcomponents.esl:highlight"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "02-subcomponents.esl"
    ```

!!! tip

    We only decomposed the `ElectricalDriveMechanism` into three subcomponents and left the
    `CentrifugalPump` untouched. The component hierarchy can have an arbitrary number of levels
    in each branch, allowing for entire freedom in the level of detail in each (sub-)component!

---8<--- "next.md"
