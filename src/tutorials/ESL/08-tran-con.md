# Constraints vs. requirements

We define **requirements** as **that what is desired** for the system. However, some parts may be
already present or are to be treated as given. **That what is given** can be expressed using
**constraints**. `goal-`, `transformation-`, and `design-constraints` exist, and are identical to
their `...-requirements` counterparts, apart from the change from the `must` to `does` verb when
expressing constraints instead of requirements.

For instance, let us assume that our `battery` power source will always convert the chemical
`power-potential` to `power` regardless of our requirements. It may even be an off-the-shelf part.
It can be expressed as such:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "08-tran-con.esl:highlight"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "08-tran-con.esl"
    ```

So with minimal changes, we can signal that this transformation is non-negotiable and universally is
so.

## Nuancing requirements

For constraints, the "verb" inside the sentence is always `does`.

For requirements further nuance is allowed to be expressed:

- `must`
- `shall`
- `should`
- `could`
- `won't`

These help to distinguish preferences and requirements. When in doubt, it is often best to stick
with `must` for requirements and `does` for constraints.

!!! note

    Most compiler implementations will warn you if you use the wrong verb, but will process your
    specification nonetheless to obtain the dependency network as it generally does not matter
    for dependency derivation.

!!! info

    If you wish to read up on the nuance between the different verbs, there are some great
    references to be found in the
    [System specifications in detail](../../explanation/10-system-specifications.md#needs-requirements-and-constraints)
    explanation page.

---8<--- "next.md"
