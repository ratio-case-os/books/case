# Behavior requirements

You can specify all **static behavior** using goal, transformation, and design requirements. That
is, the workings or capabilities of a system that are (desired to be) present at all times.
Specifying **dynamic behavior** requires an additional construct. Enter `behavior-requirements`.

Let's review an example that we added to the `ElectricalDriveMechanism` definition that describes
the amount `power` that should go from the `power-source` to the `motor` depending on whether the
`motor-control-signal` is turned on:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "07-bhvr-req.esl:highlight"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "07-bhvr-req.esl"
    ```

## When this hold, then that should, too

We specified a `goal-requirement` before for the `ElectricalDriveMechanism` that states that `power`
must be provided to the `motor`. Using the `behavior-requirement` `toggle-power`, we specify which
values this power should assume under different circumstances or a set of `case`clauses. It
requires that it must be at least`300 [W]`when the conditions for case`on` are satisfied
(`motor-control-signal is true`).

!!! note

    Note how all `when` and `then` clauses are all [design rules](./06-dsgn-req.md#design-rule) that
    we just introduced!

## When no other case applies

Furthermore, it includes a fallback case `off` using the `when no other case applies` sequence,
stating it must then be `0 [W]`.

## What behaviors are not

Even though behavior requirements describe different states (cases) for variables to be in, they
don't form a full behavior model as their starting states, transitions, and other mathematics are
missing. What you can do, is accurately describe how a variable should respond to stimuli (other
variables).

For complete supervisory controllers/automata-models/PLC-code that include starting states,
(un-)controlled transitions and more, you should look elsewhere. ESL is not meant to replace such
tools, but rather provide them with a accurately documented starting point of the "things that
should always hold".

!!! tip

    We can highly recommend taking a look at
    [CIF by Eclipse ESCET™](https://eclipse.dev/escet/cif/). Another language that was conceived
    at the Eindhoven University of Technology and now part of Eclipse that enables you to
    declaratively model your system and supervisory controller by means of automata and generate
    guaranteed-to-be-correct PLC code right from your model. Simulation batteries included.

---8<--- "next.md"
