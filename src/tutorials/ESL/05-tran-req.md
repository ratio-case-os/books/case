# Transformation requirements

While `goal-requirements` are great to express the purpose of a component with respect to another,
components often transform their inputs to their outputs internally, which we specify using
`transformation-requirements`.

For instance, our `BrushlessMotor` is the component that converts `power` into `torque`. We describe
this using the sentence `must convert power into torque`. Lets review an example:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "05-tran-req.esl:highlight"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "05-tran-req.esl"
    ```

Here, we defined the internal transformation of the brushless motor using `must [verb]
[variables in] [preposition] [variables out]` (preceded by a label).

We also added a transformation requirement for the `ElectricalDriveMechanism` definition, which you
can review in the "File" tab.

!!! note

    Note the instantiation of variables **within** the `BrushlessMotor` component definition. These
    variables are only available within this definition and can only be referenced directly by
    requirements within this definition.

!!! tip

    With both goal- and transformation-requirements you can accurately describe the flow and
    conversion of variables throughout your components. This way, it is clear which component
    "provides" flows and which components "convert" them internally.

---8<--- "next.md"
