# Components

Components are at the core of ESL specifications. Typically, your (to be developed) systems will
live as a `component` within the `world`. Components are introduced using a definition such that
they can be instantiated in the `world`.

We are going to specify a **water pump**, that consists of a **centrifugal pump** driven by an
**electromotor**, with several requirements to the subcomponents, dimensions, and performance.

So in our case, the world becomes:

```elephant
---8<--- "01-components.esl"
```

Where each component definition is defined using the `define component` keyword combination followed
by the component `[name]`, which cannot have any spaces. Dashes are fine.

Within the `world` definition, we **instantiate** defined components by given them a **local name**
and referring to their definition with `[name] is a [defined component]`.

!!! note

    Note the difference between **definition** and **instantiation**. Definitions are similar to
    classes or templates in other programming languages and instantiations are similar to the actual
    instances or objects.

!!! tip

    It is recommended to use a `CamelCase` notation for definition names and to use a `lower-case`
    notation with dashes for instantiation names.

!!! tip

    We can refer to the defined components before their written definitions. The ESL compiler
    is smart that way and resolves later `define component [name]` statements by default.

---8<--- "next.md"
