# Types, variables, verbs

With components and subcomponents, the specification merely lays out the hierarchical component
structure, but tells us little about what needs to go on inside the system. We would like to start
expressing this using **requirements** and **constraints** on various **variables** in our design
space, but we need to define those first! Let us say we want to specify the torque that our drive
mechanism has to supply to the pump:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "03-type-var-verb.esl:highlight"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "03-type-var-verb.esl"
    ```

## Types and variables

We first defined a `MechanicalEnergyFlow` **type** that will only allow values with the Newton
meter (`Nm`) unit, and instantiated a **variable** on the global (world) level of that type named
`torque`. Similar to the component instantiation from before, you can use a `[name] is a [defined
type]` to describe this. There are several built-in types, being:

`boolean`

: Boolean values (`true` or `false`)

`integer`

: integer valued numbers (`1`, `2`, `3`, etc.)

`real`

: real valued numbers (`1.0`, `-2.6234`, etc.)

`string`

: literals (`"running"`, `"blue"`, etc.)

The built-in types can always be used without any explicit definition. This is fine, for example:

```elephant
world
  variables
    foo is a real
```

## Verbs

We also defined two **verbs** that we would like to use when we start expressing requirements. It
consists of a verb like `provide` and a preposition like `to` in this example. By having to stick to
a defined set of verbs and prepositions, the ambiguity in your specifications can be kept to an
absolute minimum!

!!! tip

    Most of the time, you can start off with the verbs `provide to` and `convert into`.
    `provide to` for goal requirements and `convert into` for transformations. We'll explore these
    in the next pages!

---8<--- "next.md"
