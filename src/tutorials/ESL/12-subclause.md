# Subclauses

The requirements are meant to be clear and concise, but often require some hardening or additional
measures. That is where subclauses come in. Essentially subclauses are the [design
rules](./06-dsgn-req.md#design-rule) from before (similar to design-requirements), but attached to
another goal-, transformation- or design-requirement.

Say we want to state that the brushless motor has a conversion efficiency of at least 80%. You can
add that like so:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "12-subclause.esl:type-def-1"
    ---8<--- "12-subclause.esl:type-def-2"
    ---8<--- "12-subclause.esl:motor-def"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "12-subclause.esl"
    ```

Where we added a new type, `Efficiency`, that is just a `real` between `0.0` and `1.0` (100%), and
set a subclause on the `convert-power` transformation requirement of the `BrushlessMotor`
definition by means of the `conversion` variable that we introduced there.

All it takes to add subclauses to a requirement or constraint is the `with arguments` at the end,
followed by any number of design rules using the asterisk (`*`) list format.

---8<--- "next.md"
