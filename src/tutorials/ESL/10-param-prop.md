# Parameters, properties, arguments

Goals, transforms and needs give us the expressiveness to build a complete specification. However,
sometimes, variables need to be available **outside** a component's own definition. For instance,
when we imposed the `min-water-flow` design requirement, we could not yet link it to the water-flow
inside the `pump` component.

Just as well, we would like both the `pump` and `drive-mechanism` to work with the same `torque`
variable, instead of each having their own unique variables. That way we can check whether the
requirements that all components impose on a variable are carefully captured and coordinated.

To indicate the **owner** of a certain design variable, you can add the `property` keyword to the
parameter declaration.

## Parameters

Let's review the `CentrifugalPump`'s definition. It now has a `parameters` section:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "10-param-prop.esl:pump-def"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "10-param-prop.esl"
    ```

So instead of the earlier "variables", it now has "parameters". Parameters of a definition are the
variables that are expected to be supplied as [arguments](#arguments) when you create an instance of
the definition.

### Properties

We also added a dimension parameter, `length`. The `property` keyword is added to this parameter, as
this is a typical characteristic of the drive mechanism component.

!!! warning

    Note that by definition **flows** cannot be properties of components as they flow through the
    system. As such, no single component **owns** a flow. Flows are variables that are involved in
    `goal-` or `transformation-requirements` and `constraints`. The flows in this definition are
    the `torque` and `water-flow`.

## Arguments

The definition of parameters means that the instantiation of the components living in the `world`
has also changed. The parameters you set in a definition are required to be provided at each
instantiation. Let's review this for the `pump` component instance:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "10-param-prop.esl:world-vars"
    ---8<--- "10-param-prop.esl:pump-inst"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "10-param-prop.esl"
    ```

Here, the required parameters of the `CentrifugalPump` definition are provided by means of the `with
arguments` keywords followed by an argument list.

This list is **positional**, meaning that the arguments are mapped to the parameters by the order in
which they are defined and provided.

An argument list may be of the inline form `foo is a Foo with arguments bar, baz, quux` or the
multi-line form as shown in the snippet: using an asterisk (`*`) list.

!!! note

    Note that we do have to specify the `spatial` type both at the world level and in the
    `CentrifugalPump`'s [parameters](#parameters). The compiler will alert you if any type
    mismatches are found between parameters and the provided arguments.

## Parameters versus arguments

We use `parameters` in definitions and (`with`) `arguments` in instantiations. This distinction is
often encountered in programming languages and to translate
_Robert Nystrom's_[@nystrom2021crafting] excellent definition to our terminology of 'definitions'
and 'instantiations':

- An **argument** is an actual variable representing a value you pass to an instantiation when you
  call it. So an instantiation call has an argument list. Sometimes you hear **actual parameter**
  used for these.
- A **parameter** is a variable that holds the value of the argument inside the body of the
  definition. Thus, a function declaration has a parameter list. Others call these **formal
  parameters** or simply **formals**.

---8<--- "next.md"
