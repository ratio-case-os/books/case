# ESL tutorial

Welcome to the Elephant Specification Language tutorial! This tutorial will introduce you to
the reasoning behind and concepts within the Elephant Specification Language, or ESL for
short.

!!! info

    For a more in-depth explanation of ESL's foundations you can always
    check the [Reference](../../reference/README.md) pages.

## Introduction

System Engineers play a vital role in the development of complex systems. Coordinating the design
and production of thousands of components across modules, teams, departments and even companies is
an immense challenge! Currently, the design documentation or "specs" of a system are often
communicated in the form of PDF, Word, and Excel files. Several solutions exist that try to organize
these files and tie them to the components they are supposed to describe.

Although this document organization is helpful, the documents themselves form a huge blind spot as
they are often **inconsistent**, **incomplete**, and **out-of-date**. Enter **ESL**! A highly
structured though human-readable specification language that introduces a fixed syntax while
allowing the expression of any design requirement or constraint. Since its syntax is fixed, the ESL
documents can be checked and analyzed automatically for their consistency and completeness.

Moreover, all relations, dependencies and links of requirements to components and variables are
automatically derived from the specification instead of having to be maintained manually. Even for
simple projects the number of dependencies quickly exceeds the 100 or even 1,000 mark! The automated
dependency derivation reduces the risk of human error drastically, as dependencies cannot be
forgotten to be added by definition.

Naturally, not every System Engineer you come across will work with ESL specifications, which is
where the generated PDF output comes in. Nicely formatted documents, generated right from your ESL
specification, which once more guarantees their consistency!

Furthermore, as ESL documents are written in plain text, they are exceptionally suitable for Version
Control Systems such as Git.

## Running example

The different concepts in the ESL language will be explained using an example specification. We are
going to specify a **water pump**, that consists of a **centrifugal pump** driven by an
**electromotor**, with several requirements to the subcomponents, dimensions, and performance.

## Next!

Press next (or ++n++ on your keyboard) to head over to the next page!
