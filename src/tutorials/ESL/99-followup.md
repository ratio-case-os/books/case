# What now?

Feel free to start writing your own system specifications! While the accurate documentation of your
system specifications is incredibly valuable in itself, you might want to analyze your dependency
network or convert it to more conventional or pretty formats such as a PDF.

If you're interested in that, feel free to head over to the
[How-to guides](../../how-to-guides/README.md) to see what you can achieve right from your spec!
