# Comments and tags

There are two types of comments available in ESL specifications. Regular comments and **attached**
comments. As a comment sign we use the pound sign (`#`).

## Regular comment

For instance, you can add a regular comment at the top of the file just like:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "13-comments.esl:regular-comment"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "13-comments.esl"
    ```

!!! note

    Regular comments are discarded by the compiler and should aid in reading or writing the ESL
    specification itself. They do not end up in the processed output content.

## Attached comment

The attached comments are directly tied to a component, variable, relation, need, constraint, or
requirement and are included and available as additional information in any output document we
generate. Attached comments can be added using the `#<` combination or in a special comment section,
like so:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "13-comments.esl:attached-comment"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "13-comments.esl"
    ```

Multi-line comments are made by repeating the (attached) comment symbols on every line. The
`comments` section works by naming any component, variable or relation instance you want to attach a
comment to and using the attached comment sign. This can help separating the comment documentation
from the requirements. Valid targets in the comments section are an instance of a component,
variable, relation, need, constraint, or requirement. This can be used to refer to test protocols
for specific requirements.

---8<--- "next.md"
