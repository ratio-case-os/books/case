# Design requirements

You can use `design-requirements` to express requirements on the **values of variables**. These are,
for instance, useful if you want to specify a component's size, set a bound on a "flow" variable in
or to a component or set bounds on a key performance indicator.

For instance, we can express a minimum required water flow of the pump. Let's look at an example
we added to the world definition to embody this:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "06-dsgn-req.esl:highlight"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "06-dsgn-req.esl"
    ```

!!! note

    The unit `[L/s]` specified to this requirement **must match** a unit that is associated with its
    [Type definition](./03-type-var-verb.md#types-and-variables).

!!! note

    Note that the minimum water flow is not coupled to any specific component at this point. It is
    only a general statement for now. Tying this requirement to the instance of the pump is
    explained later on in [Parameters and properties](./10-param-prop.md).

## Design rule

Design requirements are expressed using the design rule syntax, which is re-used in other parts of
the specification later on.

A design rule, of which `water-flow must be at least 1.0 [L/s]` is an example, can be used to
express (in)equalities or objectives.

The syntax follows one of:

1. `[subject variable] must be [comparison] [bound variable]`
1. `[subject variable] must be [comparison] [value] [[optional unit]]`
1. `[subject variable] must be [objective]`

Where the first expresses an (in)equality between two variables, the second is used to express an
(in)equality for a variable with a value, and the third is to set a design objective for that
variable.

### Comparison

The `[comparison]` should be one of:

| Comparison      | Mathematical equivalent |
| :-------------- | :---------------------: |
| `equal to`      |          `==`           |
| `not equal to`  |          `!=`           |
| `at least`      |          `>=`           |
| `at most`       |          `<=`           |
| `greater than`  |           `>`           |
| `smaller than`  |           `<`           |
| `approximately` |           `~`           |

This logic may be extended using `or` within a single rule. Adding multiple "and" like statements,
however, is usually done by providing them as parallel (separate) requirements, which is equivalent
and more readable. Bracketing logic is not supported for the same reason.

### Objective

The `[objective]` may be one of:

- `maximized`
- `minimized`

### Placeholder value

The values that quantify design requirements are often subject to discussion during a design
process. The compiler therefore accepts `t.b.d.` as a valid value for anything, which stands for "to
be determined". This indicates that the exact value of a bound within a design rule should be
determined at a later point in time.

For example, this is a valid requirement:

```elephant
---8<--- "06-dsgn-req-tbd.esl"
```

---8<--- "next.md"
