# Needs

Sometimes you cannot quantify a requirement and must state something qualitatively. For example, you
may want to refer to an externally defined standard. This can be specified using a `need`, which
should be used with caution. Needs serve a documentation purpose, but are mostly ignored by the
compiler as far as any checks can go.

Let's say our `drive-mechanism` that lives in the `world` definition needs to be waterproof up to
IP68 compliance. There are no further constraints on need lines than that it needs to **start** with
a **variable** or a **component name** as in `[variable | component-name] [free text]`, which is why
they are some form of 'escape' in the specifications. Use this wisely and responsibly!

You can add the IP68 `need` to the `world` like so:

=== "Highlight"

    ```elephant title="world.esl"
    world
      # Earlier content is visible in File tab.
    ---8<--- "09-need.esl:highlight"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "09-need.esl"
    ```

!!! tip

    When client specifications are too vague (yet) to be implemented in a quantitative manner, it
    may be wise to document them using a **need** for the time being. These can serve as action
    points to reduce the **neediness** of the specification and support the transition from
    qualitative to quantitative requirements later on.

---8<--- "next.md"
