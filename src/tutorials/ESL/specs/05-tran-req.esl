define type
  MechanicalEnergyFlow is a real with unit Nm
  ElectricalEnergyFlow is a real with unit W
  LiquidMaterialFlow is a real with unit L/s
  EnergyPotential is a real with unit Wh


define verbs
  provide to
  convert into


world
  variables
    torque is a MechanicalEnergyFlow

  components
    pump is a CentrifugalPump
    drive-mechanism is an ElectricalDriveMechanism

  goal-requirements
    provide-torque: drive-mechanism must provide torque to pump


define component CentrifugalPump
  variables
    torque is a MechanicalEnergyFlow
    water-flow is a LiquidMaterialFlow

  transformation-requirements
    convert-torque: must convert torque into water-flow


define component ElectricalDriveMechanism
  variables
    power-potential is an EnergyPotential
    power is an ElectricalEnergyFlow
    torque is a MechanicalEnergyFlow

  transformation-requirements
    convert-power-potential: must convert power-potential into torque

  components
    motor is a BrushlessMotor
    power-source is a Battery
    power-switch is a Switch

  goal-requirements
    provide-power: power-source must provide power to motor


# ---8<--- [start:highlight]
define component BrushlessMotor
  variables
    power is an ElectricalEnergyFlow
    torque is a MechanicalEnergyFlow

  transformation-requirements
    convert-power: must convert power into torque
# ---8<--- [end:highlight]


define component Battery
  empty

define component Switch
  empty
