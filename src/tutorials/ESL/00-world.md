# Hello world!

We start by specifying the modelling boundary. We do so by specifying the `world`. The absolute
minimal ESL specification you can write is therefore:

=== "Highlight"

    ```elephant title="world.esl"
    ---8<--- "00-world.esl:highlight"
    ```

=== "File"

    ```elephant title="world.esl"
    ---8<--- "00-world.esl"
    ```

Thus introducing two new keywords:

- `world`: The modelling boundary, can only occur once in a ESL specification. We assume nothing
  exists outside of the `world`.
- `empty`: When we want to specify something without any further information, we write `empty` to
  signal this.

!!! note

    All code blocks in this tutorial come in two pairs. The first is the **`Highlight`**, which
    usually is an example of the newly added 'thing'. The second one is the entire **`File`**, which
    pretty much always is `world.esl` for the duration of the tutorial.

## Compiling a specification

In order to compile this specification and check for any errors, make sure you have
[RaESL](https://raesl.ratio-case.nl) or another compatible compiler installed and run `raesl compile
world.esl` on your command line while you are in the same directory as `world.esl`.

For more advanced compilation setups with multiple files and/or directories, take a look at `raesl
compile --help` for the latest command line options.

!!! tip

    By convention, we name the file containing the `world` definition `world.esl` so that it's
    always easy to find.

!!! tip

    Although we use a certain spacing style when writing ESL specifications, we only do this for
    readability.

    The parser and compiler ignore any indentation.

---8<--- "next.md"
