# Editor setup

## Syntax highlighting

Currently, we support an extension for [Visual Studio Code](https://code.visualstudio.com/) or
[VSCodium](https://vscodium.com/), the Free/Libre Open Source Software Binaries of VS Code.

It supports syntax highlighting and is available on:

- [Elephant Specification Language on VS Code Marketplace](https://marketplace.visualstudio.com/items?itemName=ratio-inno.elephant-specification-language)
- [Elephant Specification Language on Open-VSX marketplace for VSCodium](https://open-vsx.org/extension/ratio-inno/elephant-specification-language)

!!! tip

    You can also press ++ctrl+p++ in either editor and enter:

    ```bash linenums="0"
    ext install ratio-inno.elephant-specification-language
    ```

Sooner, rather than later, we aim to implement a fully fledged Language Server in that extension
that checks your specifications for problems in real-time as you type and save. Currently, you can
do the following in either of those two editors:

## Define a build task

VSCode/VSCodium support defining build tasks. These can populate/update the `PROBLEMS` tab in your
editor that typically shows any problems in your code.

First off, make sure you have a Python environment with [RaESL](https://raesl.ratio-case.nl)
installed, for instance by means of pip or pipx:

=== "pip"

    ```bash linenums="0"
    pip install "raesl[all]"
    ```

=== "pipx"

    ```bash linenums="0"
    pipx install "raesl[all]"
    ```

This should enable the `raesl` command in your terminal.

Using the task below, you can update your problems with those listed in a
run of the RaESL compiler. The steps to do so are:

1. Make sure you have a `.vscode/` directory in your workspace and add the snippet below in a
   `tasks.json` file in that directory.
1. You can optionally change the `"args": ["compile", "."],` line's `"."` to the path of your
   specification files or folder between quotes.
1. Press ++ctrl+shift+b++ to update the problems after some changes.

```json title=".vscode/tasks.json" hl_lines="14"
{
  // See https://go.microsoft.com/fwlink/?LinkId=733558
  // for the documentation about the tasks.json format
  "version": "2.0.0",
  "tasks": [
    {
      "label": "RaESL: compile",
      "group": {
        "kind": "build",
        "isDefault": true
      },
      "type": "shell",
      "command": "raesl",
      "args": ["compile", "."], // Feel free to change "." to the path of your specs.
      "isBackground": false,
      "presentation": {
        "echo": true,
        "reveal": "never",
        "focus": false,
        "panel": "dedicated",
        "showReuseMessage": false,
        "clear": false,
        "revealProblems": "always"
      },
      "problemMatcher": {
        "owner": "RaESL",
        "fileLocation": ["relative", "${workspaceFolder}"],
        "pattern": [
          {
            "regexp": "^(\\w+): ([\\w\\s]+) \\[(\\w+)\\] at (.*):(\\d+):(\\d+)-(\\d+):(\\d+).*$",
            "severity": 1,
            "code": 3,
            "file": 4,
            "line": 5,
            "column": 6,
            "endLine": 7,
            "endColumn": 8
          },
          {
            "regexp": "^\\w+:   (.*)$",
            "message": 1
          }
        ]
      }
    }
  ]
}
```
