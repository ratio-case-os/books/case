# Functional dependency analysis

!!! warning

    This content is not fully migrated, yet. Please hop over to this page on
    [docs.ratio-case.nl](https://docs.ratio-case.nl/manuals/esl_manual/index.html#functional-dependencies)
    for the source content.
