# Dependency graph generation

One of the major benefits of using ESL is that dependencies between components, variables, function,
behavior, design, and relation specifications can automatically be derived. This information is
stored within a graph which is the output of the ESL compiler. The resulting graph is the basis for
many system architecture models.

!!! note

    Feel free to check out the [Dependency derivations](../../reference/2_dependency-derivations.md)
    section of for the mathematical basis of the derived dependency graph.

!!! warning

    This content is not fully migrated, yet. Please hop over to this page on
    [docs.ratio-case.nl](https://docs.ratio-case.nl/manuals/esl_manual/index.html#generated-dependency-graphs)
    for the source content.
