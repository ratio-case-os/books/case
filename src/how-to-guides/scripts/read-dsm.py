from pathlib import Path

from ragraph.graph import Edge, Graph, Node
from ragraph.plot import dmm

if __name__ == "__main__":
    g = Graph()
    for char in ["A", "B", "C", "D"]:
        g.add_node(Node(char))

    g.add_edge(Edge(g["A"], g["B"]))
    g.add_edge(Edge(g["A"], g["C"]))
    g.add_edge(Edge(g["C"], g["A"]))
    g.add_edge(Edge(g["B"], g["D"]))

    fig = dmm(
        g.leafs,
        g.leafs,
        g.edges,
    )
    assets = Path(__file__).parent.parent / "assets"
    assets.mkdir(exist_ok=True)
    fig.write_image(assets / "read-dsm.svg")
