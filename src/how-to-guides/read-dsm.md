# How to read a DSM?

A DSM, or Dependency Structure Matrix, is a great way to display dependencies or interfaces between entities. A DSM is closely related to the adjacency matrix of a graph. A graph or network with nodes (entities) and edges (interfaces) is a more common and perhaps more intuitive representation of data, although a graph tends to lend itself less for the visualization and inspection of discrete data when things start to grow.

## Example graph

Let's consider an example graph:

```mermaid
---8<--- "read-dsm.mmd"
```

Here you can see dependencies between four entities: `A`, `B`, `C`, and `D`.

- `A` is input to `B` and `C`.
- `C` provides input back to `A`.
- `B` provides input to `D`.

## Corresponding DSM

The corresponding DSM of this would be:

<figure markdown>
  ![Corresponding DSM for the ABCD example.](./assets/read-dsm.svg)
  <figcaption>
    Corresponding DSM for the ABCD example.
  </figcaption>
</figure>

### Matrix axis: nodes

The nodes are displayed on both axis, always in identical order (`A`, `B`, `C`, `D`) from the top-left to the bottom-right, with their rows and columns numbered from 1 onwards.

### Matrix dots: edges

- The _inputs_ of a node, or its incoming edges are displayed in its _row_.
- The _outputs_ of a node, or its outgoing edges are therefore displayed in its _column_.
- You can interpret the diagonal as the "self" of a node.
  - Any self-loops could be displayed here.
  - It is often greyed out for readability purposes.

For instance the blue dot in the first row (`A`) in the third column (`C`) corresponds to the arrow from `C` to `A`.

!!! info

    This is called the _IR/FAD_ convention, or _Inputs in Rows/Feedback Above Diagonal_. Note how the _feedback_ from `C` to `A` is above the diagonal if you were to interpret the nodes on the axis as the steps of a process, for example.

    Sometimes the transpose matrix is used (_IC/FBD_), but it is far less common and we usually stick to IR/FAD whenever we can.

So for the given dependencies in the graph's description that corresponds to:

- `A` is input to `B` and `C`: column 1 to row 2 and 3.
- `C` provides input back to `A`: column 3 to row 1.
- `B` provides input to `D`: column 2 to row 4.
