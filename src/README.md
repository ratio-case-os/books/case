# Introduction

Welcome to Ratio's Computer Aided Systems Engineering (CASE) knowledge base! We hope you will find
what you seek.

This place is the home of:

- Our best practices and usage of our [open-source](#licensing) tools in the field of Systems
  Engineering such as the use of the Dependency Structure Matrix (DSM).
- The Elephant Specification Language (ESL), a formal though human-readable language to write system
  specifications that kick-off your Model-Based Systems Engineering efforts right from the start!

We offer that information in the following formats:

- [Tutorials](tutorials/README.md) for a step-by-step approach to learn ESL,
- [How-to guides](how-to-guides/README.md) for a more use-case centric approach,
- [Explanation](explanation/README.md) for more background information regarding systems engineering
  and engineering design, as well as reasoning behind our methods and tools,
- [Reference](reference/README.md) for the complete language reference and nitty gritty,
- [Governance](governance/README.md) for the language enhancement proposals and their current
  status.

!!! warning

    This page aims to replace [https://docs.ratio-case.nl](https://docs.ratio-case.nl) in due time.
    We're currently in the process of transferring all content, so some pages might be incomplete.

!!! info

    Looking for Python package docs? Head over to the specific websites for
    [RaGraph](https://ragraph.ratio-case.nl) and [RaESL](https://raesl.ratio-case.nl).

!!! tip "Contribute"

    If you're looking to code and contribute, you might want to check out our repositories over at
    [our GitLab organization](https://gitlab.com/ratio-case-os). There are Visual Studio Code
    (or Codium) extensions, Python packages, Rust crates, Docker images (i.e. for JupyterHub), some
    general development helpers, and of course the source code for this guide.

## Who or what is Ratio CASE?

<img src="/assets/logo-primary-dark.svg#only-light" alt="Ratio CASE Logo" />
<img src="/assets/logo-primary-light.svg#only-dark" alt="Ratio CASE Logo" />

OK, but who or what is Ratio? We're a company founded by
[Tim Wilschut](https://nl.linkedin.com/in/tim-wilschut-98a362103) and
[Tiemen Schuijbroek](https://nl.linkedin.com/in/tiemen-schuijbroek) and are based in
Eindhoven, the Netherlands. The home of companies like ASML, Philips, and our alma mater: The
Eindhoven University of Technology. In short:

!!! quote

    Ratio CASE focuses on the development of methods and tools for the specification, visualization,
    analysis, and optimization of system architectures to support engineers in avoiding rework and
    in developing even better systems. Ratio combines the latest innovative and scientific insights
    as well as algorithms to ultimately boost the productivity of our clients and their partners.

## Could use some help?

Are you looking for help in the field of systems engineering? Would you like some training for
yourself or your employees? We, Ratio Computer Aided Systems Engineering or Ratio CASE if you will,
are here to help! Head over to our main website
[https://www.ratio-case.nl](https://www.ratio-case.nl) and look for our contact options. We offer:

- Systems modelling and analysis workshops
- Elephant Specification Language training
- Systems Engineering support
- Systems Engineering consultancy services

We've got more than 4 years of experience as a company in brownfield analysis of system
architectures and requirements analysis. It might actually be 8 if you count Tim's 4-year PhD in the
field of Systems Engineering at the Eindhoven University of Technology for the Dutch Ministry of
Infrastructure. Through our structured approach we offer our customers:

- A start with Model-Based Systems Engineering right from their requirements!
- Gain valuable insights and deep understanding into their current products' architecture by
  analyzing current documentation.
- Are enabled to develop product platforms to move from an Engineer-to-Order to a Configure-to-Order
  approach.

## Licensing

All our open-source software can be found over at
[our GitLab organization](https://gitlab.com/ratio-case-os).

All software related to the processing of the Elephant Specification Language (ESL) is
[MIT](http://opensource.org/licenses/MIT) or
[Apache-2.0](http://www.apache.org/licenses/LICENSE-2.0) licensed at your volition, since we believe
a language is as good as the people that can use it unconstrained!

All our analysis software in the field graph (network) or Dependency Structure Matrix analysis is
licensed under the [GPL-v3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.en.html) license, since
we believe in encouraging companies to distribute their packages as open-source for these more
"exploitation" oriented packages. For tailor-made licensing options, for instance for incorporation
of any of our packages into commercial software where the GPL license is not feasible, please reach
out to us via the contact options on [https://www.ratio-case.nl](https://www.ratio-case.nl).
