# Ratio's Computer Aided Systems Engineering Guide

This repository contains the source code for Ratio's Computer Aided Systems Engineering knowledge
and related tools documentation over at https://guide.ratio-case.nl. It also serves as the home for
the Elephant Specification Language (ESL) governance. The best way to enjoy this content is to head
over there! Possible uses for this repository itself might be:

- Contribute!
- Build older versions.

## Contributions

Contributions to this knowledge base are highly encouraged! We heartily welcome:

- Language Enhancement Proposals for the Elephant Specification Language (ESL).
- New datasets in the form of ESL specifications or Dependency Structure Matrix (DSM) cases.
- Examples for ESL or our tools.

Any contributions made to this repository are assumed to be in line with the license and copyright
notice below. Contributors themselves are responsible for making sure that their contributed content
adheres to the set terms.

## License and copyright

All code samples written in Python, Rust or the Elephant Specification Language are free to use and
can be consumed under the MIT or Apache-2.0 license at your volition.

Ratio Innovations B.V. shall retain all rights, foremost but not exclusively copyright, to the
contents of this book and repository. Quoting or directly copying the text without proper
attribution or written consent is not allowed. As for proper attribution, a clearly visible notice
of "Ratio Computer Aided Systems Engineering" or "Ratio CASE" and a link to the quoted documentation
would suffice, as well as a properly formatted citation following academic guidelines. When in
doubt, feel free to ask!
